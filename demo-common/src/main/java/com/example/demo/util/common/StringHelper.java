package com.example.demo.util.common;/**
 * Created by penglee on 2019/7/2.
 */

/**
 * StringHelper
 * <p>
 * Description: <br>
 * Created in 2019/7/2 11:38 <br>
 *
 * @author 李鹏
 */
public class StringHelper {
    /**
     * Description: getObjectValue
     * Params: [obj]
     * Return: java.lang.String
     * CreateDate: Created in
     * Modify:
     * Author: 李鹏
     */
    public static String getObjectValue(Object obj) {
        return obj == null ? "" : obj.toString();
    }
}
