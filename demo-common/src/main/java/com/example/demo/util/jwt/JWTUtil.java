package com.example.demo.util.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.example.demo.common.constant.CommonConstants;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * @program: xy-framework
 * @description: jwt工具类
 * @author: FN
 * @create: 2020-03-23
 **/
public class JWTUtil {

    public static final String SECRET_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQD5mA0ASNnjryMvSP5+IhNhbk1G\n" +
            "odMj5da1/+qNnq9C9ytIqQAeOy3QOxo/8+ZXjPWSY12WY9TtnB8RqmyPux/zh06D\n" +
            "KWcXSDLyMY4tZapEDHv+ggcaLjdgx8dXqyeodFgJMTzvA9peaLs5ajxx1oWDQ98L\n" +
            "w+ub5sYZemCuJwLHXQIDAQAB"; //秘钥

    private static final String ISSUER = "xyaq"; //签发人

    /**
     * 生成签名
     */
    public static String generateToken(String account) {
        Date now = new Date();
        Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY); //算法

        String token = JWT.create()
                .withIssuer(ISSUER) //签发人
                .withIssuedAt(now) //签发时间
                .withExpiresAt(new Date(now.getTime() + CommonConstants.REFRESH_TOKEN_EXPIRE_TIME)) //过期时间
                .withClaim("account", account) //保存身份标识
                .sign(algorithm);
        return token;
    }

    /**
     * 生成签名+过期时间
     */
    public static String generateToken(String account, long expireTime) {
        Date now = new Date();
        Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY); //算法

        String token = JWT.create()
                .withIssuer(ISSUER) //签发人
                .withIssuedAt(now) //签发时间
                .withExpiresAt(new Date(now.getTime() + expireTime)) //过期时间
                .withClaim("account", account) //保存身份标识
                .sign(algorithm);
        return token;
    }

    /**
     * 验证token
     */
    public static boolean verify(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY); //算法
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(ISSUER)
                    .build();
            verifier.verify(token);
            return true;
        } catch (Exception ex) {
//            ex.printStackTrace();
        }
        return false;
    }

    /**
     * 从token获取username
     */
    public static String getUsername(String token) {
        if (StringUtils.isNotBlank(token)) {
            try {
                return JWT.decode(token).getClaim("account").asString();
            } catch (Exception ex) {
//                ex.printStackTrace();
                return "";
            }
        }

        return "";
    }


    public static void main(String[] args) {
        String jwt = generateToken("admin");
        System.out.println(jwt);
        System.out.println(getUsername("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ4eWFxIiwiZXhwIjoxNTg1MzAxNTI5LCJpYXQiOjE1ODUzMDEzNDksImFjY291bnQiOiJ3eGR0ZXN0In0.KFzaPeBEwBZ0GUYrN36GZbOWK3mUoxeqix9DuF0rIAg"));
    }
}
