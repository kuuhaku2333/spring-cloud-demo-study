package com.example.demo.util.gson;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GsonUtil {
	//不用创建对象,直接使用Gson.就可以调用方法
	private static Gson gson = null;
	private static Gson formatGson = null;
	
	//判断gson对象是否存在了,不存在则创建对象
	static {
		if (gson == null) {
			//gson = GsonUtil.getGson();
			//当使用GsonBuilder方式时属性为空的时候输出来的json字符串是有键值key的,显示形式是"key":null，而直接new出来的就没有"key":null的
			gson = new GsonBuilder()
					.registerTypeAdapter(String.class, TypeAdaptersRedefine.STRING)
					.registerTypeAdapter(new TypeToken<Map<String, Object>>() {
					}.getType(), TypeAdaptersRedefine.MAP)
//					.registerTypeAdapter(Integer.class,TypeAdaptersRedefine.INTEGER)
//					.registerTypeAdapter(Double.class,TypeAdaptersRedefine.DOUBLE)
					.setDateFormat("yyyy-MM-dd HH:mm:ss")
					.create();
		}
		if (formatGson == null) {
			formatGson = new GsonBuilder().setPrettyPrinting().create();
		}
	}
	
	//无参的私有构造方法
	private GsonUtil() {
	}
	
	public static Gson getGson() {
		return gson;
	}
	
	/**
	 * 将对象转成json格式
	 *
	 * @param object
	 * @return String
	 */
	public static String GsonString(Object object) {
		String gsonString = null;
		if (gson != null) {
			gsonString = gson.toJson(object);
		}
		return gsonString;
	}
	
	/**
	 * 将对象转成json格式 并且格式化打印
	 *
	 * @param object
	 * @return String
	 */
	public static String GsonformatString(Object object) {
		String gsonString = null;
		if (formatGson != null) {
			gsonString = formatGson.toJson(object);
		}
		return gsonString;
	}
	
	/**
	 * 将对象转成map
	 *
	 * @param object
	 * @return String
	 */
	public static Map<String, Object> GsonObjToMap(Object object) {
		Map<String, Object> map = null;
		if (gson != null) {
			String json = GsonUtil.GsonString(object);
			map = GsonUtil.GsonToMaps(json);
		}
		return map;
	}
	
	
	/**
	 * 将json转成特定的cls的对象
	 *
	 * @param gsonString
	 * @param cls
	 * @return
	 */
	public static <T> T GsonToBean(String gsonString, Class<T> cls) {
		T t = null;
		if (gson != null) {
			//传入json对象和对象类型,将json转成对象
			t = gson.fromJson(gsonString, cls);
		}
		return t;
	}
	
	
	/**
	 * 转成list
	 * 解决泛型问题
	 *
	 * @param json
	 * @param cls
	 * @param <T>
	 * @return
	 */
	public static <T> List<T> GsonToList(String json, Class<T> cls) {
		Gson gson = new Gson();
		List<T> list = new ArrayList<T>();
		JsonArray array = new JsonParser().parse(json).getAsJsonArray();
		for (final JsonElement elem : array) {
			list.add(gson.fromJson(elem, cls));
		}
		return list;
	}
	
	
	/**
	 * json字符串转成list中有map的
	 * 有bug int 类型 会装换成 double 类型的
	 *
	 * @param gsonString
	 * @return
	 */
	@Deprecated
	public static <T> List<Map<String, T>> GsonToListMaps(String gsonString) {
		List<Map<String, T>> list = null;
		if (gson != null) {
			list = gson.fromJson(gsonString,
					new TypeToken<List<Map<String, T>>>() {
					}.getType());
		}
		return list;
	}
	
	/**
	 * json字符串转成map的
	 *
	 * @param gsonString
	 * @return
	 */
	public static <T> Map<String, T> GsonToMaps(String gsonString) {
		Map<String, T> map = null;
		if (gson != null) {
			map = gson.fromJson(gsonString, new TypeToken<Map<String, Object>>() {
			}.getType());
		}
		return map;
	}
	
	/**
	 * 根据实体类修改Map Key
	 *
	 * @param nodeVo
	 * @param cls
	 * @param <T>
	 * @return
	 */
	public static <T> Map<String, Object> mapKeyChangeofObj(Map<String, Object> nodeVo, Class<T> cls) {
		for (String key : nodeVo.keySet()) {
			nodeVo.put(key, String.valueOf(nodeVo.get(key)));
		}
		String json = GsonUtil.GsonString(nodeVo);
		T bean = GsonUtil.GsonToBean(json, cls);
		return GsonUtil.GsonObjToMap(bean);
	}
	
}
