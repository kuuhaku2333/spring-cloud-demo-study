package com.example.demo.util.client;/**
 * Created by penglee on 2019/7/2.
 */

import javax.servlet.http.HttpServletRequest;

/**
 * Ip4ClientUtil
 * <p>
 * Description: <br>
 * Created in 2019/7/2 09:01 <br>
 *
 * @author 李鹏
 */
public class Ip4ClientUtil {
    /**
     * Description: getClientIp
     * Params: [request]
     * Return: java.lang.String
     * CreateDate: Created in
     * Modify:
     * Author: 李鹏
     */
    public static String getClientIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for" );
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP" );
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP" );
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }
}
