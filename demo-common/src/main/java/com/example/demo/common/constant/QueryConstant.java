package com.example.demo.common.constant;

/**
 * ES查询常量
 *
 * @date： 2021/6/8
 * @author: wbx
 */
public interface QueryConstant {

     String INDEX_NAME = "indexName";

     /**
      *  要高亮的字段
      */
     String HIGH_LIGHT_FIELD = "test2";

     /**
      *  需要带高亮标签的字段
      */
     String HIGH_LIGHT_TEXT = "highLightText";


     /**
      *  每页条数
      */
     String size = "size";

     /**
      *  当前页数
      */
     String current = "current";

     /**
      *  搜索关键字
      */
     String searchText = "searchText";

     /**
      *  创建时间字段
      */
     String createTime = "createTime";

     /**
      *  分词查询
      */
     String MATCH = "match";

     /**
      *  模糊查询
      */
     String WILDCARD_QUERY = "wildcardQuery";

}
