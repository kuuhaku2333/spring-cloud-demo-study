package com.example.demo.common.response;/**
 * Created by penglee on 2019/7/26.
 */

import com.example.demo.common.constant.StatusBusiness;
import lombok.Data;

/**
 * ResponseData
 * <p>
 * Description: 响应体封装类
 *
 * @author FN
 */
@Data
public class ResponseData<T> extends BaseResponse {
    T data;

    public ResponseData() {}

    public ResponseData(Builder<T> builder) {
        super(builder.status, builder.message);
        this.data = builder.data;
    }


    public static class Builder<T> {
        private int status = StatusBusiness.SUCCESS.getStatus();
        private Object message = StatusBusiness.SUCCESS.getMessage();
        private T data;

        public Builder() {
        }

        public Builder<T> status(int status) {
            this.status = status;
            return this;
        }

        public Builder<T> message(Object message) {
            this.message = message;
            return this;
        }

        public Builder<T> data(T data) {
            this.data = data;
            return this;
        }

        public ResponseData<T> build() {
            return new ResponseData<T>(this);
        }
    }
}
