package com.example.demo.common.exception;

import org.apache.commons.lang3.StringUtils;

import java.util.Collection;

/**
 * @program: xy-framework
 * @description: 异常信息断言辅助接口
 * @author: FN
 * @create: 2020-04-30
 **/
public interface Assert {

    BaseException BusinessException();

    BaseException BusinessException(Object... args);

    default void throwException() {
        throw BusinessException();
    }

    default void assertIsTrue(boolean obj) {
        if (obj) {
            throw BusinessException();
        }
    }

    default void assertNotTrue(boolean obj) {
        if (!obj) {
            throw BusinessException();
        }
    }

    default void assertIsNull(Object obj) {
        if (null != obj) {
            throw BusinessException();
        }
    }

    default void assertNotNull(Object obj) {
        if (null == obj) {
            throw BusinessException();
        }
    }

    default void assertStringNotNull(String str) {
        if (StringUtils.isBlank(str)) {
            throw BusinessException();
        }
    }

    default void assertStringIsNull(String str) {
        if (!StringUtils.isBlank(str)) {
            throw BusinessException();
        }
    }

    default void assertListIsNull(Collection obj) {
        if (!obj.isEmpty()) {
            throw BusinessException();
        }
    }

    default void assertListNotNull(Collection obj) {
        if (obj.isEmpty()) {
            throw BusinessException();
        }
    }
}