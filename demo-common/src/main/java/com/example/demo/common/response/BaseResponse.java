package com.example.demo.common.response;/**
 * Created by penglee on 2019/7/2.
 */

import com.example.demo.common.constant.StatusBusiness;
import lombok.Data;

import java.io.Serializable;

/**
 * BaseResponse
 * <p>
 * Description: <br>
 * Created in 2019/7/2 09:49 <br>
 *
 * @author 李鹏
 */
@Data
public class BaseResponse implements Serializable {
    private int status = StatusBusiness.SUCCESS.getStatus();
    private Object message = StatusBusiness.SUCCESS.getMessage();

    public BaseResponse(int status, Object message) {
        this.status = status;
        this.message = message;
    }

    public BaseResponse() {
    }

    public BaseResponse setStatusAndMessage(int status, Object message) {
        this.status = status;
        this.message = message;
        return this;
    }
}
