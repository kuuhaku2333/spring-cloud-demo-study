//package com.example.demo.common.algorithm.sm4;/**
// * Created by penglee on 2019/7/25.
// */
//
//
//import com.example.demo.common.algorithm.Util;
//import java.io.IOException;
//import java.util.Base64;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import static java.util.regex.Pattern.compile;
//
///**
// * SM4Utils
// * <p>
// * Description: <br>
// * Created in 2019/7/25 18:41 <br>
// *
// * @author 李鹏
// */
//public class SM4Utils {
//
//    public String secretKey = "";
//    private String iv = "";
//    public boolean hexString = false;
//
//    public SM4Utils() {
//    }
//
//    public String encryptData_ECB1(byte[] inputStream){
//        try
//        {
//            SM4_Context ctx = new SM4_Context();
//            ctx.isPadding = true;
//            ctx.mode = SM4.SM4_ENCRYPT;
//
//            byte[] keyBytes;
//            if (hexString)
//            {
//                keyBytes = Util.hexStringToBytes(secretKey);
//            }
//            else
//            {
//                keyBytes = secretKey.getBytes();
//            }
//
//            SM4 sm4 = new SM4();
//            sm4.sm4_setkey_enc(ctx, keyBytes);
////            String cipherText = new BASE64Encoder().encode(inputStream);
//            Base64.Encoder encoder = Base64.getEncoder();
//            String cipherText = encoder.encodeToString(inputStream);
//            if (cipherText != null && cipherText.trim().length() > 0)
//            {
//                Pattern p = compile("\\s*|\t|\r|\n");
//                Matcher m = p.matcher(cipherText);
//                cipherText = m.replaceAll("");
//            }
//            return cipherText;
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//            return  null;
//        }
//    }
//
//    public String encryptData_ECB(String plainText) {
//        try {
//            SM4_Context ctx = new SM4_Context();
//            ctx.isPadding = true;
//            ctx.mode = SM4.SM4_ENCRYPT;
//
//            byte[] keyBytes;
//            if (hexString) {
//                keyBytes = Util.hexStringToBytes(secretKey);
//            } else {
//                keyBytes = secretKey.getBytes();
//            }
//
//            SM4 sm4 = new SM4();
//            sm4.sm4_setkey_enc(ctx, keyBytes);
//            byte[] encrypted = sm4.sm4_crypt_ecb(ctx, plainText.getBytes("GBK" ));
////            String cipherText = new BASE64Encoder().encode(encrypted);
//            Base64.Encoder encoder = Base64.getEncoder();
//            String cipherText = encoder.encodeToString(encrypted);
//            if (cipherText != null && cipherText.trim().length() > 0) {
//                Pattern p = compile("\\s*|\t|\r|\n" );
//                Matcher m = p.matcher(cipherText);
//                cipherText = m.replaceAll("" );
//            }
//            return cipherText;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    public String decryptData_ECB(String cipherText) {
//        try {
//            SM4_Context ctx = new SM4_Context();
//            ctx.isPadding = true;
//            ctx.mode = SM4.SM4_DECRYPT;
//
//            byte[] keyBytes;
//            if (hexString) {
//                keyBytes = Util.hexStringToBytes(secretKey);
//            } else {
//                keyBytes = secretKey.getBytes();
//            }
//
//            SM4 sm4 = new SM4();
//            sm4.sm4_setkey_dec(ctx, keyBytes);
////            byte[] decrypted = sm4.sm4_crypt_ecb(ctx, new BASE64Decoder().decodeBuffer(cipherText));
//            Base64.Decoder decoder = Base64.getDecoder();
//            byte[] result = decoder.decode(cipherText);
//            byte[] decrypted = sm4.sm4_crypt_ecb(ctx, result);
//            return new String(decrypted, "GBK" );
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    public String encryptData_CBC(String plainText) {
//        try {
//            SM4_Context ctx = new SM4_Context();
//            ctx.isPadding = true;
//            ctx.mode = SM4.SM4_ENCRYPT;
//
//            byte[] keyBytes;
//            byte[] ivBytes;
//            if (hexString) {
//                keyBytes = Util.hexStringToBytes(secretKey);
//                ivBytes = Util.hexStringToBytes(iv);
//            } else {
//                keyBytes = secretKey.getBytes();
//                ivBytes = iv.getBytes();
//            }
//
//            SM4 sm4 = new SM4();
//            sm4.sm4_setkey_enc(ctx, keyBytes);
//            byte[] encrypted = sm4.sm4_crypt_cbc(ctx, ivBytes, plainText.getBytes("GBK" ));
////            String cipherText = new BASE64Encoder().encode(encrypted);
//            Base64.Encoder encoder = Base64.getEncoder();
//            String cipherText = encoder.encodeToString(encrypted);
//            if (cipherText != null && cipherText.trim().length() > 0) {
//                Pattern p = compile("\\s*|\t|\r|\n" );
//                Matcher m = p.matcher(cipherText);
//                cipherText = m.replaceAll("" );
//            }
//            return cipherText;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    public String decryptData_CBC(String cipherText) {
//        try {
//            SM4_Context ctx = new SM4_Context();
//            ctx.isPadding = true;
//            ctx.mode = SM4.SM4_DECRYPT;
//
//            byte[] keyBytes;
//            byte[] ivBytes;
//            if (hexString) {
//                keyBytes = Util.hexStringToBytes(secretKey);
//                ivBytes = Util.hexStringToBytes(iv);
//            } else {
//                keyBytes = secretKey.getBytes();
//                ivBytes = iv.getBytes();
//            }
//
//            SM4 sm4 = new SM4();
//            sm4.sm4_setkey_dec(ctx, keyBytes);
////            byte[] decrypted = sm4.sm4_crypt_cbc(ctx, ivBytes, new BASE64Decoder().decodeBuffer(cipherText));
//            Base64.Decoder decoder = Base64.getDecoder();
//            byte[] result = decoder.decode(cipherText);
//            byte[] decrypted = sm4.sm4_crypt_cbc(ctx, ivBytes, result);
//            return new String(decrypted, "GBK" );
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    public static void main(String[] args) throws IOException {
//        String plainText = "Admin!xyaq@77";
//
//        SM4Utils sm4 = new SM4Utils();
//        sm4.secretKey = "Smxxyaq!UIAS@Key";
////        sm4.secretKey = "JeF8U9wHFOMfs2Y8";
//        sm4.hexString = false;
//
//        System.out.println("ECB模式加密" );
//        String cipherText = sm4.encryptData_ECB(plainText);
//        System.out.println("密文: " + cipherText);
//
//        System.out.println("ECB模式解密" );
//        plainText = sm4.decryptData_ECB(cipherText);
//        System.out.println("明文: " + plainText);
//        System.out.println();
//
//        System.out.println("CBC模式加密" );
//        sm4.iv = "Smxxyaq!UIAS@Key";
////        sm4.iv = "UISwD9fW6cFh9SNS";
//        cipherText = sm4.encryptData_CBC(plainText);
//        System.out.println("密文: " + cipherText);
//        plainText = sm4.decryptData_CBC(cipherText);
//        System.out.println("明文: " + plainText);
//
//        System.out.println("CBC模式解密" );
//        String cipherText2 = "4Wl1Q9R3x/CIiqNI9wmanQ==";
//        System.out.println("密文："+cipherText2 );
////        System.out.println("密文：4esGgDn/snKraRDe6uM0jQ==" );
////        String cipherText2 = "4esGgDn/snKraRDe6uM0jQ==";
//        plainText = sm4.decryptData_CBC(cipherText2);
//        System.out.println("明文: " + plainText);
//    }
//}
