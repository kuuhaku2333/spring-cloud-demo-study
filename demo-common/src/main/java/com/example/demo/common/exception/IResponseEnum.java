package com.example.demo.common.exception;

/**
 * @program: xy-framework
 * @description: 响应枚举接口
 * @author: FN
 * @create: 2020-04-30
 **/
public interface IResponseEnum {

    int getStatus();

    String getMessage();
}