package com.example.demo.common.constant;

/**
 * 账号状态
 */
public enum AccountStatus {

    FORBID(0),ACTIVE(1),UNACTIVE(2);

    private int code;//账号状态 0 禁用 1 激活 2 未激活

    AccountStatus(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
