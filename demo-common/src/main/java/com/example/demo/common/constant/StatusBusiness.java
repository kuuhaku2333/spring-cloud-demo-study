package com.example.demo.common.constant;

import com.example.demo.common.exception.BusinessExceptionAssert;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 业务状态码
 * Created by penglee on 2019/8/2.
 */
@AllArgsConstructor
@Getter
public enum StatusBusiness implements BusinessExceptionAssert {

    SUCCESS(100010200, "成功"),
    EMPTY(100010201, "不存在更多数据"),
    UNAUTH(100010401, "未授权"),
    INVALIDATE(100010402, "无效参数"),
    AUTHFAILED(100010403, "身份验证失败"),
    RESOURCES_IS_NULL(100010404, "资源不存在"),
    TIME_OUT(100010408, "请求超时"),
    FAILD(100010500, "服务器处理异常"),
    GATEWAY_AUTHFAILED(100010601, "网关身份验证异常"),
    UNAVAILABLE(100010503, "服务中断"),
    BASE(100010700, "基础服务异常"),

    UPDATE_EXCEPTION(100010001, "更新失败"),
    DELETE_EXCEPTION(100010002, "删除失败"),
    INSERT_EXCEPTION(100010003, "新增失败"),
    BATCH_QUERY_EXCEPTION(100010004, "批量查询条件参数不能超过1000个"),
    PASSWORD_DECRYPT_EXCEPTION(100010005, "密码解密失败"),
    RSA_PUBLIC_KEY_EXCEPTION(100010006, "加密公钥已过期，请重新获取");

    /**
     * 返回码
     */
    private int status;
    /**
     * 返回消息
     */
    private String message;

}
