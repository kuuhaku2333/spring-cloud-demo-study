package com.example.demo.common.exception;/**
 * Created by penglee on 2019/7/2.
 */

import lombok.Data;

/**
 * BaseException
 * <p>
 * Description: <br>
 * Created in 2019/7/2 09:56 <br>
 *
 * @author FN
 */
@Data
public class BaseException extends RuntimeException {
    private int status;
    private String message;

    public BaseException(IResponseEnum iResponseEnum) {
        this.status = iResponseEnum.getStatus();
        this.message = iResponseEnum.getMessage();
    }

    public BaseException(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public BaseException(String message, int status, String message1) {
        super(message);
        this.status = status;
        this.message = message1;
    }

    public BaseException(String message, Throwable cause, int status, String message1) {
        super(message, cause);
        this.status = status;
        this.message = message1;
    }

    public BaseException(Throwable cause, int status, String message) {
        super(cause);
        this.status = status;
        this.message = message;
    }

    public BaseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, int status, String message1) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.status = status;
        this.message = message1;
    }
}
