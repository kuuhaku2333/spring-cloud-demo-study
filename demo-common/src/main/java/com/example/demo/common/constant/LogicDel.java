package com.example.demo.common.constant;

/**
 * @program: xy-framework
 * @description: 逻辑删除
 * @author: FN
 * @create: 2019-12-02
 **/
public enum  LogicDel {

    NORMAL(0),DEL(1);

    private int code;//0 正常  1删除

    LogicDel(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
