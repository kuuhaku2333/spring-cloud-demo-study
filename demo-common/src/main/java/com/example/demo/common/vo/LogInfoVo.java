package com.example.demo.common.vo;/**
 * Created by penglee on 2019/10/10.
 */

import lombok.Data;

import java.io.Serializable;

/**
 * LogInfoVo
 * <p>
 * Description: <br> 该接口尽量使用JSON字符串，以便全日制分析。
 * Created in 2019/10/10 10:33 <br>
 *
 * @author 李鹏
 */
@Data
public class LogInfoVo implements Serializable {

    private String user;

    private String website;

    private String menu;

    private String opt;

    private String uri;

    private String result;
}
