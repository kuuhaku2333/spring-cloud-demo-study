//package com.example.demo.common.algorithm;/**
// * Created by penglee on 2019/7/25.
// */
//
//import java.math.BigInteger;
//
///**
// * Util
// * <p>
// * Description: <br>
// * Created in 2019/7/25 11:38 <br>
// *
// * @author 李鹏
// */
//public class Util {
//
//    /**
//     * Description: intToBytes
//     * Params: [num 一个整型数据]
//     * Return: byte[] 4个字节的自己数组
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static byte[] intToBytes(int num) {
//        byte[] bytes = new byte[4];
//        bytes[0] = (byte) (0xff & (num >> 0));
//        bytes[1] = (byte) (0xff & (num >> 8));
//        bytes[2] = (byte) (0xff & (num >> 16));
//        bytes[3] = (byte) (0xff & (num >> 24));
//        return bytes;
//    }
//
//    /**
//     * Description: byteToInt 四个字节的字节数据转换成一个整形数据
//     * Params: [bytes] 4个字节的字节数组
//     * Return: int 一个整型数据
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static int byteToInt(byte[] bytes) {
//        int num = 0;
//        int temp;
//        temp = (0x000000ff & (bytes[0])) << 0;
//        num = num | temp;
//        temp = (0x000000ff & (bytes[1])) << 8;
//        num = num | temp;
//        temp = (0x000000ff & (bytes[2])) << 16;
//        num = num | temp;
//        temp = (0x000000ff & (bytes[3])) << 24;
//        num = num | temp;
//        return num;
//    }
//
//    /**
//     * Description: longToBytes 长整形转换成网络传输的字节流（字节数组）型数据
//     * Params: [num] 一个长整型数据
//     * Return: byte[] 4个字节的自己数组
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static byte[] longToBytes(long num) {
//        byte[] bytes = new byte[8];
//        for (int i = 0; i < 8; i++) {
//            bytes[i] = (byte) (0xff & (num >> (i * 8)));
//        }
//
//        return bytes;
//    }
//
//    /**
//     * Description: byteConvert32Bytes 大数字转换字节流（字节数组）型数据
//     * Params: [n]
//     * Return: byte[]
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static byte[] byteConvert32Bytes(BigInteger n) {
//        byte[] tmpd = null;
//        if (n == null) {
//            return null;
//        }
//
//        if (n.toByteArray().length == 33) {
//            tmpd = new byte[32];
//            System.arraycopy(n.toByteArray(), 1, tmpd, 0, 32);
//        } else if (n.toByteArray().length == 32) {
//            tmpd = n.toByteArray();
//        } else {
//            tmpd = new byte[32];
//            for (int i = 0; i < 32 - n.toByteArray().length; i++) {
//                tmpd[i] = 0;
//            }
//            System.arraycopy(n.toByteArray(), 0, tmpd, 32 - n.toByteArray().length, n.toByteArray().length);
//        }
//        return tmpd;
//    }
//
//    /**
//     * Description: byteConvertInteger  换字节流（字节数组）型数据转大数字
//     * Params: [b]
//     * Return: java.math.BigInteger
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static BigInteger byteConvertInteger(byte[] b) {
//        if (b[0] < 0) {
//            byte[] temp = new byte[b.length + 1];
//            temp[0] = 0;
//            System.arraycopy(b, 0, temp, 1, b.length);
//            return new BigInteger(temp);
//        }
//        return new BigInteger(b);
//    }
//
//    /**
//     * Description: getHexString 根据字节数组获得值(十六进制数字)
//     * Params: [bytes]
//     * Return: java.lang.String
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static String getHexString(byte[] bytes) {
//        return getHexString(bytes, true);
//    }
//
//    /**
//     * Description: getHexString 根据字节数组获得值(十六进制数字)
//     * Params: [bytes, upperCase]
//     * Return: java.lang.String
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static String getHexString(byte[] bytes, boolean upperCase) {
//        String ret = "";
//        for (int i = 0; i < bytes.length; i++) {
//            ret += Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1);
//        }
//        return upperCase ? ret.toUpperCase() : ret;
//    }
//
//    /**
//     * Description: printHexString 打印十六进制字符串
//     * Params: [bytes]
//     * Return: void
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static void printHexString(byte[] bytes) {
//        for (int i = 0; i < bytes.length; i++) {
//            String hex = Integer.toHexString(bytes[i] & 0xFF);
//            if (hex.length() == 1) {
//                hex = '0' + hex;
//            }
//            System.out.print("0x" + hex.toUpperCase() + "," );
//        }
//        System.out.println();
//    }
//
//    /**
//     * Description: hexStringToBytes Convert hex string to byte[]
//     * Params: [hexString] the hex string
//     * Return: byte[]
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static byte[] hexStringToBytes(String hexString) {
//        if (hexString == null || hexString.equals("" )) {
//            return null;
//        }
//
//        hexString = hexString.toUpperCase();
//        int length = hexString.length() / 2;
//        char[] hexChars = hexString.toCharArray();
//        byte[] d = new byte[length];
//        for (int i = 0; i < length; i++) {
//            int pos = i * 2;
//            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
//        }
//        return d;
//    }
//
//    /**
//     * Description: charToByte Convert char to byte
//     * Params: [c] char
//     * Return: byte
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static byte charToByte(char c) {
//        return (byte) "0123456789ABCDEF".indexOf(c);
//    }
//
//    /**
//     * 用于建立十六进制字符的输出的小写字符数组
//     */
//    private static final char[] DIGITS_LOWER = {'0', '1', '2', '3', '4', '5',
//            '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
//
//    /**
//     * 用于建立十六进制字符的输出的大写字符数组
//     */
//    private static final char[] DIGITS_UPPER = {'0', '1', '2', '3', '4', '5',
//            '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
//
//    /**
//     * Description: encodeHex 将字节数组转换为十六进制字符数组
//     * Params: [data]
//     * Return: char[] 十六进制
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static char[] encodeHex(byte[] data) {
//        return encodeHex(data, true);
//    }
//
//    /**
//     * Description: encodeHex 将字节数组转换为十六进制字符数组
//     * Params: [data byte[], toLowerCase {true:"传换成小写格式",false:"传换成大写格式"}]
//     * Return: char[] 十六进制
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static char[] encodeHex(byte[] data, boolean toLowerCase) {
//        return encodeHex(data, toLowerCase ? DIGITS_LOWER : DIGITS_UPPER);
//    }
//
//    /**
//     * Description: encodeHex 将字节数组转换为十六进制字符数组
//     * Params: [data byte[], toDigits 用于控制输出的char[]]
//     * Return: char[] 十六进制
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    protected static char[] encodeHex(byte[] data, char[] toDigits) {
//        int l = data.length;
//        char[] out = new char[l << 1];
//        // two characters form the hex value.
//        for (int i = 0, j = 0; i < l; i++) {
//            out[j++] = toDigits[(0xF0 & data[i]) >>> 4];
//            out[j++] = toDigits[0x0F & data[i]];
//        }
//        return out;
//    }
//
//    /**
//     * Description: encodeHexString 将字节数组转换为十六进制字符串
//     * Params: [data byte[]]
//     * Return: java.lang.String 十六进制
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static String encodeHexString(byte[] data) {
//        return encodeHexString(data, true);
//    }
//
//    /**
//     * Description: encodeHexString 将字节数组转换为十六进制字符串
//     * Params: [data byte[], toLowerCase {true:"传换成小写格式",false:"传换成大写格式"}]
//     * Return: java.lang.String 十六进制
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static String encodeHexString(byte[] data, boolean toLowerCase) {
//        return encodeHexString(data, toLowerCase ? DIGITS_LOWER : DIGITS_UPPER);
//    }
//
//    /**
//     * Description: encodeHexString 将字节数组转换为十六进制字符串
//     * Params: [data byte[], toDigits 用于控制输出的char[]]
//     * Return: java.lang.String 十六进制
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    protected static String encodeHexString(byte[] data, char[] toDigits) {
//        return new String(encodeHex(data, toDigits));
//    }
//
//    /**
//     * Description: decodeHex 将十六进制字符数组转换为字节数组
//     * Params: [data 十六进制char[]]
//     * Return: byte[]
//     * Throws: RuntimeException 如果源十六进制字符数组是一个奇怪的长度，将抛出运行时异常
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static byte[] decodeHex(char[] data) {
//        int len = data.length;
//
//        if ((len & 0x01) != 0) {
//            throw new RuntimeException("Odd number of characters." );
//        }
//
//        byte[] out = new byte[len >> 1];
//
//        // two characters form the hex value.
//        for (int i = 0, j = 0; j < len; i++) {
//            int f = toDigit(data[j], j) << 4;
//            j++;
//            f = f | toDigit(data[j], j);
//            j++;
//            out[i] = (byte) (f & 0xFF);
//        }
//
//        return out;
//    }
//
//    /**
//     * Description: toDigit
//     * Params: [ch 十六进制char, index 十六进制字符在字符数组中的位置]
//     * Return: int
//     * Throws: RuntimeException 当ch不是一个合法的十六进制字符时，抛出运行时异常
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    protected static int toDigit(char ch, int index) {
//        int digit = Character.digit(ch, 16);
//        if (digit == -1) {
//            throw new RuntimeException("Illegal hexadecimal character " + ch
//                    + " at index " + index);
//        }
//        return digit;
//    }
//
//    /**
//     * Description: StringToAsciiString 数字字符串转ASCII码字符串
//     * Params: [content] 字符串
//     * Return: java.lang.String ASCII字符串
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static String StringToAsciiString(String content) {
//        String result = "";
//        int max = content.length();
//        for (int i = 0; i < max; i++) {
//            char c = content.charAt(i);
//            String b = Integer.toHexString(c);
//            result = result + b;
//        }
//        return result;
//    }
//
//    /**
//     * Description: hexStringToString 十六进制转字符串
//     * Params: [hexString 十六进制字符串, encodeType 编码类型:{4:"Unicode"，2:"普通编码"}]
//     * Return: java.lang.String  字符串
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static String hexStringToString(String hexString, int encodeType) {
//        String result = "";
//        int max = hexString.length() / encodeType;
//        for (int i = 0; i < max; i++) {
//            char c = (char) hexStringToAlgorism(hexString
//                    .substring(i * encodeType, (i + 1) * encodeType));
//            result += c;
//        }
//        return result;
//    }
//
//    /**
//     * Description: hexStringToAlgorism 十六进制字符串装十进制
//     * Params: [hex 十六进制字符串]
//     * Return: int 十进制数值
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static int hexStringToAlgorism(String hex) {
//        hex = hex.toUpperCase();
//        int max = hex.length();
//        int result = 0;
//        for (int i = max; i > 0; i--) {
//            char c = hex.charAt(i - 1);
//            int algorism = 0;
//            if (c >= '0' && c <= '9') {
//                algorism = c - '0';
//            } else {
//                algorism = c - 55;
//            }
//            result += Math.pow(16, max - i) * algorism;
//        }
//        return result;
//    }
//
//    /**
//     * Description: hexStringToBinary 十六转二进制
//     * Params: [hex 十六进制字符串]
//     * Return: java.lang.String 二进制字符串
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static String hexStringToBinary(String hex) {
//        hex = hex.toUpperCase();
//        String result = "";
//        int max = hex.length();
//        for (int i = 0; i < max; i++) {
//            char c = hex.charAt(i);
//            switch (c) {
//                case '0':
//                    result += "0000";
//                    break;
//                case '1':
//                    result += "0001";
//                    break;
//                case '2':
//                    result += "0010";
//                    break;
//                case '3':
//                    result += "0011";
//                    break;
//                case '4':
//                    result += "0100";
//                    break;
//                case '5':
//                    result += "0101";
//                    break;
//                case '6':
//                    result += "0110";
//                    break;
//                case '7':
//                    result += "0111";
//                    break;
//                case '8':
//                    result += "1000";
//                    break;
//                case '9':
//                    result += "1001";
//                    break;
//                case 'A':
//                    result += "1010";
//                    break;
//                case 'B':
//                    result += "1011";
//                    break;
//                case 'C':
//                    result += "1100";
//                    break;
//                case 'D':
//                    result += "1101";
//                    break;
//                case 'E':
//                    result += "1110";
//                    break;
//                case 'F':
//                    result += "1111";
//                    break;
//            }
//        }
//        return result;
//    }
//
//    /**
//     * Description: AsciiStringToString ASCII码字符串转数字字符串
//     * Params: [content] ASCII字符串
//     * Return: java.lang.String 字符串
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static String AsciiStringToString(String content) {
//        String result = "";
//        int length = content.length() / 2;
//        for (int i = 0; i < length; i++) {
//            String c = content.substring(i * 2, i * 2 + 2);
//            int a = hexStringToAlgorism(c);
//            char b = (char) a;
//            String d = String.valueOf(b);
//            result += d;
//        }
//        return result;
//    }
//
//    /**
//     * Description: algorismToHexString 将十进制转换为指定长度的十六进制字符串
//     * Params: [algorism 十进制数字, maxLength 转换后的十六进制字符串长度]
//     * Return: java.lang.String 转换后的十六进制字符串
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static String algorismToHexString(int algorism, int maxLength) {
//        String result = "";
//        result = Integer.toHexString(algorism);
//
//        if (result.length() % 2 == 1) {
//            result = "0" + result;
//        }
//        return patchHexString(result.toUpperCase(), maxLength);
//    }
//
//    /**
//     * Description: byteToString 字节数组转为普通字符串（ASCII对应的字符）
//     * Params: [bytearray byte[]]
//     * Return: java.lang.String
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static String byteToString(byte[] bytearray) {
//        String result = "";
//        char temp;
//
//        int length = bytearray.length;
//        for (int i = 0; i < length; i++) {
//            temp = (char) bytearray[i];
//            result += temp;
//        }
//        return result;
//    }
//
//    /**
//     * Description: binaryToAlgorism 二进制字符串转十进制
//     * Params: [binary 二进制字符串]
//     * Return: int 十进制数值
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static int binaryToAlgorism(String binary) {
//        int max = binary.length();
//        int result = 0;
//        for (int i = max; i > 0; i--) {
//            char c = binary.charAt(i - 1);
//            int algorism = c - '0';
//            result += Math.pow(2, max - i) * algorism;
//        }
//        return result;
//    }
//
//    /**
//     * Description: algorismToHEXString 十进制转换为十六进制字符串
//     * Params: [algorism 十进制的数字]
//     * Return: java.lang.String 对应的十六进制字符串
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static String algorismToHEXString(int algorism) {
//        String result = "";
//        result = Integer.toHexString(algorism);
//
//        if (result.length() % 2 == 1) {
//            result = "0" + result;
//
//        }
//        result = result.toUpperCase();
//
//        return result;
//    }
//
//    /**
//     * Description: patchHexString HEX字符串前补0，主要用于长度位数不足。
//     * Params: [str 需要补充长度的十六进制字符串, maxLength 补充后十六进制字符串的长度]
//     * Return: java.lang.String 补充结果
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    static public String patchHexString(String str, int maxLength) {
//        String temp = "";
//        for (int i = 0; i < maxLength - str.length(); i++) {
//            temp = "0" + temp;
//        }
//        str = (temp + str).substring(0, maxLength);
//        return str;
//    }
//
//    /**
//     * Description: parseToInt 将一个字符串转换为int
//     * Params: [s 要转换的字符串, defaultInt 如果出现异常,默认返回的数字, radix 要转换的字符串是什么进制的,如16 8 10.]
//     * Return: int 转换后的数字
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static int parseToInt(String s, int defaultInt, int radix) {
//        int i = 0;
//        try {
//            i = Integer.parseInt(s, radix);
//        } catch (NumberFormatException ex) {
//            i = defaultInt;
//        }
//        return i;
//    }
//
//    /**
//     * Description: parseToInt 将一个十进制形式的数字字符串转换为int
//     * Params: [s 要转换的字符串, defaultInt 如果出现异常,默认返回的数字]
//     * Return: int 转换后的数字
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static int parseToInt(String s, int defaultInt) {
//        int i = 0;
//        try {
//            i = Integer.parseInt(s);
//        } catch (NumberFormatException ex) {
//            i = defaultInt;
//        }
//        return i;
//    }
//
//    /**
//     * Description: hexToByte 十六进制串转化为byte数组
//     * Params: [hex]
//     * Return: byte[]
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static byte[] hexToByte(String hex)
//            throws IllegalArgumentException {
//        if (hex.length() % 2 != 0) {
//            throw new IllegalArgumentException();
//        }
//        char[] arr = hex.toCharArray();
//        byte[] b = new byte[hex.length() / 2];
//        for (int i = 0, j = 0, l = hex.length(); i < l; i++, j++) {
//            String swap = "" + arr[i++] + arr[i];
//            int byteint = Integer.parseInt(swap, 16) & 0xFF;
//            b[j] = new Integer(byteint).byteValue();
//        }
//        return b;
//    }
//
//    /**
//     * Description: byteToHex 字节数组转换为十六进制字符串
//     * Params: [b byte[]需要转换的字节数组]
//     * Return: java.lang.String 十六进制字符串
//     * CreateDate: Created in
//     * Modify:
//     * Author: 李鹏
//     */
//    public static String byteToHex(byte[] b) {
//        if (b == null) {
//            throw new IllegalArgumentException(
//                    "Argument b ( byte array ) is null! " );
//        }
//        String hs = "";
//        String stmp = "";
//        for (int n = 0; n < b.length; n++) {
//            stmp = Integer.toHexString(b[n] & 0xff);
//            if (stmp.length() == 1) {
//                hs = hs + "0" + stmp;
//            } else {
//                hs = hs + stmp;
//            }
//        }
//        return hs.toUpperCase();
//    }
//
//    public static byte[] subByte(byte[] input, int startIndex, int length) {
//        byte[] bt = new byte[length];
//        for (int i = 0; i < length; i++) {
//            bt[i] = input[i + startIndex];
//        }
//        return bt;
//    }
//
//}
