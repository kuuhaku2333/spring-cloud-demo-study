package com.example.demo.common.constant;

/**
 * @program: xy-framework
 * @description: 账号性质
 * @author: FN
 * @create: 2019-12-02
 **/
public enum  AccountNature {

    PERSONAL(0,"个人"),ENTERPRISE(1,"企业");
    private int code;
    private String msg;

    AccountNature() {
    }

    AccountNature(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
