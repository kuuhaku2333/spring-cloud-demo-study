package com.example.demo.common.constant;

/**
 * @program: xy-framework
 * @description: 系统状态
 * @author: FN
 * @create: 2019-12-03
 **/
public enum Status {

    DISABLE(0), ENABLE(1);
    private int code;//0 禁用 1 启用

    Status(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
