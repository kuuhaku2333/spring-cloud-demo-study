package com.example.demo.common.vo;/**
 * Created by penglee on 2019/9/4.
 */

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

/**
 * PageCondition
 * <p>
 * Description: <br>
 * Created in 2019/9/4 16:27 <br>
 *
 * @author 李鹏
 */
@Data
public class PageCondition<T> {
    private Page<T> page;
    private T condition;
}
