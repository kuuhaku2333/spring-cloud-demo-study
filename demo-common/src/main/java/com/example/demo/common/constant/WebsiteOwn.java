package com.example.demo.common.constant;

/**
 * @program: xy-framework
 * @description: 站点归属
 * @author: FN
 * @create: 2019-12-04
 **/
public enum WebsiteOwn {

    OTHER(0), OWN(1);


    private int code;//站点归属 0 三方 1 自有

    WebsiteOwn(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
