package com.example.demo.common.exception;

/**
 * @program: xy-framework
 * @description: 业务异常处理工具
 * @author: FN
 * @create: 2020-04-30
 **/
public class BusinessException extends BaseException {

    private static final long serialVersionUID = 1L;

    public BusinessException() {
        super(500, "服务繁忙，请重试");
    }

    public BusinessException(int status, String message) {
        super(status, message);
    }

    public BusinessException(IResponseEnum responseEnum) {
        super(responseEnum);
    }

}