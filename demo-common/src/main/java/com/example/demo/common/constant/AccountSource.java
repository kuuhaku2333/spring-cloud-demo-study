package com.example.demo.common.constant;

/**
 * 账号来源
 */
public enum AccountSource {

    OWN(0, "自有" ),OUTSIDE(1,"河南省政务平台"),SYS(2,"系统用户");

    private int code;
    private String msg;

    AccountSource() {
    }

    AccountSource(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
