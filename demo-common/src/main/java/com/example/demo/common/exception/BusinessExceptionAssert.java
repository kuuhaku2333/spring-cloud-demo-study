package com.example.demo.common.exception;

/**
 * @program: xy-framework
 * @description:
 * @author: FN
 * @create: 2020-04-30
 **/
public interface BusinessExceptionAssert extends IResponseEnum, Assert {

    @Override
    default BaseException BusinessException(Object... args) {
        return new BusinessException(this.getStatus(), this.getMessage());
    }

    @Override
    default BaseException BusinessException() {
        return new BusinessException(this);
    }

}