package com.example.demo;
import com.dtflys.forest.springboot.annotation.ForestScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@ForestScan(basePackages = "com.example.demo.service")
public class SeckillApplication {

    /**
     *  秒杀系统服务
     **/
    public static void main(String[] args) {
        SpringApplication.run(SeckillApplication.class, args);
        System.out.println("秒杀系统启动完成！");
    }
}
