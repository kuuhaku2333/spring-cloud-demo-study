package com.example.demo.service;

import com.example.demo.entity.User;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @date： 2021/12/23
 * @author: wbx
 */
@Service
public class SeckillService implements ApplicationRunner {
    @Resource
    private SeckillInterface seckillInterface;



    @Override
    public void run(ApplicationArguments args) throws Exception {
        String cookie = "ASP.NET_SessionId=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2NDAyMjM4MTQuODE3OTI3MSwiZXhwIjoxNjQwMjI3NDE0LjgxNzkyNzEsInN1YiI6IllOVy5WSVAiLCJqdGkiOiIyMDIxMTIyMzA5NDMzNCIsInZhbCI6IitDY2hBUUlBQUFBUU1tUXpZbVV4T0RRNU5URmxOREl5WXh4dmNYSTFielZDVWtwcGJIRnJUbGxGTTBsRmJHcDBXVVJmZFV4ckFCeHZcclxuVlRJMldIUXlNV3hLVGxCSFMyd3dkMUU1UzJaUGVtbEpNRXhaRGpJeE9DNHlPUzR4TmpjdU1qTXdBQUFBQUFBQUFBPT0ifQ.DwFtk0UGYLRr7kDytuuYripS8h-L-yUXtbEBmrqoq50";
        User userInfo = seckillInterface.getUserInfo(cookie);
        System.out.println(userInfo);
    }
}
