package com.example.demo.service;

import com.dtflys.forest.annotation.*;
import com.example.demo.conf.HttpSimpleInterceptor;
import com.example.demo.entity.User;
import org.springframework.stereotype.Component;

@Component
@BaseRequest(interceptor = HttpSimpleInterceptor.class)//配置拦截器，主要用于解决请求错误异常
public interface SeckillInterface {

    public final String common_url = "https://cloud.cn2030.com/sc/wx/HandlerSubscribe.ashx?act=";


    @Get(
            url = common_url + "User",
//            contentType = "application/json",
            headers = {
                    "Cookie: ${cookie}",
//                    "Type:application/json",
//                    "zftsl:136fe6736b751b3f44b09162af9a2e17/json",
//                    "User-Agent:Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36 MicroMessenger/7.0.9.501 NetType/WIFI MiniProgramEnv/Windows WindowsWechat",
//                    "Referer:https://servicewechat.com/wx2c7f0f3c30d99445/91/page-frame.html",
//                    "Host:cloud.cn2030.com",
//                    "Connection:keep-alive",
//                    "Type:application/json",
            }
    )
    User getUserInfo(@Var("cookie") String cookie);


}
