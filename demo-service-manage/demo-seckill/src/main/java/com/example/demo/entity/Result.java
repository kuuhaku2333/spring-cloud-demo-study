package com.example.demo.entity;

import lombok.Data;

@Data
public class Result {

    /**
     * 状态
     * status : 200
     */
    protected int status;

    protected String msg;

}
