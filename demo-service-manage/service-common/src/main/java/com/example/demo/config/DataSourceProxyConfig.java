package com.example.demo.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * 手动配置数据源
 */
@Configuration
public class DataSourceProxyConfig {
	
	@Autowired(required = true)
	private DataSourceProperties dataSourceProperties;
	
	@Bean
	@Primary
	public DataSource dataSource() {
		HikariDataSource hikariDataSource = dataSourceProperties
				.initializeDataSourceBuilder()
				.type(HikariDataSource.class)
				.build();
		return hikariDataSource;
	}
}
