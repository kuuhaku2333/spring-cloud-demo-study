package com.example.demo.interceptor;

import com.example.demo.aop.ContextDadaHolder;
import com.example.demo.common.constant.CommonConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description 跨域拦截器
 **/
@Slf4j
@Component
public class CorsIntercepteor extends HandlerInterceptorAdapter {

    //在请求处理之前进行调用Controller方法调用之前
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //响应头缺失
        response.setHeader("X-Frame-Options", "SAMEORIGIN;"); // X-Frame-Options 响应头缺失
        response.setHeader("Referer-Policy", "origin;");//Referer-Policy 响应头缺失
        response.setHeader("Content-Security-Policy", "frame-ancestors 'self';");//Content-Security-Policy 响应头缺失
        response.setHeader("X-Permitted-Cross-Domain-Policies", "'master-only';");//X-Permitted-Cross-Domain-Policies 响应头缺失
        response.setHeader("X-XSS-Protection", "1; mode=block;");//X-XSS-Protection 响应头缺失
        response.setHeader("X-Download-Options", " SAMEORIGIN;");//X-Download-Options 响应头缺失
        response.setHeader("X-Content-Type-Options", "nosniff;");//X-Content-Type-Options 响应头缺失
        response.setHeader("Strict-Transport-Security", "max-age=31536000;");//Strict-Transport-Security 响应头缺失

        //拦截逻辑
        String myOrigin = request.getHeader("Origin");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Origin", myOrigin);
        response.setHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS");
        response.setHeader("Access-Control-Max-Age", "86400");
        response.setHeader("Access-Control-Allow-Headers", "XMLHttpRequest,accessToken,content-type,origin,x-requested-with,user-agent,Authorization,FormId");
        /* response.setHeader("Access-Control-Allow-Headers", "Content-Type, x-requested-with, X-Custom-Header, Authorization");*/
        /*防止用户注销以后点击浏览器后退，从缓存中读取数据*/
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        // 如果是OPTIONS则结束请求
        if (HttpMethod.OPTIONS.toString().equals(request.getMethod())) {
            response.setStatus(HttpStatus.NO_CONTENT.value());
            return false;
        }
//        if (response.getStatus() == 500) {
//            // 重定向进来
//            throw new BusinessException(StatusBusiness.FAILD);
//        } else if (response.getStatus() == 404) {
//            // 重定向进来
//            throw new BusinessException(StatusBusiness.RESOURCES_IS_NULL);
//        }
        setContextDadaInfo(request);
        return super.preHandle(request, response, handler);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
        ContextDadaHolder.clear();
    }

    private void setContextDadaInfo(HttpServletRequest request) {
        // 请求的唯一id
        ContextDadaHolder.buildRequestUuid();
        // 请求的路径
        String requestURI = request.getRequestURI();
        ContextDadaHolder.putRequestUrl(requestURI);
        // 请求的所属模块编号
        int moduleCode = 0;
        if (requestURI.startsWith(CommonConstants.MODULE_PRODUCT_PREFIX)) {
            moduleCode = CommonConstants.PRODUCT_STATUS_CODE;
        } else if (requestURI.startsWith(CommonConstants.MODULE_CONSUMER_PREFIX)) {
            moduleCode = CommonConstants.CONSUMER_PUB_STATUS_CODE;
        }
        /*
        * else if (requestURI.startsWith(CommonConstants.MODULE_DZZJBF_URL_PREFIX)) {
            moduleCode = CommonConstants.MODULE_DZZJBF_STATUS_CODE;
        } else if (requestURI.startsWith(CommonConstants.MODULE_DZCLQF_URL_PREFIX)) {
            moduleCode = CommonConstants.MODULE_DZCLQF_STATUS_CODE;
        } else if (requestURI.startsWith(CommonConstants.MODULE_DZCLYY_URL_PREFIX)) {
            moduleCode = CommonConstants.MODULE_DZCLYY_STATUS_CODE;
        } else {
            moduleCode = CommonConstants.MODULE_UNKNOWN_STATUS_CODE;
        }
        * */
        ContextDadaHolder.putModuleCode(moduleCode);
    }
}
