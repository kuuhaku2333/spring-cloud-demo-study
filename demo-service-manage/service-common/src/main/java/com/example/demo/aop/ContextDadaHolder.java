package com.example.demo.aop;

import com.example.demo.util.common.UUIDUtil;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 用户上下文
 */
public class ContextDadaHolder {

	private static ThreadLocal<Map<String, String>> threadLocal = new ThreadLocal<>();

	/**
	 * 用户上下文中放入信息
	 *
	 * @param map
	 */
	public static void setMapData(Map<String, String> map) {
		if (map == null) {
			map = new HashMap<>();
		}
		threadLocal.set(map);
	}

	/**
	 * 获取上下文中的信息
	 *
	 * @return
	 */
	public static Map<String, String> getMapData() {
		Map<String, String> map = threadLocal.get();
		if (map == null) {
			map = new HashMap<>();
			threadLocal.set(map);
		}
		return map;
	}

	// 获取模块信息
	public static int getModuleCode() {
		String code = getMapData().get("module_code");
		return Integer.parseInt(StringUtils.defaultIfBlank(code, "0"));
	}

	// 设置模块信息
	public static String putModuleCode(int moduleCode) {
		return getMapData().put("module_code", String.valueOf(moduleCode));
	}

	public static String putRequestUrl(String requestURI) {
		return getMapData().put("request_url", requestURI);
	}

	public static String getRequestUrl() {
		return getMapData().get("request_url");
	}

	public static String buildRequestUuid() {
		return getMapData().put("request_uuid", UUIDUtil.getShortUuid());
	}

	public static String getRequestUuid() {
		return getMapData().get("request_uuid");
	}


//	// 设置用户信息
//	public static String putUserData(UserInfo user) {
//		if (user == null) {
//			user = new UserInfo();
//		}
//		return getMapData().put("user_data", GsonUtil.GsonString(user));
//	}
//
//	public static UserInfo getUserData() {
//		String userStr = getMapData().get("user_data");
//		UserInfo user;
//		if (StringUtils.isBlank(userStr)) {
//			user = new UserInfo();
//		} else {
//			user = GsonUtil.GsonToBean(userStr, UserInfo.class);
//		}
//		return user;
//	}


	/**
	 * 清空上下文
	 */
	public static void clear() {
		threadLocal.remove();
	}


}
