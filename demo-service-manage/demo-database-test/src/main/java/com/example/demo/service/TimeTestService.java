package com.example.demo.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.TimeTestPo;

/**
 * (TimeTest)表服务接口
 *
 * @author wbx
 * @since 2022-03-01 15:43:49
 */
public interface TimeTestService extends IService<TimeTestPo> {

    /**
     * 分页查询所有数据
     *
     * @return TimeTest分页数据
     */
    IPage<TimeTestPo> getPageInfo();


    /**
     * 通过主键查询单条
     *
     * @param id 主键
     * @return
     */
    TimeTestPo getTimeTestById(String id);


    TimeTestPo addTimeTest();


}
