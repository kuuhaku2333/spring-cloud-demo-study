package com.example.demo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;


@SpringBootApplication
@MapperScan(basePackages = {"com.example.demo.mapper"})
public class DataBaseApplication {



    /**
     *  数据库服务
     **/
    public static void main(String[] args) {
        SpringApplication.run(DataBaseApplication.class, args);
        System.out.println("数据库服务启动成功，访问 " + "127.0.0.1:" +  ServerPort.port + "/");
    }

    // 静态变量注入 property里的值 需要加 @Component @Value("${server.port}")要写在set方法上
    @Component
    public static class ServerPort {

        private static String port;

        @Value("${server.port}")
        public  void setPort(String port) {
            ServerPort.port = port;
        }
    }
}
