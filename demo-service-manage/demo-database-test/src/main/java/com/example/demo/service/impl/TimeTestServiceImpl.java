package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.TimeTestPo;
import com.example.demo.mapper.TimeTestMapper;
import com.example.demo.service.TimeTestService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * 日期测试实现类
 *
 * @date： 2022/3/1
 * @author: wbx
 */
@Service("timeTestService")
public class TimeTestServiceImpl extends ServiceImpl<TimeTestMapper,TimeTestPo> implements TimeTestService {
    @Override
    public IPage<TimeTestPo> getPageInfo() {
        return this.page(new Page<>(0 ,10));
    }

    @Override
    public TimeTestPo getTimeTestById(String id) {
        return getById(id);
    }

    @Override
    public TimeTestPo addTimeTest() {
        TimeTestPo timeTestPo = new TimeTestPo();
        timeTestPo.setLocalTime(LocalDateTime.now());
        timeTestPo.setStamp(String.valueOf(System.currentTimeMillis()));
        return (save(timeTestPo)) ? timeTestPo : null;
    }
}
