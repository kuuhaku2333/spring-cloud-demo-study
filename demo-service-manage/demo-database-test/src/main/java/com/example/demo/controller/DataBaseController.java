package com.example.demo.controller;

import com.example.demo.entity.AddcolumnParam;
import com.example.demo.entity.BatchForm;
import com.example.demo.service.DataBaseService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/dataBase")
public class DataBaseController {

    @Resource
    private DataBaseService dataBaseService;

    /**0
     *  获取数据库中所有的表名称
     */
    @GetMapping("/getTables")
    public List<String> getTables() {
        return dataBaseService.getTables();
    }


    /**
     *  获取数据库中所有的表名称 及表注释
     */
    @GetMapping("/getTablesAndComment")
    public List<Map<String, String>> getTablesAndComment(String schema) {
        return dataBaseService.getTablesAndComment(schema);
    }

    /**
     *  获取某张数据表的数据信息
     */
    @GetMapping("/getTableDetail")
    public List<Map<String,String>> getTableDetail(String schema, String tableName) {
        return dataBaseService.getTableDetail(schema, tableName);
    }

    /**
     *  动态向数据表中新增一行数据
     * @param addcolumnParam 提交信息
     * @return true 成功 / flase  失败
     */
    @PostMapping("/addColumn")
    public Boolean addColumn(@RequestBody AddcolumnParam addcolumnParam) {
        return dataBaseService.addColumn(addcolumnParam);
    }

    /**
     *  动态向数据表中新增一行数据
     * @param param 提交信息
     * @return true 成功 / flase  失败
     */
    @PostMapping("/addData")
    public Boolean addData(@RequestBody Map<String, String> param) {
        return dataBaseService.addData(param);
    }

    /**
     *  修改数据库中的一条数据
     * @param param 提交信息
     * @return true 成功 / flase  失败
     */
    @PostMapping("/modifyData")
    public Boolean modifyData(@RequestBody Map<String, String> param) {
        return dataBaseService.modifyData(param);
    }

    /**
     *  动态向数据表中新增多行数据
     * @param batchForm 提交信息
     * @return true 成功 / flase  失败
     */
    @PostMapping("/saveBatch")
    public Boolean saveBatch(@RequestBody BatchForm batchForm) {
        return dataBaseService.saveBatch(batchForm);
    }




}
