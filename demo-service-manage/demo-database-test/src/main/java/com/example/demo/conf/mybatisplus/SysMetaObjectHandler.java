//package com.example.demo.conf.mybatisplus;
//
//import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
//import com.xy.aq.sx.synergy.config.web.AccountHolder;
//import org.apache.ibatis.reflection.MetaObject;
//import org.springframework.stereotype.Component;
//
//import java.time.LocalDateTime;
//
///**
// * @program: xy-framework
// * @description: 系统字段默认填充配置
// * @author: FN
// * @create: 2019-12-10
// **/
//@Component
//public class SysMetaObjectHandler implements MetaObjectHandler {
//
//    @Override
//    public void insertFill(MetaObject metaObject) {
//        boolean result = metaObject.hasSetter("gmtCreated");
//        if (result) {
//            Object gmtCreated = getFieldValByName("gmtCreated", metaObject);
//            if (gmtCreated == null) {
//                setInsertFieldValByName("gmtCreated", account, metaObject);
//            }
//        }
//    }
//
//    @Override
//    public void updateFill(MetaObject metaObject) {
//        Object modifiedTime = getFieldValByName("modifiedTime", metaObject);
//        if (null == modifiedTime) {
//            setUpdateFieldValByName("modifiedTime", LocalDateTime.now(), metaObject);
//        }
//    }
//
//}
