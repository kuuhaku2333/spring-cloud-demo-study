package com.example.demo.mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @date： 2021/12/24
 * @author: wbx
 */
public interface DateBaseMapper {

    /**
     *  获取数据库中所有的表名称 及表注释
     * @param schema 数据库名称
     * @return 数据库中所有的表名称 及表注释
     */
//    @Select("SELECT TABLE_NAME,TABLE_COMMENT FROM information_schema.TABLES WHERE  table_schema=#{schema};")
    @Select("SELECT TABLE_NAME,TABLE_COMMENT FROM information_schema.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND table_schema=#{schema};")
    List<Map<String, String>> getTablesAndComment(String schema);

    /**
     *  获取数据库中所有的表
     */
    List<String> getTables();

    /**
     *  获取某张数据表的数据信息
     *  @param schema 数据库名称
     *  @param tableName 数据表名称
     */
    List<Map<String,String>> getTableDetail(@Param("schema") String schema,  @Param("tableName") String tableName);

    /**
     *  动态向数据表中新增一行数据
     * @param param 提交信息
     * @param tableName 数据表名称
     * @return true 成功 / flase  失败
     */
    Integer addData(@Param("param") Map<String, String> param, @Param("tableName") String tableName);

    /**
     *  修改数据库中的一条数据
     * @param param 提交信息
     * @param tableName 数据表名称
     * @param id 数据ID
     * @return true 成功 / flase  失败
     */
    Integer modifyData(@Param("param") Map<String, String> param, @Param("tableName") String tableName, @Param("id") String id);

}
