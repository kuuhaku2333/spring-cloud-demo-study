package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * (TimeTest)实体类 po
 *
 * @author wbx
 * @since 2022-03-01 15:42:21
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("time_test")
public class TimeTestPo {
    @TableId(type = IdType.AUTO)
    private Integer id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime localTime;

    private String stamp;

}
