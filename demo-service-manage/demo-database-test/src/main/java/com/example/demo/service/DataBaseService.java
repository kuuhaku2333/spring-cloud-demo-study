package com.example.demo.service;
import com.example.demo.entity.AddcolumnParam;
import com.example.demo.entity.BatchForm;

import java.util.List;
import java.util.Map;

public interface DataBaseService {

    List<String> getTables();

    List<Map<String, String>> getTablesAndComment(String schema);

    List<Map<String,String>> getTableDetail(String schema, String tableName);

    Boolean addColumn(AddcolumnParam addcolumnParam);

    /**
     *  动态向数据表中新增一行数据
     * @param param 提交信息
     * @return true 成功 / flase  失败
     */
    Boolean addData(Map<String, String> param);

    /**
     *  修改数据库中的一条数据
     * @param param 提交信息
     * @return true 成功 / flase  失败
     */
    Boolean modifyData(Map<String, String> param);

    /**
     *  动态向数据表中新增多行数据
     * @param batchForm 提交信息
     * @return true 成功 / flase  失败
     */
    Boolean saveBatch(BatchForm batchForm);
}
