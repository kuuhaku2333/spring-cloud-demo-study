package com.example.demo.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.example.demo.entity.TimeTestPo;
import com.example.demo.service.TimeTestService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (TimeTest)控制层
 *
 * @author wbx
 * @since 2022-03-01 15:43:49
 */
@RestController
@RequestMapping("/timeTest")
@Validated
public class TimeTestController extends ApiController {
    /**
     * 服务
     */
    @Resource
    private TimeTestService timeTestService;

    /**
     * 分页查询所有数据
     *
     * @return 分页列表
     */
    @PostMapping("/getPageInfo")
    public IPage<TimeTestPo> getPageInfo() {
        IPage<TimeTestPo> pageData = timeTestService.getPageInfo();
        return pageData;
    }


    /**
     * 通过主键查询单条
     *
     * @param id 主键
     * @return
     */
    @GetMapping("/getById")
    public TimeTestPo getById(@RequestParam  String id) {
        return timeTestService.getTimeTestById(id);
    }

    /**
     * 通过主键查询单条
     *
     * @return
     */
    @PostMapping("/addTimeTest")
    public TimeTestPo addTimeTest() {
        return timeTestService.addTimeTest();
    }


}
