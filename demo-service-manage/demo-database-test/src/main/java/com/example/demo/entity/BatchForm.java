package com.example.demo.entity;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @date： 2021/12/24
 * @author: wbx
 */
@Data
public class BatchForm {
    // 表名
    private String tableName;

    private List<Map<String,String>> data;
}
