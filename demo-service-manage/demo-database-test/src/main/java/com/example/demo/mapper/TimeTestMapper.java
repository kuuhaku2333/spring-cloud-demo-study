package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.TimeTestPo;

/**
 * (TimeTest)表数据库访问层
 *
 * @author wbx
 * @since 2022-03-01 15:43:49
 */
public interface TimeTestMapper extends BaseMapper<TimeTestPo> {

}
