package com.example.demo.entity;

import lombok.Data;

/**
 * @date： 2021/12/24
 * @author: wbx
 */
@Data
public class AddcolumnParam {
    // 表名
    private String tableName;
    // 列名
    private String columnName;
    // 字段类型
    private String type;
    // 字段备注
    private String comment;
    // 默认值字段
    private String defaultStr = " ";
}
