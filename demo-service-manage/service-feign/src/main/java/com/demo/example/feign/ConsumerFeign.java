package com.demo.example.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * AuthManagerFeignService
 * <p>
 * Description: <br>
 * Created in 2019/9/24 15:29 <br>
 *
 * @author FN
 */
@FeignClient(contextId = "ConsumerFeign", value = "product-service", path = "/product-server")
public interface ConsumerFeign {
    @GetMapping("/index")
    String getIndex();

}
