package com.demo.example.feign;


import com.example.demo.entity.po.Test1Po;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * AuthManagerFeignService
 * <p>
 * Description: <br>
 * Created in 2019/9/24 15:29 <br>
 *
 * @author FN
 */
@FeignClient(contextId = "Test1Feign", value = "product-service", path = "/product-server/test1")
public interface Test1Feign {

    @PostMapping("/test1Add")
    String test1Add(Test1Po test1Po);

}
