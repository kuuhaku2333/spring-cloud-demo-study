package com.example.demo.service;
import lombok.Data;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.PageModelPipeline;
import java.util.List;
@Data
public class MessageSpiederPipeline implements PageModelPipeline<MessageSpieder> {

    @Override
    public void process(MessageSpieder spieder, Task task) {
        List<String> titles = spieder.getTitle();
        List<String> abstractsList = spieder.getAbstracts();
        List<String> hrefs = spieder.getHref();
        List<String> dates = spieder.getDate();
        for (int i = 0; i < titles.size(); i++) {
            String title = titles.get(i);
            String href = hrefs.get(i);
            String date = dates.get(i);
            String abstracts = abstractsList.get(i);
            System.out.println(date + " " + title + " " + href + " " + abstracts);
        }
    }
}
