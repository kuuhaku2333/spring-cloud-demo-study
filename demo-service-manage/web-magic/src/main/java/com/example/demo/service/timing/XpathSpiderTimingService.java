package com.example.demo.service.timing;
import org.springframework.scheduling.annotation.Scheduled;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

//@Component
public class XpathSpiderTimingService implements PageProcessor {


    // 部分一：抓取网站的相关配置，包括编码、抓取间隔、重试次数等
    private Site site = Site.me()
            // 重试次数
            .setCycleRetryTimes(3)
            // 抓取间隔 ms
            .setSleepTime(1000);
    @Override
    // process是定制爬虫逻辑的核心接口，在这里编写抽取逻辑
    //  page 爬虫执行后返回的页面信息
    public void process(Page page) {
        // 获取page中的静态页面代码
        Html html = page.getHtml();
        // 1、选取 div标签 class=box-content 的元素
        // 2、选取当前div中的所有 li标签
        List<Selectable> selectables = html.xpath("//div[@class=box-content]//li").nodes();
        List<String> hrefs = new ArrayList<>();
        // 选择当前div的所有li标签
        selectables.forEach(item -> {
            // 链接 (a标签的href属性)
            String href = item.xpath("//a//@href").get();
            // 标题 (span标签中的文本)
            String title = item.xpath("//span//text()").get();
            // 文章发布时间(em标签里的文本内容)
            String date = item.xpath("//em//text()").get();
            // 文章摘要(p标签里的文本内容)
            String abstracts = item.xpath("//p//text()").get();
            hrefs.add(href);
//            page.addTargetRequest(href);
            System.out.println(date + " " + title + " " + href + " " + abstracts);
        });
    }

    @Override
    public Site getSite() {
        return site;
    }

    //    每隔5秒执行一次：*/5 * * * * ?
//
//    每隔1分钟执行一次：0 */1 * * * ?
//
//    每天23点执行一次：0 0 23 * * ?
//
//    每天凌晨1点执行一次：0 0 1 * * ?
//
//    每月1号凌晨1点执行一次：0 0 1 1 * ?
//
//    每月最后一天23点执行一次：0 0 23 L * ?
//
//    每周星期天凌晨1点实行一次：0 0 1 ? * L
//
//    在26分、29分、33分执行一次：0 26,29,33 * * * ?
//
//    每天的0点、13点、18点、21点都执行一次：0 0 0,13,18,21 * * ?
    @Scheduled(cron="*/5 * * * * ?")
    public void entrance () {
        System.out.println("执行静态态定时任务: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        Spider.create(new XpathSpiderTimingService())
                .addUrl("http://www.ccpit-henan.org/mcxw/index.jhtml")
                .thread(5)
                .run();
    }

}
