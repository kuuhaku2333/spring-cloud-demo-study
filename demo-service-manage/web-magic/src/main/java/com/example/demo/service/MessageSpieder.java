package com.example.demo.service;
import lombok.Data;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.model.OOSpider;
import us.codecraft.webmagic.model.annotation.ExtractBy;
import us.codecraft.webmagic.model.annotation.TargetUrl;

import java.util.List;

// 正则匹配目标url
@TargetUrl(value = "http://www.ccpit-henan.org/mcxw/index_[2-9].jhtml")
//@TargetUrl(value = "http://www.ccpit-henan.org/mcxw/index.jhtml")
@Data
public class MessageSpieder {

    @ExtractBy("//div[@class=box-content]//li//span//text()")
    List<String> title;
    @ExtractBy("//div[@class=box-content]//li//a//@href")
    List<String> href;
    @ExtractBy("//div[@class=box-content]//li//em//text()")
    List<String> date;
    @ExtractBy("//div[@class=box-content]//li//p//text()")
    List<String> abstracts;

    public static void main(String[] args) {
        Site site = Site.me().setCycleRetryTimes(3).setSleepTime(1000);
        OOSpider.create(site,
                // 输出Json格式文本文件
//                 new JsonFilePageModelPipeline("D:\\webmagic\\"),
                // 打印到控制台。
                new MessageSpiederPipeline(),
                MessageSpieder.class)
                .addUrl("http://www.ccpit-henan.org/mcxw/index.jhtml")
                .thread(3)
                .run();
    }
}
