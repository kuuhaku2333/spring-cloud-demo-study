package com.example.demo.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 实体类
 * @author: wbx
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SpiderEntity {
    private String href;
    private String date;
    private String title;
    private String abstracts;
}
