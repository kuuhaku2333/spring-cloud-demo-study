package com.example.demo.service;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import java.util.ArrayList;
import java.util.List;

/**
 * xpath提取内容网页爬虫
 */
public class XpathSpiderService implements PageProcessor {


    // 部分一：抓取网站的相关配置，包括编码、抓取间隔、重试次数等
    private Site site = Site.me()
            // 重试次数
            .setCycleRetryTimes(3)
            // 抓取间隔 ms
            .setSleepTime(1000);
    @Override
    // process是定制爬虫逻辑的核心接口，在这里编写抽取逻辑
    //  page 爬虫执行后返回的页面信息
    public void process(Page page) {
        // 获取page中的静态页面代码
        Html html = page.getHtml();
        // 1、选取 div标签 class=box-content 的元素
        // 2、选取当前div中的所有 li标签
        List<Selectable> selectables = html.xpath("//div[@class=box-content]//li").nodes();
        List<String> hrefs = new ArrayList<>();
        // 选择当前div的所有li标签
        selectables.forEach(item -> {
            // 链接 (a标签的href属性)
            String href = item.xpath("//a//@href").get();
            // 标题 (span标签中的文本)
            String title = item.xpath("//span//text()").get();
            // 文章发布时间(em标签里的文本内容)
            String date = item.xpath("//em//text()").get();
            // 文章摘要(p标签里的文本内容)
            String abstracts = item.xpath("//p//text()").get();
            hrefs.add(href);
//            page.addTargetRequest(href);
            System.out.println(date + " " + title + " " + href + " " + abstracts);
        });
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        Spider.create(new XpathSpiderService())
                .thread(5)
                .addUrl("http://www.ccpit-henan.org/mcxw/index.jhtml")
                .addUrl("http://www.ccpit-henan.org/mcxw/index.jhtml")
                .addUrl("http://www.ccpit-henan.org/mcxw/index.jhtml")
                .addUrl("http://www.ccpit-henan.org/mcxw/index.jhtml")
                .addUrl("http://www.ccpit-henan.org/mcxw/index.jhtml")
                .addUrl("http://www.ccpit-henan.org/mcxw/index.jhtml")
                .addUrl("http://www.ccpit-henan.org/mcxw/index.jhtml")
                .addUrl("http://www.ccpit-henan.org/mcxw/index.jhtml")
                .run();
    }

}
