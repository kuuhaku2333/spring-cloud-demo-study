package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
// 开启定时任务功能
@EnableScheduling
public class WebMagicApplication {

    /**
     * @Description webMagic启动类
     **/
    public static void main(String[] args) {
        SpringApplication.run(WebMagicApplication.class, args);
    }
}
