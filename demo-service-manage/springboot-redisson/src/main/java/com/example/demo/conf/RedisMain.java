package com.example.demo.conf;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

@SuppressWarnings("all")
public class RedisMain {
    public static void main(String[] args) throws IOException {
        JedisCluster cluster = null;
        try {
            Set<HostAndPort> nodes = new LinkedHashSet<HostAndPort>();
            //一般选用slaveof从IP+端口进行增删改查，不用master
            nodes.add(new HostAndPort("192.168.174.128", 6379));
            nodes.add(new HostAndPort("192.168.174.128", 6380));
            nodes.add(new HostAndPort("192.168.174.128", 6381));
            // Jedis连接池配置
            JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
            // 最大空闲连接数, 默认8个
            jedisPoolConfig.setMaxIdle(100);
            // 最大连接数, 默认8个
            jedisPoolConfig.setMaxTotal(500);
            //最小空闲连接数, 默认0
            jedisPoolConfig.setMinIdle(0);
            // 获取连接时的最大等待毫秒数(如果设置为阻塞时BlockWhenExhausted),如果超时就抛异常, 小于零:阻塞不确定的时间,  默认-1
            jedisPoolConfig.setMaxWaitMillis(2000); // 设置2秒
            //对拿到的connection进行validateObject校验
//         jedisPoolConfig.setTestOnBorrow(true);
            JedisCluster jedis = new JedisCluster(nodes, jedisPoolConfig);
            //设置auth Password
            //JedisCluster jedis = new JedisCluster(nodes,5000,3000,10,{auth_password}, new JedisPoolConfig());
            System.out.println(jedis.set("mykey", "1111"));
            System.out.println(String.valueOf(jedis.get("mykey")));
            jedis.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cluster)
                cluster.close();
        }
    }
}