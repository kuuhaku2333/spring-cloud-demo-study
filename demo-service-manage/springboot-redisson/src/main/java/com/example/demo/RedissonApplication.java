package com.example.demo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;


@SpringBootApplication
public class RedissonApplication {

    /**
     * @Description redisson测试
     **/
    // 访问 http://127.0.0.1:18889/login 密码1234
    public static void main(String[] args) {
        SpringApplication.run(RedissonApplication.class, args);
        System.out.println("redisson测试demo启动成功！，访问 " + "127.0.0.1:" + ServerPort.port + "/");
    }

    // 静态变量注入 property里的值 需要加 @Component @Value("${server.port}")要写在set方法上
    @Component
    public static class ServerPort {

        private static String port;

        @Value("${server.port}")
        public void setPort(String port) {
            ServerPort.port = port;
        }
    }
}
