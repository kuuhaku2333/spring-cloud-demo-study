package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * redisson测试controller
 *
 * @date： 2021/12/13
 * @author: wbx
 */
@RestController
@Slf4j
public class TestController {
    @Resource
    private RedissonClient redissonClient;
    //    @Resource
//    private JReJSON client ;
    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * JReJSON 操作json格式redis数据
     */
    @GetMapping("/setTest")
    public String setTest() {
        redisTemplate.opsForValue().set("test", "test" + System.currentTimeMillis());
        System.out.println("mykey :" + redisTemplate.opsForValue().get("mykey").toString());
        System.out.println("test :" + redisTemplate.opsForValue().get("test").toString());
        return Objects.requireNonNull(redisTemplate.opsForValue().get("test")).toString();
    }


//    /**
//     *  JReJSON 操作json格式redis数据
//     */
//    @RequestMapping("/setJson")
//    public List<Map<String,String>> setJson() {
//        List<Map<String, String>> list = new ArrayList<>();
//        for (int i = 0; i < 3; i++) {
//            Map<String, String> map = new HashMap<>();
//            map.put("name", "name" + i);
//            list.add(map);
//        }
//        // 设置JSON格式
//        client.set("map", list);
//        // 设置过期时间
//        redisTemplate.expire("map",100,TimeUnit.SECONDS);
//        return client.get("map");
//    }

    @RequestMapping("/hello")
    public String hello() {
        return "hello！";
    }

    @GetMapping("/test")
    public String test(String id) {
        RLock rLock = redissonClient.getLock(id);
        // 锁标记
        AtomicBoolean flag = new AtomicBoolean(false);
        try {
            // 尝试加锁，最多等待1秒，上锁以后10秒自动解锁
            flag.set(rLock.tryLock(1, 10, TimeUnit.SECONDS));
            // 尝试加锁成功
            if (flag.get()) {
                // 加锁 执行业务代码
                // 业务代码
                Thread.sleep(100000);
                log.info("进入业务代码: " + id);
                return "请求成功";
            } else {
                return "服务器繁忙，请稍后再试！";
            }
        } catch (Exception e) {
            log.error("请求异常：{}", e);
        } finally {
            // 获取锁成功 解锁
            if (flag.get()) {
                rLock.unlock();
            }
        }
        return "请求失败";
    }

    @GetMapping("/test2")
    public String test2(String id) {
        RLock rLock = redissonClient.getLock(id);
        // 锁标记
        AtomicBoolean flag = new AtomicBoolean(false);
        try {
            // 尝试加锁，最多等待1秒，上锁以后10秒自动解锁
            flag.set(rLock.tryLock(1, 10, TimeUnit.SECONDS));
            // 尝试加锁成功
            if (flag.get()) {
                // 加锁 执行业务代码
                // 业务代码
                log.info("进入业务代码: " + id);
                return "请求成功";
            } else {
                return "服务器繁忙，请稍后再试！";
            }
        } catch (Exception e) {
            log.error("请求异常：{}", e);
        } finally {
            // 获取锁成功 解锁
            if (flag.get()) {
                rLock.unlock();
            }
        }
        return "请求失败";
    }

//    @GetMapping("/test2")
//    public String test2(String id) {
//        RLock rLock = redissonClient.getLock(id);
//        // 锁标记
//        AtomicBoolean flag = new AtomicBoolean(false);
//        try {
//            flag.set(rLock.tryLock(1, 1, TimeUnit.SECONDS));
//            // 尝试加锁成功
//            if (flag.get()) {
//                // 加锁 执行业务代码
//                rLock.lock();
//                // 业务代码
//                log.info("进入业务代码: " + id);
//                return "请求成功";
//            } else {
//                return "服务器繁忙，请稍后再试！";
//            }
//        } catch (Exception e) {
//            log.error("请求异常：{}", e);
//        } finally {
//            // 获取锁成功 解锁
//            if (flag.get()) {
//                rLock.unlock();
//            }
//        }
//        return "请求失败";
//    }

}
