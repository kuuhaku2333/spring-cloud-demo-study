package com.example.demo.entity.base.entity;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.Data;

/**
 * Created by zhoutaoo on 2018/6/1.
 */
@Data
public class BaseParam<T extends BasePo> {

    public QueryWrapper<T> build() {
        QueryWrapper<T> queryWrapper = new QueryWrapper<T>();
        return queryWrapper;
    }
}
