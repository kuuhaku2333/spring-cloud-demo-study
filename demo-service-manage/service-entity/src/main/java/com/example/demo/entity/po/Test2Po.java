package com.example.demo.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (Test2)实体类 po
 *
 * @author wbx
 * @since 2021-05-14 11:08:09
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("test2")
public class Test2Po  {
    /**
     * 主键id
     */
    @TableId(type = IdType.UUID)
    private String id;

    /**
     * test2
     */
    private String test2;

    /**
     * test1的主键ID
     */
    private String test1Id;

}
