package com.example.demo.entity.base.entity;

import lombok.Data;
import org.springframework.beans.BeanUtils;

@Data
public class BasePo {
	public <T extends BaseVo> T toVo(Class<T> clazz) {
		T t = BeanUtils.instantiateClass(clazz);
		BeanUtils.copyProperties(this, t);
		return t;
	}
}
