package com.example.demo.entity.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;


/**
 * (MongoParam)实体类
 *
 * @author wbx
 * @since 2021-05-14 11:08:09
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MongoParam {

    @Min(message = "每页条数不能小于1", value = 1)
    private int pageSize;
    @Min(message = "当前页数不能小于1", value = 1)
    private int current;

//    @NotBlank(message = "集合名称不能为空")
    /**
     *  集合名称
     */
    private String collectName;

//    @NotBlank(message = "查询字段的值不能为空")
    private String value;

}
