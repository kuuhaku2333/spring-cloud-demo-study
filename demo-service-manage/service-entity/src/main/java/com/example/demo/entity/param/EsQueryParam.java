package com.example.demo.entity.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * (Test2)实体类 po
 *
 * @author wbx
 * @since 2021-05-14 11:08:09
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EsQueryParam {

    @NotBlank(message = "索引名称不能为空")
    private String indexName;

    // 要高亮的字段
    private String highLightField;

    // 查询匹配的字段
    private String queryField;

    private String searchText;

    @NotBlank(message = "分页数不能为空")
    private String size;

    @NotBlank(message = "当前页数不能为空")
    private String current;

    // 创建时间
    private String createTime;

}
