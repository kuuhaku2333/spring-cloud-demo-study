package com.example.demo.entity.base.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BaseVo<T extends BasePo> {
}
