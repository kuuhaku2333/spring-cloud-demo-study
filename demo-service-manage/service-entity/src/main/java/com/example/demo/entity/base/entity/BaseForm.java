package com.example.demo.entity.base.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;

@ApiModel
@Slf4j
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseForm<T extends BasePo> {
	public interface GroupInsert {

	}
	public interface  GroupUpdate{

	}
	/**
	 * From转化为Po，进行后续业务处理
	 *
	 * @param clazz
	 * @return
	 */
	public T toPo(Class<T> clazz) {
		T t = BeanUtils.instantiateClass(clazz);
		BeanUtils.copyProperties(this, t);
		return t;
	}
}
