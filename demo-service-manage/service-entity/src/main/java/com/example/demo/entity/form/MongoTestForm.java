package com.example.demo.entity.form;

import com.example.demo.entity.base.entity.BaseForm;
import com.example.demo.entity.mongoDB.MongoTestPo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MongoTestForm extends BaseForm<MongoTestPo> {

    /**
     * @Document 代表映射到数据库的那个集合上
     * @Id 代表主键如果没指定主键 mongoDB会自动生成
     * @Field 代表字段映射
     * @Transient 代表该字段不会被录入到数据库中
     */
    @NotNull(message = "年龄不能为空")
    private Integer age;
    @NotBlank(message = "名字不能为空")
    private String name;
    @NotBlank(message = "班级不能为空")
    private String classes;

}
