package com.example.demo.entity.base.entity;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class DataPageForm<T> {
	@ApiModelProperty("页数")
	private Long page = 1L;
	@ApiModelProperty("单页数量")
	private Long pageSize = 10L;
	@ApiModelProperty(value = "总数", hidden = true)
	private long total;
	@ApiModelProperty(value = "列表数据", hidden = true)
	private List<T> list;
	
	protected DataPageForm() {
	}
	
	public static <P extends BasePo, V extends BaseVo> DataPageForm<V> builder(IPage<P> data, Class<V> voClass) {
		List<V> listVo = data.getRecords()
				.stream()
				.map((po) -> po.toVo(voClass))
				.collect(Collectors.toList());
		DataPageForm<V> pDataPage = new DataPageForm<>();
		pDataPage.page = data.getCurrent();
		pDataPage.pageSize = data.getSize();
		pDataPage.total = data.getTotal();
		pDataPage.list = listVo;
		return pDataPage;
	}
	
	public static <P extends BasePo, V extends BaseVo> List<V> builder(List<P> listPo, Class<V> voClass) {
		List<V> listVo = listPo.stream()
				.map((po) -> po.toVo(voClass))
				.collect(Collectors.toList());
		return listVo;
	}
	
	public static <P extends BasePo> DataPageForm<P> builder(IPage<P> data) {
		
		DataPageForm<P> pDataPage = new DataPageForm<>();
		pDataPage.page = data.getCurrent();
		pDataPage.pageSize = data.getSize();
		pDataPage.total = data.getTotal();
		pDataPage.list = data.getRecords();
		return pDataPage;
	}
	
	public static <P extends BaseVo> DataPageForm<P> builderVo(IPage<P> data) {
		
		DataPageForm<P> pDataPage = new DataPageForm<>();
		pDataPage.page = data.getCurrent();
		pDataPage.pageSize = data.getSize();
		pDataPage.total = data.getTotal();
		pDataPage.list = data.getRecords();
		return pDataPage;
	}
	
	//
	public IPage<T> builderPage() {
		IPage<T> iPage = new Page<>();
		iPage.setCurrent(page);
		iPage.setSize(pageSize);
		return iPage;
	}
	
	public static DataPageForm getInstance() {
		return new DataPageForm();
	}
}
