package com.example.demo.entity.base.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class DataPage<T> extends DataPageForm<T> {
	@ApiModelProperty(value = "列表数据")
	private List<T> list;
	
	private DataPage() {
		super();
	}
}
