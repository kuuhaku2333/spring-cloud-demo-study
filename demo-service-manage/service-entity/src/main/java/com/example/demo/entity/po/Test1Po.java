package com.example.demo.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (Test1)实体类 po
 *
 * @author wbx
 * @since 2021-05-14 11:16:15
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("test1")
public class Test1Po  {
    /**
     * 主键id
     */
    @TableId(type = IdType.UUID)
    private String id;

    /**
     * test1
     */
    private String test1;

}
