package com.example.demo.entity.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * (Test2)实体类 po
 *
 * @author wbx
 * @since 2021-05-14 11:08:09
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParamTest2 {

    private String indexName;
    /**
     * test2
     */
    private String test2;

    @NotBlank(message = "分页数不能为空")
    private String size;

    @NotBlank(message = "当前页数不能为空")
    private String current;

}
