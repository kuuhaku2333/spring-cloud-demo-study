package com.example.demo.entity.mongoDB;

import com.example.demo.entity.base.entity.BasePo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * (MongoTestPo)实体类 po
 * @author wbx
 */
// 映射mongoDB 集合的实体类

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "test")
public class MongoTestPo extends BasePo {
    /**
     * @Document 代表映射到数据库的那个集合上
     * @Id 代表主键如果没指定主键 mongoDB会自动生成
     * @Field 代表字段映射
     * @Transient 代表该字段不会被录入到数据库中
     */

    @Id
    private String id;

    private Integer age;

    private String name;

    @Field("mydate")
    private String mydate;

    @Field("classes")
    private String classes;

}
