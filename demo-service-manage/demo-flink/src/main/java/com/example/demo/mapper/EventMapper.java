package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.Event;

/**
 * (Event)表数据库访问层
 *
 * @author wbx
 * @since 2022-05-11 09:39:05
 */
public interface EventMapper extends BaseMapper<Event> {

}
