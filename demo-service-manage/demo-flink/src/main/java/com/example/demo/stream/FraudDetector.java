package com.example.demo.stream;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.walkthrough.common.entity.Alert;
import org.apache.flink.walkthrough.common.entity.Transaction;

import static com.example.demo.stream.FraudDetector.Lagrange.lglrchzh;

public class FraudDetector extends KeyedProcessFunction<Long, Transaction, Alert> {

    private static final Log log = LogFactory.get();

    private static final long serialVersionUID = 1L;

    private static final double SMALL_AMOUNT = 1.00;
    private static final double LARGE_AMOUNT = 500.00;
    private static final long ONE_MINUTE = 60 * 1000;

    private transient ValueState<Boolean> flagState;
    private transient ValueState<Long> timerState;

    @Override
    public void open(Configuration parameters) {
        log.info("进入open方法");
        ValueStateDescriptor<Boolean> flagDescriptor = new ValueStateDescriptor<>(
                "flag",
                Types.BOOLEAN);
        flagState = getRuntimeContext().getState(flagDescriptor);

        ValueStateDescriptor<Long> timerDescriptor = new ValueStateDescriptor<>(
                "timer-state",
                Types.LONG);
        timerState = getRuntimeContext().getState(timerDescriptor);
    }

    @Override
    public void processElement(
            Transaction transaction,
            Context context,
            Collector<Alert> collector) throws Exception {
        log.info("进入processElement方法");

        // Get the current state for the current key
        Boolean lastTransactionWasSmall = flagState.value();

        // Check if the flag is set
        if (lastTransactionWasSmall != null) {
            if (transaction.getAmount() > LARGE_AMOUNT) {
                //Output an alert downstream
                Alert alert = new Alert();
                alert.setId(transaction.getAccountId());

                collector.collect(alert);
            }
            // Clean up our state
            cleanUp(context);
        }

        if (transaction.getAmount() < SMALL_AMOUNT) {
            // set the flag to true
            flagState.update(true);

            long timer = context.timerService().currentProcessingTime() + ONE_MINUTE;
            context.timerService().registerProcessingTimeTimer(timer);

            timerState.update(timer);
        }
    }

    @Override
    public void onTimer(long timestamp, OnTimerContext ctx, Collector<Alert> out) {
        log.info("进入onTimer方法");
        // remove flag after 1 minute
        timerState.clear();
        flagState.clear();
    }

    private void cleanUp(Context ctx) throws Exception {
        // delete timer
        Long timer = timerState.value();
        ctx.timerService().deleteProcessingTimeTimer(timer);

        // clean up all state
        timerState.clear();
        flagState.clear();
    }


    public static class Lagrange {
        /**
         * 拉格朗日插值
         *
         * @param x x坐标数组
         * @param y y坐标数组
         * @param X 需要计算的x坐标值
         * @return 插值结果
         */
        public static double lglrchzh(double[] x, double[] y, double X) {
            int n = x.length - 1;
            double Y = 0;
            for (int k = 0; k <= n; k++) {
                double t = 1;
                for (int j = 0; j <= n; j++) {
                    if (j == k) {
                        j = k + 1;
                        if (j > n) {
                            break;
                        }
                    }
                    t *= (X - x[j]) / (x[k] - x[j]);
                }
                Y = Y + t * y[k];
            }
            return Y;
        }
    }

    public static void main(String[] args) {
        double[] x = {1, 2, 3};
        double[] y = {4.2, 5.5, 6.8};
        double result = lglrchzh(x, y, 2.5);
        System.out.println("lglrchzh = " + result);
    }

}
