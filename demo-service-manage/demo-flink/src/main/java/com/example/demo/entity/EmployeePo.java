package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * (Employee)实体类 po
 *
 * @author wbx
 * @since 2022-05-06 10:43:01
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@TableName("employee")
public class EmployeePo implements Serializable {

    @TableField
    private Long id;

    /**
     * 用户名
     */

    private String userName;

    /**
     * 性别：0-男，1-女
     */

    private String gender;

    /**
     * 年龄
     */

    private Integer age;

    /**
     * 生日
     */
    private Date birthday;

    /**
     * 婚姻状态：0-未婚，1-已婚
     */
    private String maritalStatus;

    /**
     * 学历：0-大专，1-本科，2-硕士，3-研究生
     */
    private String education;

    /**
     * 血型：A，B，O，AB
     */
    private String bloodType;

    /**
     * 电话
     */
    private String mobile;

    /**
     * 部门
     */
    private String departmentName;

    /**
     * 国家地区
     */
    private String nationalArea;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 身份证
     */
    private String idCardNumber;

    /**
     * 个人邮箱
     */
    private String personalMailBox;

}
