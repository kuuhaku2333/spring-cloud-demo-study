//package com.example.demo.stream.sink;
//
//import cn.hutool.json.JSONUtil;
//import com.example.demo.entity.EmployeePo;
//import com.example.demo.entity.Event;
//import com.example.demo.env.SpringContextHolder;
//import org.apache.flink.api.common.io.FileOutputFormat;
//import org.apache.flink.api.java.io.PojoCsvInputFormat;
//import org.apache.flink.api.java.typeutils.PojoTypeInfo;
//import org.apache.flink.configuration.Configuration;
//import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.JedisPool;
//
//import java.io.IOException;
//import java.io.Writer;
//import java.lang.reflect.Field;
//import java.util.Arrays;
//import java.util.Objects;
//import java.util.stream.Collectors;
//
///**
// * 写出到mySQL
// *
// * @date： 2022/5/6
// * @author: wbx
// */
//public class CsvSink<In> extends FileOutputFormat<In> {
//
//
//    private Writer writer;
//
//
//    @Override
//    public void writeRecord(In record) throws IOException {
//        PojoTypeInfo<EmployeePo> poPojoTypeInfo = new PojoTypeInfo<>();
//        EmployeePo employeePo = new EmployeePo();
//        PojoCsvInputFormat format = new PojoCsvInputFormat(employeePo);
//        writer.write();
//
//    }
//
//
//}
