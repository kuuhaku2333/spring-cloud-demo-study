package com.example.demo.env;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Objects;

/**
 * 手动操作springBean类
 *
 * @date： 2022/5/10
 * @author: wbx
 */
@Slf4j
public class SpringContextHolder implements ApplicationContextAware {

    public static ApplicationContext applicationContext;

    /**
     *  加载xml中配置的bean对象
     */
    public static ApplicationContext init() {
        synchronized (SpringContextHolder.class) {
            if(Objects.isNull(applicationContext)) {
                applicationContext = new ClassPathXmlApplicationContext("spring-mybatis.xml");
                log.info("初始化ApplicationContext完成！！！");
            }
        }
        return applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringContextHolder.applicationContext = applicationContext;
    }

    /**
     *  获取spring中的bin对象
     * @param cls 注册到bean工厂的类 类型
     * @param <T> 注册到bean工厂的类型
     * @return bean对象
     */
    public static <T> T getBean(Class<T> cls) {
        return init().getBean(cls);
    }


    /**
     * 关闭连接 释放资源
     */
    public static void close() {
        if(Objects.nonNull(applicationContext)) {
            ((AbstractApplicationContext)applicationContext).close();
            log.info("关闭ApplicationContext完成！！！");
        }
    }

}
