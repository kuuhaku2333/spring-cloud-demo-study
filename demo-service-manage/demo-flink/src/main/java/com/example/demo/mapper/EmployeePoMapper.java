package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.example.demo.entity.EmployeePo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * (Employee)表数据库访问层
 *
 * @author wbx
 * @since 2022-05-10 10:04:45
 */
public interface EmployeePoMapper extends BaseMapper<EmployeePo> {

    @Select("SELECT * FROM EMPLOYEE ${ew.customSqlSegment}")
    List<EmployeePo> getEmployeeList(@Param(Constants.WRAPPER) Wrapper<EmployeePo> wrapper);

}
