package com.example.demo.util;

import java.io.InputStream;
import java.util.Properties;

/**
 * properties工具类
 *
 * @date： 2022/5/9
 * @author: wbx
 */
public class PropertyUtil {
    public static Properties loadProperty() {
        //创建Properties对象
        Properties properties = new Properties();
        // 1、获取class目录下的配置文件
        ClassLoader classLoader = PropertyUtil.class.getClassLoader();
        try (InputStream in = classLoader.getResourceAsStream("application.properties")) {
            // 2、直接用流加载
            properties.load(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return properties;
    }
}
