package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.EmployeePo;

/**
 * (Employee)表数据库访问层
 *
 * @author wbx
 * @since 2022-05-10 10:04:45
 */
public interface IEmployeeService extends IService<EmployeePo> {

}
