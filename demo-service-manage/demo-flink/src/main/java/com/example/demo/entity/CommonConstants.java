package com.example.demo.entity;/**
 * Created by penglee on 2019/7/2.
 */

/**
 * CommonConstants
 * <p>
 * Description: <br>
 * Created in 2019/7/2 09:54 <br>
 *
 * @author 李鹏
 */
public class CommonConstants {
    // 客户端token异常
    public static final String CONTEXT_KEY_USER_ID = "currentUserId";
    public static final String CONTEXT_KEY_USERNAME = "currentUserName";
    public static final String CONTEXT_KEY_USER_NAME = "currentUser";
    public static final String CONTEXT_KEY_USER_TOKEN = "currentUserToken";

    public static final String HYVERIFY_KEY = "HYKEY:";
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String TENANT_ID = "tenant_id";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String ISSUER = "temporary_account";
    //自增表count_name
    public static final String ACCOUNT_CODE = "accountCode";
    public static final String ORG_TYPE_CODE = "orgTypeCode";
    public static final String WEB_SITE_CODE = "siteCode";
    public static final String CATALOG_CODE = "catalogCode:";
    public static final String RSA_KEY = "RSA_KEY:";    //RSA缓存name
    public static final String VERIFY_CODE = "VerifyCode";    //验证码缓存name

    public static final String ACCOUNT_ACCOUNT_REDIS_KEY = "account_account:";    //账号缓存name
    public static final String ACCOUNT_ID_REDIS_KEY = "account_id:";    //账号缓存name
    public static final String ACCOUNT_PHONE_REDIS_KEY = "account_phone:";    //账号缓存name
    public static final String ACCOUNT_KEY_REDIS_KEY = "account_key:";    //账号缓存name

    //校验正则
    public static final String PASSWORD_REGEX = "^(?![A-Za-z]+$)(?!\\d+$)(?![\\W_]+$)\\S{8,16}$"; //密码正则 8-16位字符，数字、字母、字符至少包含两种
    public static final String ACCOUNT_REGEX = "^[a-zA-Z][\\w]{5,19}$"; //账号正则 6到20位
    public static final String PHONE_REGEX = "^1[3-9]\\d{9}$"; //手机号正则

    //时间 单位：毫秒
    public static final long REFRESH_TOKEN_EXPIRE_TIME = 4 * 60 * 60 * 1000; //refreshToken过期时间 4小时
    public static final long REDIS_EXPIRE_TIME = 300 * 60 * 100; //Redis中token失效时间 30分钟
    public static final long ACCOUNT_EXPIRE_TIME = 2 * 60 * 60 * 1000; //Redis中账号失效时间 2小时

    public static final long RSA_PUBLIC_KEY_EXPIRE_TIME = 50 * 60 * 100; //RSA公钥过期时间 5分钟
    public static final long VERIFY_CODE_EXPIRE_TIME = 50 * 60 * 100; //图片验证码过期时间 5分钟

    //redis分布式锁KEY
    public static final String LOCK_KEY_BUSINESS_ITEMS_VERSION = "sx_item_version";//业务项版本递增锁
    public static final String LOCK_KEY_CATALOG_VERSION = "sx_catalog_version";//主项目录版本递增锁
    public static final String LOCK_KEY_COUNT_ORG_TYPE_CODE = "count_org_typ_code";//自增表 组织机构类型编码
    public static final String LOCK_KEY_COUNT_RANDOM_SITE_CODE = "count_random_site_code";//自增表 站点编码
    public static final String LOCK_KEY_COUNT_RANDOM_ACCOUNT = "count_random_account";//自增表 随机账号
    public static final String LOCK_KEY_COUNT_CATALOG_CODE = "count_sx_catalog_code";//自增表 目录编码


    // 服务模块前缀
    public static String MODULE_PRODUCT_PREFIX =  "/product-server";
    public static String MODULE_CONSUMER_PREFIX =  "/consumer-server";

    // 生产者服务状态码
    public static int PRODUCT_STATUS_CODE = 103 * 1000000;
    // 消费者服务状态码
    public static int CONSUMER_PUB_STATUS_CODE = 104 * 1000000;

    // kafka使用topic
    public final static String KAFKA_TOPIC = "kafka_test_topic";


}