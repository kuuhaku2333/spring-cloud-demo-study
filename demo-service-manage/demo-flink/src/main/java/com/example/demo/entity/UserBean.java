package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created with IntelliJ IDEA
 * @author : wbx
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserBean {
    private String userID;
    private long eventTime;
    private String eventType;
    private String productID;
    private int productPrice;

    public UserBean(String userID, String eventType) {
        this.userID = userID;
        this.eventType = eventType;
    }
}

