package com.example.demo.stream.deal;

import com.example.demo.entity.Event;
import org.apache.flink.api.common.functions.AggregateFunction;

import java.util.HashMap;
import java.util.Map;

public class CountEventAggregate implements AggregateFunction<Event, Map<Event, Long>, Map<Event, Long>> {

        /**
         * 创建累加器
         *
         * @return 累加器对象
         */
        @Override
        public Map<Event, Long> createAccumulator() {
            // 创建累加器
            return new HashMap<>();
        }

        /**
         * 每次流循环中 操作累加器
         *
         * @param value       当前流循环对象
         * @param accumulator 累加器对象
         * @return 累加器数据
         */
        @Override
        public Map<Event, Long> add(Event value, Map<Event, Long> accumulator) {
            // 属于本窗口的数据来一条累加一次，并返回累加器
            if (accumulator.containsKey(value)) {
                accumulator.put(value, accumulator.get(value) + 1);
            } else {
                accumulator.put(value, 1L);
            }
            return accumulator;
        }

        /**
         * 获取最终操作结果
         *
         * @param accumulator 累加器对象
         * @return 最终结果
         */
        @Override
        public Map<Event, Long> getResult(Map<Event, Long> accumulator) {
            // 窗口闭合时，增量聚合结束，将计算结果发送到下游
            return accumulator;
        }

        @Override
        public Map<Event, Long> merge(Map<Event, Long> a, Map<Event, Long> b) {
            return null;
        }
    }