
package com.example.demo.batch;

import ch.ethz.ssh2.Session;
import com.example.demo.upload.FileUploadUtil;
import com.example.demo.upload.ResultEntity;

import java.io.File;
import java.util.Objects;

/**
 *  将生成的jar包上传到服务器指定路径 并执行脚本命令
 */
public class UploadMain {
    public static void main(String[] args) throws Exception {
        // 上传的jar包名称
        String jarName = "demo-flink-0.0.1-SNAPSHOT.jar";
        // jar包本地地址
        String filePath = "D:\\Java\\日常\\demo-service-manage\\demo-flink\\target\\";
        // 上传到服务器的地址
        String linuxPath = "/usr/local/soft/flink/jobJar";
        File file = new File(filePath + jarName);
        if(!file.exists()) {
            System.err.println("上传的文件：" + filePath + jarName + "不存在");
            return;
        }
        FileUploadUtil fileUploadUtil = new FileUploadUtil();
        // 调用上传
        ResultEntity resultEntity = fileUploadUtil.uploadFile(file, linuxPath, file.getName());
        if(Objects.nonNull(resultEntity) && "ok".equals(resultEntity.getCode()))
            System.out.println("文件上传成功：" + linuxPath + file.getName());
        // 执行的linux命令
        String command = "bash /usr/local/soft/flink/jobJar/script.sh";
        System.out.println("执行的命令是：" + command);
        // 获取session连接
        Session session = fileUploadUtil.getSession();
        // 执行命令
        session.execCommand(command);
        System.out.println("任务执行成功！");
    }
}
