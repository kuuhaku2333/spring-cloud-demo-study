//package com.example.grpc.server.service;
//
//import com.example.grpc.server.api.*;
//import io.grpc.ServerServiceDefinition;
//import io.grpc.stub.StreamObserver;
//import net.devh.boot.grpc.server.service.GrpcService;
//
///**
// * GrpcServerServiceImpl
// *
// * @date： 2022/2/24
// * @author: wbx
// */
//@GrpcService
//public  class RouteGuideGrpcService extends RouteGuideGrpc.RouteGuideImplBase {
//    @Override
//    public void getFeature(Point request, StreamObserver<Feature> responseObserver) {
//        responseObserver.onNext(checkFeature(request));
//        responseObserver.onCompleted();
//    }
//
//    @Override
//    public void listFeatures(Rectangle request, StreamObserver<Feature> responseObserver) {
//        super.listFeatures(request, responseObserver);
//    }
//
//    @Override
//    public StreamObserver<Point> recordRoute(StreamObserver<RouteSummary> responseObserver) {
//        return super.recordRoute(responseObserver);
//    }
//
//    @Override
//    public StreamObserver<RouteNote> routeChat(StreamObserver<RouteNote> responseObserver) {
//        return super.routeChat(responseObserver);
//    }
//
//    @Override
//    public ServerServiceDefinition bindService() {
//        return super.bindService();
//    }
//
//    private Feature checkFeature(Point location) {
//        for (Feature feature : RouteGuideGrpc.f) {
//            if (feature.getLocation().getLatitude() == location.getLatitude()
//                    && feature.getLocation().getLongitude() == location.getLongitude()) {
//                return feature;
//            }
//        }
//}
