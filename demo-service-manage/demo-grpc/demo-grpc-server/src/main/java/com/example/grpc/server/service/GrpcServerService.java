package com.example.grpc.server.service;

import com.example.grpc.server.api.HelloReply;
import com.example.grpc.server.api.HelloRequest;
import com.example.grpc.server.api.SimpleGrpc;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

/**
 * GrpcServerServiceImpl
 *
 * @date： 2022/2/24
 * @author: wbx
 */
@GrpcService
public class GrpcServerService extends SimpleGrpc.SimpleImplBase {
    @Override
    public void sayHello(HelloRequest req, StreamObserver<HelloReply> responseObserver){
        HelloReply reply = HelloReply.newBuilder()
                .setMessage("grpc-server调用成功！！！" + req.getName())
                .build();
        responseObserver.onNext(reply);
        // 指出我们已经完成了和 RPC的交互
        responseObserver.onCompleted();
    }
}
