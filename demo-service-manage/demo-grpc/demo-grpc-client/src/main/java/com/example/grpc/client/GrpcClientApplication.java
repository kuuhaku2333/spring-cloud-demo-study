package com.example.grpc.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class GrpcClientApplication {

    /**
     *  grpc-client服务
     **/
    public static void main(String[] args) {
        SpringApplication.run(GrpcClientApplication.class, args);
        System.out.println("grpc-client启动成功！");
    }
}
