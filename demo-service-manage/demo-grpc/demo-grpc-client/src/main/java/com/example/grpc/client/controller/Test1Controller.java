package com.example.grpc.client.controller;


import com.example.grpc.client.service.GrpcClientService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * (Test1)控制层
 *
 * @author wbx
 * @since 2021-05-14 11:16:12
 */
@RestController
@RequestMapping("/client")
public class Test1Controller {
    @Resource
    private GrpcClientService grpcClientService;
    @GetMapping("/test")
    public String test1Add(){
        return grpcClientService.sendMessage("发起调用");
    }
}
