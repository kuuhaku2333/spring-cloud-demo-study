package com.example.demo;

import cn.hutool.core.date.ChineseDate;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.DesensitizedUtil;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.time.LocalDateTime;

/**
 * @date： 2021/6/15
 * @author: wbx
 */
@SpringBootTest(classes = {ForestApplication.class})
@RunWith(SpringRunner.class)
public class HuToolTest {

    @org.junit.Test
    public void test() {
        LocalDateTime now = LocalDateTimeUtil.now();
        System.out.println("now = " + now);
        String format = LocalDateTimeUtil.format(now, DatePattern.NORM_DATE_PATTERN);
        System.out.println("format = " + format);

        LocalDateTime localDateTime = LocalDateTimeUtil.parse("2020-01-23 16:59", DatePattern.NORM_DATETIME_MINUTE_PATTERN);
        System.out.println("parse = " + localDateTime);
        String format1 = LocalDateTimeUtil.format(localDateTime, DatePattern.NORM_DATETIME_MINUTE_PATTERN);
        System.out.println("format1 = " + format1);

        //通过公历构建
        ChineseDate date = new ChineseDate(DateUtil.parseDate("2020-01-25"));
        // 一月
        date.getChineseMonth();
        // 正月
        date.getChineseMonthName();
        // 初一
        date.getChineseDay();
        // 庚子
        date.getCyclical();
        // 生肖：鼠
        date.getChineseZodiac();
        // 传统节日（部分支持，逗号分隔）：春节
        date.getFestivals();
        // 庚子鼠年 正月初一
        String string = date.toString();
        System.out.println("string = " + string);

        //通过公历构建
        ChineseDate chineseDate = new ChineseDate(DateUtil.parseDate("2020-08-28"));

        // 庚子年甲申月癸卯日
        String cyclicalYMD = chineseDate.getCyclicalYMD();
        System.out.println("cyclicalYMD = " + cyclicalYMD);

        // 身份证脱敏
        String s = DesensitizedUtil.idCardNum("51343620000320711X", 1, 2);
        System.out.println("s = " + s);

        // 180****1999 手机号脱敏
        String s1 = DesensitizedUtil.mobilePhone("18049531999");
        System.out.println("s1 = " + s1);

        // ********** 密码脱敏
        String password = DesensitizedUtil.password("1234567890");
        System.out.println("password = " + password);

        // 验证是否为电子邮件格式
        boolean isEmail = Validator.isEmail("loolly@gmail.com");
        System.out.println("isEmail = " + isEmail);

        // 校验 在不符合时候抛出异常
//        String s2 = Validator.validateChinese("我是一段zhongwen", "内容中包含非中文");
//        System.out.println("s2 = " + s2);

        // Bean对象转换为Map
//        Map<String, Object> map = BeanUtil.beanToMap(person);


    }
}
