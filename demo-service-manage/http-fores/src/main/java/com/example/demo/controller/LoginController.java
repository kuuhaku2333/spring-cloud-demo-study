package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @version 1.0
 * @author： hjb
 * @date： 2021-08-10 08:57
 */
@Controller
public class LoginController {
    private Logger logger = LoggerFactory.getLogger(LoginController.class);

//    @RequestMapping("/")
//    public String showHome() {
//       // String name = SecurityContextHolder.getContext().getAuthentication().getName();
//        logger.info("当前登陆用户：" + name);
//        //return "home.html";
//        return "index.html";
//    }

    @RequestMapping("/login")
    public String showLogin() {
        return "index.html";
    }

    @PostMapping("/unionLogin")
    public String login() {
        return "redirect:/unionPay.html";
    }

    @RequestMapping("/unionPay")
    public String unionPay() {
        return "unionPay.html";
    }

    @RequestMapping("/admin")
    @ResponseBody
  //  @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String printAdmin() {
        return "如果你看见这句话，说明你有ROLE_ADMIN角色";
    }

    @RequestMapping("/user")
    @ResponseBody
   // @PreAuthorize("hasRole('ROLE_HJB')")
   // @PreAuthorize("hasAnyAuthority('write')")
    public String printUser() {
        return "如果你看见这句话，说明你有ROLE_USER角色";
    }
}

