package com.example.demo.entity.esentity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

/**
 * (Test2)实体类 po
 *
 * @author wbx
 * @since 2021-05-14 11:08:09
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EsTestParam {

    /**
     *  索引名称
     */
    @NotBlank(message = "索引名称不能为空！")
    private String indexName;

    /**
     * test2
     */
    private String test2;

    /**
     * test1的主键ID
     */
    private String test1Id;

    @ApiModelProperty("页数")
    @Min(message = "当前页数最小值为1！",value = 1)
    private Long page = 1L;

    @ApiModelProperty("单页数量")
    private Long pageSize = 10L;

}
