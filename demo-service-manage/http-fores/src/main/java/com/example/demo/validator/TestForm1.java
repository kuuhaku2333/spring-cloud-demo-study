
package com.example.demo.validator;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 自定义日期校验配置模拟使用
 * @author wbx
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TestForm1 {
     /**
     * 早餐时间
     */
//    @NotEmpty(message = "早餐时间必填")
    @ApiModelProperty("早餐时间")
    @NotBlank(message = "早餐时间不能为空！")
    @DateValidator(format = "yyyyMMdd", value = DateValidator.AFTERNOW, message = "早餐时间不能小于当前时间！")
    private String breakfastTime;

    /**
     * 午餐时间
     */
//    @NotEmpty(message = "午餐时间必填")
    @ApiModelProperty("午餐时间")
//    @NotBlank(message = "午餐时间不能为空！")
    @DateValidator(format = "yyyy-MM-dd", value = DateValidator.BEFORENOW, message = "午餐时间不能大于当前时间！")
    private String lunchTime;


    /**
     * 晚餐时间
     */
    @ApiModelProperty("晚餐时间")
//    @NotBlank(message = "晚餐时间不能为空！")
    @DateValidator(format = "yyyy-MM-dd",value = DateValidator.AFTERNOW, message = "晚餐时间不能大于当前时间！")
    private String dinnerTime;

}
