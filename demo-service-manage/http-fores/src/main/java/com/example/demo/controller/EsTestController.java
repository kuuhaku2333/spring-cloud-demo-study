package com.example.demo.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.common.constant.QueryConstant;
import com.example.demo.common.response.ResponseData;
import com.example.demo.entity.esentity.EsTest;
import com.example.demo.entity.esentity.EsTestHighLight;
import com.example.demo.entity.esentity.EsTestParam;
import com.example.demo.util.EsUtil;
import com.example.demo.util.gson.GsonUtil;
import com.example.demo.validator.TestForm;
import com.example.demo.validator.TestForm1;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static com.example.demo.util.common.ResponseDataUtil.responseData;

/**
 * @version 1.0
 * @author： wbx
 * @date： 2021-08-10 08:57
 */
@RestController
@Slf4j
@Api(value = "ES测试", tags = "ES测试")
@Validated
@RequestMapping("/es")
public class EsTestController {

    /**
     *  判断索引是否存在
     * @param indexName 索引名
     * @return true 存在 / false 不存在
     */
    @GetMapping("/existIndex")
    @ApiOperation("判断索引是否存在")
    @ApiImplicitParam(name = "indexName" ,value = "索引名称" ,required = true)
    public ResponseData<Boolean> existIndex(String indexName) {
        return responseData(EsUtil.existsIndex(indexName));
    }

    /**
     *  根据索引名称和ID查询数据
     * @param indexName 索引名
     * @return 文档数据
     */
    @GetMapping("/getByIndexAndId")
    @ApiOperation("根据索引名称和ID查询数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "indexName" ,value = "索引名称" ,required = true),
            @ApiImplicitParam(name = "id" ,value = "文档ID" ,required = true)
    })
    public ResponseData<EsTest> getByIndexAndId(String indexName, String id) {
        return responseData(GsonUtil.GsonToBean(EsUtil.getDataByIndexAndId(indexName, id), EsTest.class));
    }

    /**
     *  往指定索引插入数据
     * @param indexName 索引名称
     * @param esTest 文档数据
     * @return true 成功 / false 失败
     */
    @PostMapping("/saveDoc")
    @ApiImplicitParam(name = "indexName" ,value = "索引名称" ,required = true)
    @ApiOperation("往指定索引插入数据")
    public ResponseData<Boolean> saveDoc(@RequestParam String indexName, @RequestBody EsTest esTest) {
        return responseData((EsUtil.addDoc(indexName, JSONObject.fromObject(esTest))));
    }

    /**
     *  修改指定索引的文档数据
     * @param indexName 索引名称
     * @param esTest 文档数据
     * @return true 成功 / false 失败
     */
    @PostMapping("/modifyDoc")
    @ApiImplicitParam(name = "indexName" ,value = "索引名称" ,required = true)
    @ApiOperation("修改指定索引的文档数据")
    public ResponseData<Boolean> modifyDoc(@RequestParam String indexName, @RequestBody @Validated({EsTest.GroupUpdate.class}) EsTest esTest) {
        return responseData((EsUtil.updateDoc(indexName, JSONObject.fromObject(esTest))));
    }

    /**
     *  删除指定索引的文档数据
     * @param indexName 索引名称
     * @param id 文档id
     * @return true 成功 / false 失败
     */
    @DeleteMapping("/deleteDoc")
    @ApiImplicitParam(name = "indexName" ,value = "索引名称" ,required = true)
    @ApiOperation("删除指定索引的文档数据")
    public ResponseData<Boolean> deleteDoc(@RequestParam String indexName, @RequestParam String id) {
        return responseData((EsUtil.deleteById(indexName, id)));
    }

    /**
     *  删除指定索引的文档数据
     * @param esTestParam 查询条件实体
     * @return true 成功 / false 失败
     */
    @PostMapping("/getList")
    @ApiOperation("按条件分页查询文档数据")
    public ResponseData<Page<EsTest>> getList(@RequestBody @Validated EsTestParam esTestParam) {
        return responseData((EsUtil.searchPageByKeywords(esTestParam.getIndexName(), JSONObject.fromObject(esTestParam), EsTest.class)));
    }

    /**
     *  按条件分页查询文档数据并高亮显示
     * @param esTestParam 查询条件实体
     * @return true 成功 / false 失败
     */
    @PostMapping("/getListHighLight")
    @ApiOperation("按条件分页查询文档数据并高亮显示")
    public ResponseData<Page<EsTest>> getListHighLight(@RequestBody @Validated EsTestParam esTestParam) {
        JSONObject jsonObject = JSONObject.fromObject(esTestParam);
        // 需要进行分词的字段内容
        jsonObject.put(QueryConstant.searchText, esTestParam.getTest2());
        // 需要进行高亮拼接的字段
        jsonObject.put(QueryConstant.HIGH_LIGHT_TEXT, "test2");
        // 开启分词查询  需要分词查询的字段为test2
        jsonObject.put(QueryConstant.MATCH,"test2");
        return responseData((EsUtil.searchHighLightPageByKeywords(esTestParam.getIndexName(), jsonObject, EsTestHighLight.class)));
    }

    @PostMapping("/testValidator")
    @ApiOperation("测试自定义校验注解")
    public ResponseData<Boolean> testValidator(@RequestBody @Validated TestForm testForm) {
        return responseData(true);
    }

    @PostMapping("/testValidator1")
    @ApiOperation("测试自定义校验注解1")
    public ResponseData<Boolean> testValidator1(@RequestBody @Validated TestForm1 testForm1) {
        return responseData(true);
    }




}

