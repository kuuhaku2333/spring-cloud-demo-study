package com.example.demo;

import com.example.demo.acp.sdk.SDKConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
// 开启定时任务功能
public class ForestApplication {

    /**
     * @Description 第三方支付 及 forest
     * 访问页面 http://127.0.0.1:8889/login
     **/
    public static void main(String[] args) {
        /* 记载支付配置项 */
        SDKConfig.getConfig().loadPropertiesFromSrc();
        SpringApplication.run(ForestApplication.class, args);
        System.out.println("第三方支付demo地址：" + "http://127.0.0.1:8889/login");
        System.out.println("接口文档地址：" + "http://127.0.0.1:8889/doc.html");
    }
}
