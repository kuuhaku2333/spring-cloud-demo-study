//package com.example.demo.forest;
//
//import com.dtflys.forest.annotation.*;
//import com.xy.aq.common.response.ResponseData;
//import com.xy.aq.sx.config.HttpSimpleInterceptor;
//import com.xy.aq.sx.provincialsxmodule.vo.PreService2020Vo;
//import com.xy.aq.sx.sxStoreroom.vo.PreServiceStoreroom2020Vo;
//import org.springframework.stereotype.Component;
//
//import java.util.HashMap;
//
//@Component
//@BaseRequest(interceptor = HttpSimpleInterceptor.class)//配置拦截器，主要用于解决请求错误异常
//public interface PaasInterface {
//
//    /**
//     * @Description 根据目录编码获取下一个业务项代码
//     * @Date 11:29 2021/4/20
//     **/
//    @Request(
//            url = "${getNextCodeByCatalogCodeURL}",
//            type = "get",
//            contentType = "application/json",
//            headers = {
//                    "Authorization: ${token}"
//            }
//    )
//    ResponseData<String> getNextCodeByCatalogCode(@Var("getNextCodeByCatalogCodeURL") String getNextCodeByCatalogCodeURL, @Query("catalogCode") String catalogCode, @Var("token") String authorizationToken);
//
//    /**
//     * 调用paas接口推送省事项
//     *
//     * @param getPublishPreService2020VoURL 接口地址
//     * @param preService2020Vo              推送对象
//     * @return
//     */
//    @Request(
//            url = "${getNextCodeByCatalogCodeURL}",
//            type = "post",
//            contentType = "application/json",
//            headers = {
//                    "Authorization: ${token}"
//            }
//    )
//    ResponseData<Boolean> publishPreService2020Vo(@Var("getNextCodeByCatalogCodeURL") String getPublishPreService2020VoURL, @Body PreService2020Vo preService2020Vo, @Var("token") String authorizationToken);
//
//    /**
//     * 调用paas接口推送自有事项
//     *
//     * @param getPublishPreService2020VoURL 接口地址
//     * @param preServiceStoreroom2020Vo     推送对象
//     * @return
//     */
//    @Request(
//            url = "${getNextCodeByCatalogCodeURL}",
//            type = "post",
//            contentType = "application/json",
//            headers = {
//                    "Authorization: ${token}",
//            }
//    )
//    ResponseData<Boolean> publishPreServiceStoreroom2020Vo(@Var("getNextCodeByCatalogCodeURL") String getPublishPreService2020VoURL, @Body PreServiceStoreroom2020Vo preServiceStoreroom2020Vo, @Var("token") String authorizationToken);
//
//    /**
//     * Token核验接口
//     *
//     * @param siteCode 应用系统站点ID
//     * @Header Authorization token
//     */
//    @Request(
//            url = "${getTokenVerifyUrl}",
//            type = "post",
//            contentType = "application/x-www-form-urlencoded",
//            headers = {
//                    "Authorization: ${token}",
//            }
//    )
//    ResponseData<Boolean> tokenVerify(@Var("getTokenVerifyUrl") String loginUrl, @Var("token") String token, @Body("siteCode") String siteCode);
//
//    /**
//     * 根据token查询对应的账号信息
//     *
//     * @param token token
//     * @Header Authorization token
//     */
//    @Request(
//            url = "${queryAccountByToken}",
//            type = "post",
//            contentType = "application/x-www-form-urlencoded",
//            headers = {
//                    "Authorization: ${token}",
//            }
//    )
//    ResponseData<HashMap<String, String>> queryAccountByToken(@Var("queryAccountByToken") String queryAccountByTokenUrl, @Var("token") String authorizationToken, @Body("token") String token);
//
//    /**
//     * 根据组织机构ID查询组织机构信息
//     *
//     * @param orgId 组织机构ID
//     * @Header Authorization token
//     */
//    @Request(
//            url = "${queryOrgByOrgId}",
//            type = "post",
//            contentType = "application/x-www-form-urlencoded",
//            headers = {
//                    "Authorization: ${token}",
//            }
//    )
//    ResponseData<HashMap<String, String>> queryOrgByOrgId(@Var("queryOrgByOrgId") String queryOrgByOrgIdUrl, @Var("token") String authorizationToken, @Body("orgId") String orgId);
//
//}