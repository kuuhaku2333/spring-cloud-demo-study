package com.example.demo.validator;
import com.example.demo.common.constant.StatusBusiness;
import com.example.demo.common.exception.BusinessException;
import com.example.demo.util.DateUtil;
import com.example.demo.validator.impl.DateTimeHandle;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 自定义日期校验
 * @author:mfkarj
 * @date : 2020/3/30 16:07
 * @Version 1.0
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DateTimeHandle.class)
public @interface DateValidator {

    /**
     *  在当前时间之后
     */
    String AFTERNOW = "after";

    /**
     *  在当前时间之前
     */
    String BEFORENOW = "before";

    /**
     *  时间校验格式
     */
    String format() default "yyyy-MM-dd";

    // 提示内容
    String message() default "";

    // 提示内容
    String value() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
