package com.example.demo.entity.esentity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * (Test2)实体类 po
 *
 * @author wbx
 * @since 2021-05-14 11:08:09
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EsTest {
    /**
     * 主键id
     */
    @NotBlank(message = "id不能为空", groups = {GroupUpdate.class})
    private String id;

    /**
     * test2
     */
    private String test2;

    /**
     * test1的主键ID
     */
    private String test1Id;

    public interface  GroupUpdate{

    }

}
