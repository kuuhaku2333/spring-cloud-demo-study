package com.example.demo.entity.esentity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (Test2)实体类 po
 *
 * @author wbx
 * @since 2021-05-14 11:08:09
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EsTestHighLight {
    /**
     * 主键id
     */
    private String id;

    /**
     * test2
     */
    private String test2;

    /**
     * test1的主键ID
     */
    private String test1Id;

    /**
     *  高亮字段
     */
    private String highLight;


}
