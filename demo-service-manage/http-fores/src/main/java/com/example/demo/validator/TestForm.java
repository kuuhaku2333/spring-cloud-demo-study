
package com.example.demo.validator;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotEmpty;

/**
 * 自定义日期校验配置模拟使用
 * @author wbx
 */
@DateTime
@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TestForm {
     /**
     * 早餐时间
     */
//    @NotEmpty(message = "早餐时间必填")
    @ApiModelProperty("早餐时间")
    @DateTimeValidator(format = "yyyy-MM-dd",after = "lunchTime",aftermessage = "早餐时间应该早于午餐时间")
    private String breakfastTime;

    /**
     * 午餐时间
     */
//    @NotEmpty(message = "午餐时间必填")
    @ApiModelProperty("午餐时间")
    @DateTimeValidator(format = "yyyy-MM-dd",before = "lunchTime",after = "dinnerTime",beforemessage = "早餐时间应该早于午餐时间",aftermessage = "午餐时间应该早于晚餐时间")
    private String lunchTime;


    /**
     * 晚餐时间
     */
    @NotEmpty(message = "晚餐时间不能为空")
    @ApiModelProperty("晚餐时间")
    @DateTimeValidator(format = "yyyy-MM-dd",before = "lunchTime",beforemessage = "午餐时间应该早于早餐时间")
    private String dinnerTime;

}
