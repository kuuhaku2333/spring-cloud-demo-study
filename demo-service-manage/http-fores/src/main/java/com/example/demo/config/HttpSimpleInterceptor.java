//package com.example.demo.config;
//
//import com.dtflys.forest.exceptions.ForestRuntimeException;
//import com.dtflys.forest.http.ForestRequest;
//import com.dtflys.forest.http.ForestResponse;
//import com.dtflys.forest.interceptor.Interceptor;
//import com.dtflys.forest.reflection.ForestMethod;
//import com.xy.aq.common.exception.BusinessException;
//
//import static com.xy.aq.common.constant.StatusBusiness.REQUEST_FOR_THIRD_PARTY_SERVICE_FAILED;
//
///*
// * @Description  HTTP请求的拦截器
// * @Date 10:23 2021/4/20
// * http://forest.dtflyx.com/docs/adv/interceptor
// **/
//public class HttpSimpleInterceptor implements Interceptor<Object> {
//
//    /**
//     * 该方法在被调用时，并在beforeExecute前被调用
//     *
//     * @Param request Forest请求对象
//     * @Param args 方法被调用时传入的参数数组
//     */
//    @Override
//    public void onInvokeMethod(ForestRequest request, ForestMethod method, Object[] args) {
//
//    }
//
//    /**
//     * 该方法在请求发送之前被调用, 若返回false则不会继续发送请求
//     *
//     * @Param request Forest请求对象
//     */
//    @Override
//    public boolean beforeExecute(ForestRequest request) {
//        // 执行在发送请求之前处理的代码
////        request.addHeader("accessToken", "11111111");  // 添加Header
////        request.addQuery("username", "foo");  // 添加URL的Query参数
//        return true;  // 继续执行请求返回true
//    }
//
//    /**
//     * 该方法在请求成功响应时被调用
//     */
//    @Override
//    public void onSuccess(Object data, ForestRequest request, ForestResponse response) {
//        // 执行成功接收响应后处理的代码
//        System.out.println("执行成功接收响应后处理 = " + data);
//
//    }
//
//    /**
//     * 该方法在请求发送失败时被调用
//     */
//    @Override
//    public void onError(ForestRuntimeException ex, ForestRequest request, ForestResponse response) {
//        // 执行发送请求失败后处理的代码
//        throw new BusinessException(REQUEST_FOR_THIRD_PARTY_SERVICE_FAILED.getStatus(), REQUEST_FOR_THIRD_PARTY_SERVICE_FAILED.getMessage() + "：[ " + ex.getMessage() + " & " + response.getRequest().getUrl() + " ]");
//    }
//
//    /**
//     * 该方法在请求发送之后被调用
//     */
//    @Override
//    public void afterExecute(ForestRequest request, ForestResponse response) {
//        // 执行在发送请求之后处理的代码
//    }
//}
