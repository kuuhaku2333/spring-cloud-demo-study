
package com.example.demo.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义日期校验配置
 * @Author:mfkarj
 * @Date: 2020/3/30 16:07
 * @Version 1.0
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DateTimeValidator {


    /**
     * 日期先后错误错误描述
     */
    String beforemessage() default "日期先后顺序不正确";

    /**
     * 日期先后错误错误描述
     */
    String aftermessage() default "日期先后顺序不正确";

    /**
     * 验证日期格式
     */
    String format() default "yyyyMMdd";

    /**
     * 验证当前日期值是否在该参数日期之前
     */
    String before() default "";

    /**
     * 是否允许与before时间相同
     */
    boolean identicalBefore() default true;

    /**
     * 验证当前日期值是否在该参数日期之后
     */
    String after() default "";

    /**
     * 是否允许与after时间相同
     */
    boolean identicalAfter() default true;
}
