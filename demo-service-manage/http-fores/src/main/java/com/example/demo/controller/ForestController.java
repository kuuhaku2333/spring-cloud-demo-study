package com.example.demo.controller;
import com.example.demo.forest.MyClient;
import com.example.demo.util.WebToolUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

/**
 * (Test1)控制层
 *
 * @author wbx
 * @since 2021-05-14 11:16:12
 */
@RestController
@RequestMapping("/forest")
public class ForestController {
    /**
     * 服务
     */
    @Resource
    private MyClient myClient;
    @Resource
    private HttpServletRequest request;
    /**
     *  百度地图秘钥ak  https://lbsyun.baidu.com/apiconsole/key#/home
     *  百度地图api文档地址 https://lbsyun.baidu.com/index.php?title=webapi/ip-api
     */
    private final String ak = "8DKLB15KYRWnDtzklrrIQQ2U5VnAVvEe";

    @GetMapping("/test1")
    public String test1Add(){
        return myClient.index();
    }

    @GetMapping("/test2")
    public String test2(){
        return "123";
    }


    /**
     *
     * 获取当前位置
     * {
     * 	"address": "CN|河南省|三门峡市|None|None|100|100",
     * 	"content": {
     * 		"address_detail": {
     * 			"province": "河南省",
     * 			"city": "三门峡市",
     * 			"adcode": "411202",
     * 			"street": "",
     * 			"district": "",
     * 			"street_number": "",
     * 			"city_code": 212
     *                },
     * 		"address": "河南省三门峡市",
     * 		"point": {
     * 			"x": "12379589.35",
     * 			"y": "4109198.86"
     *        }* 	},
     * 	"status": 0
     * }
     * @return 当前位置信息
     */
    @GetMapping("/getAddress")
    public Map<String, Object> getAddress() throws Exception {
        System.out.println("ip：" + WebToolUtils.getLocalIP());
        return  myClient.getAddress(null, ak);
    }


    /**
     * 获取用户ip地址
     * @return
     */
    public  String getIp(){
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }


    public static String publicip() {
        URL url = null;
        URLConnection urlconn = null;
        BufferedReader br = null;
        try {
            url = new URL("http://2017.ip138.com/ic.asp");//爬取的网站是百度搜索ip时排名第一的那个
            urlconn = url.openConnection();
            br = new BufferedReader(new InputStreamReader(
                    urlconn.getInputStream()));
            String buf = null;
            String get = null;
            while ((buf = br.readLine()) != null) {
                get += buf;
            }
            int where, end;
            for (where = 0; where < get.length() && get.charAt(where) != '['; where++) ;
            for (end = where; end < get.length() && get.charAt(end) != ']'; end++) ;
            get = get.substring(where + 1, end);
            return get;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
