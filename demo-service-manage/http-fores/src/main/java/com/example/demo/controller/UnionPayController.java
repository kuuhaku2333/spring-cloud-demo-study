package com.example.demo.controller;

import cn.hutool.core.util.IdUtil;
import com.example.demo.acp.demo.BaseConfig;
import com.example.demo.entity.AlipayBean;
import com.example.demo.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 网关支付
 */
@Controller
@RequestMapping("/pay")
@Slf4j
public class UnionPayController {

	@Autowired
	private HttpServletRequest request;

	@Resource
	private HttpServletResponse response;


	@Autowired
	private PayService payService;

	/**
	 * 支付接口
	 */
	@PostMapping("/toPay")
	@ResponseBody
	public void toPay(@RequestBody Map<String,String> param) throws IOException {
		// 交易金额
		String txnAmt = param.get("txnAmt");
		// 商品名称
		String commodityName = param.get("commodityName");
		request.setCharacterEncoding(BaseConfig.encoding);
		response.setContentType("text/html; charset="+ BaseConfig.encoding);
		String merId =  BaseConfig.merId;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String txnTime =sdf.format(new Date());
		// 订单ID
		String orderId = IdUtil.simpleUUID();
		log.info("订单ID: " + orderId);
		String html = payService.toPay(merId,txnAmt,orderId,txnTime,commodityName);
		response.getWriter().write(html);
	}

	/**
	 * 撤销支付接口
	 */
	@RequestMapping(value = "/undo",method = RequestMethod.GET)
	@ResponseBody
	public void undo(String origQryId,String txnAmt) throws IOException {
		response.getWriter().write(payService.undo(txnAmt,origQryId));
	}

	@GetMapping("/queryOrder")
	@ResponseBody
	public Map<String,String> queryOrder(String orderId) {
		return payService.query(orderId);
	}

	@PostMapping("/refund")
	@ResponseBody
	public Map<String, String> refund(String orderId) {
		return  payService.refund(orderId);
	}

//	/**
//	 * 支付回调 （一个订单回调多次  需处理）
//	 * @return
//	 */
//	@RequestMapping(value = "backRev",method = RequestMethod.POST)
//	@ResponseBody
//	public String backRev(){
//
//		LogUtil.writeLog("BackRcvResponse接收后台通知开始");
//		String encoding = BaseConfig.encoding;
//		// 获取银联通知服务器发送的后台通知参数
//		Map<String, String> reqParam = payService.getAllRequestParam(request);
//		LogUtil.printRequestLog(reqParam);
//
//		//重要！验证签名前不要修改reqParam中的键值对的内容，否则会验签不过
//		if (!AcpService.validate(reqParam, encoding)) {
//			LogUtil.writeLog("验证签名结果[失败].");
//			//验签失败，需解决验签问题
//		} else {
//			LogUtil.writeLog("验证签名结果[成功].");
//			//【注：为了安全验签成功才应该写商户的成功处理逻辑】交易成功，更新商户订单状态
//			//返回值accessType=0&bizType=000201&currencyCode=156&encoding=UTF-8&merId=898111948161230&orderId=201811291504119&queryId=691811301931137757518&respCode=00&respMsg=成功[0000000]&settleAmt=1&settleCurrencyCode=156&settleDate=1130&signMethod=01
//			String orderId =reqParam.get("orderId"); //获取后台通知的数据，其他字段也可用类似方式获取
//			String respCode = reqParam.get("respCode");
//			//判断respCode=00、A6后，对涉及资金类的交易，请再发起查询接口查询，确定交易成功后更新数据库。
//			LogUtil.writeLog("get respCode="+respCode);
//			switch (respCode){
//				case "00":
//					String backCode = payService.query(orderId);
//					//#TODO: 成功 更新状态
//					if("00".equals(backCode)){
//
//					}else{
//
//					}
//				break;
//				default:
//
//			}
//		}
//		LogUtil.writeLog("BackRcvResponse接收后台通知结束");
//		//返回给银联服务器http 200  状态码
//		return "ok";
//	}

	public void query(String orderId) {
	}

	public static void main(String[] args) {
		AlipayBean alipayBean = new AlipayBean();
		System.out.println("alipayBean = " + alipayBean);
	}
}

