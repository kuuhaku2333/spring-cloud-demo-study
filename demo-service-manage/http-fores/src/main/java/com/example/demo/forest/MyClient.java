package com.example.demo.forest;

import com.dtflys.forest.annotation.DataVariable;
import com.dtflys.forest.annotation.Get;
import com.dtflys.forest.annotation.Query;

import java.util.Map;

/**
 * <p>
 * 远程调用三方API
 * </p>
 */
public interface MyClient {

    public static String baiduMap = "http://api.map.baidu.com/location/ip";

    /**
     * 本地测试接口
     */
    @Get(url = "http://127.0.0.1:8889/forest/test2", headers = {""}, contentType = "application/json" )
    String index();

    @Get(url = "http://127.0.0.1:80/demo/hello?msg=${msg}")
    String hello(@DataVariable("msg") String msg);

    /**
     * 高德地图API
     */
    @Get(url = "http://ditu.amap.com/service/regeo?longitude=${longitude}&latitude=${latitude}")
    Map getLocation(@DataVariable("longitude") String longitude, @DataVariable("latitude") String latitude);

    @Get(url = MyClient.baiduMap)
    Map<String, Object> getAddress(@Query("ip") String ip, @Query("ak") String ak);

}
