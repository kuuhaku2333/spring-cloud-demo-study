package com.example.demo.util;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;

public class DateUtil
{

    public static final String C_DATE_DIVISION = "-";
    public static final String C_TIME_PATTON_DEFAULT = "yyyy-MM-dd HH:mm:ss";
    public static final String C_DATE_PATTON_DEFAULT = "yyyy-MM-dd";
    public static final String C_DATA_PATTON_YYYYMMDD = "yyyyMMdd";
    public static final String C_TIME_PATTON_HHMMSS = "HH:mm:ss";
    public static final int C_ONE_SECOND = 1000;
    public static final int C_ONE_MINUTE = 60000;
    public static final int C_ONE_HOUR = 0x36ee80;
    public static final long C_ONE_DAY = 0x5265c00L;

    public DateUtil(){ }
    //得到当前年
    public static String getCurrentYear(){
    	Calendar c;
		String year;
		c = new GregorianCalendar();
	    year = Integer.toString(c.get(1));
    	return year;
    }
  //得到当前月
    public static String getCurrentMonth(){
     	 Calendar c;
		 String year;
		 String month;
		 c = new GregorianCalendar();
	     year = Integer.toString(c.get(1));
	     int k_c = c.get(2);
	     k_c++;
	     month = Integer.toString(k_c);
	     if(month.length()==1) month="0"+month;
    	return month;
    }
  //得到当前日
    public static String getCurrentDay(){
    	 Calendar c;
		 String year;
		 String month;
	     String day;
	     c = new GregorianCalendar();
	     year = Integer.toString(c.get(1));
	     int k_c = c.get(2);
	     k_c++;
	     month = Integer.toString(k_c);
	     day = Integer.toString(c.get(5));
	     if(day.length()==1) day="0"+day;
    	return day;
    }
    public static String getTime(String DateTime){
    	Date date = parseDate("yyyy-MM-dd HH:mm:ss z", DateTime);
    	return format(date, "HH:mm");
    }
    public static String getYear(String DateTime){
    	Date date = parseDate("yyyy-MM-dd", DateTime);
    	return format(date, "yyyy");
    }
    public static String getMonth(String DateTime){
    	Date date = parseDate("yyyy-MM-dd", DateTime);
    	return format(date, "MM");
    }
    public static String getMonth(String DateTime,boolean IsIncluede0 ){
    	Date date = parseDate("yyyy-MM-dd", DateTime);
    	String MonStr=format(date, "MM");
    	if(MonStr.length()==2&&MonStr.substring(0, 1).equals("0")&&IsIncluede0==false) MonStr=MonStr.substring(1, 2);

    	return MonStr;
    }
    public static String getDay(String DateTime){
    	Date date = parseDate("yyyy-MM-dd", DateTime);
    	return format(date, "dd");
    }
    public static String  DateStr(String DateTime){
    	Date date = parseDate("yyyy-MM-dd", DateTime);
	    String dateStr=format(date, "yyyy")+"-"+format(date, "MM")+"-"+format(date, "dd");
	    if(date==null)dateStr="";
	    return dateStr;
    }

    /**
     *  获取格式化的当前时间字符串
     * @param format 时间格式
     * @return 格式化后的时间 2022-01-17 11:27:34
     */
    public static String  getLocalDateStr(String format) {
        String result = "";
        try {
            result = LocalDateTime.now().format(DateTimeFormatter.ofPattern(format));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     *
     * @param str 传入的日期格式字符串 2021-01-17
     * @param formatStr 格式化的日期格式 yyyy-MM-dd
     * @return true 传入时间符合时间格式 / false 传入时间不符合校验格式
     */
    public static boolean isValidDate(String str, String formatStr) {
        boolean convertSuccess = true;
        // 指定日期格式为四位年/两位月份/两位日期，注意yyyy/MM/dd区分大小写；
        SimpleDateFormat format = new SimpleDateFormat(formatStr);
        try {
            // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
            format.setLenient(false);
            format.parse(str);
        } catch (Exception e) {
            e.printStackTrace();
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            convertSuccess = false;
        }
        return convertSuccess;
    }

    public static String getDatetimeStr(String dateTime){
    	Date date = parseDate(C_TIME_PATTON_DEFAULT, dateTime);
    	return format(date, C_TIME_PATTON_DEFAULT);
    }
    public static String  getCurrentTimeStr(){
		//�õ���ǰ������ʱ�������
	     Calendar c;
		 String year;
		 String month;
	     String day;
	     String hour;
		 int hour_middle;
	     String minute;
	     String second;
		 String nanos;
		 String date;
	       c = new GregorianCalendar();
	       year = Integer.toString(c.get(1));
	       int k_c = c.get(2);
	       k_c++;
	       month = Integer.toString(k_c);
	       day = Integer.toString(c.get(5));
	       int j_c = c.get(10);
			hour = Integer.toString(j_c);
	       minute = Integer.toString(c.get(12));
	       second = Integer.toString(c.get(13));
	       nanos = Integer.toString(c.get(14));
		    k_c = c.get(9);
			if(k_c == 1)
	       {
			hour_middle = Integer.parseInt(hour)+12;
			hour=Integer.toString(hour_middle);
	       }
			if(month.length()==1) month="0"+month;
			if(day.length()==1) day="0"+day;
			if(hour.length()==1) hour="0"+hour;
			if(minute.length()==1) minute="0"+minute;
			if(second.length()==1) second="0"+second;
			if(nanos.length()==0) nanos="000";
			if(nanos.length()==1) nanos="00"+nanos;
			if(nanos.length()==2) nanos="0"+nanos;
			String currentTime = hour + ":" + minute + ":" +second;
			return currentTime;
	}

    public static String  getCurrentDateTimeStr(){
		//�õ���ǰ������ʱ�������
	     Calendar c;
		 String year;
		 String month;
	     String day;
	     String hour;
		 int hour_middle;
	     String minute;
	     String second;
		 String nanos;
		 String date;
	       c = new GregorianCalendar();
	       year = Integer.toString(c.get(1));
	       int k_c = c.get(2);
	       k_c++;
	       month = Integer.toString(k_c);
	       day = Integer.toString(c.get(5));
	       int j_c = c.get(10);
			hour = Integer.toString(j_c);
	       minute = Integer.toString(c.get(12));
	       second = Integer.toString(c.get(13));
	       nanos = Integer.toString(c.get(14));
		    k_c = c.get(9);
			if(k_c == 1)
	       {
			hour_middle = Integer.parseInt(hour)+12;
			hour=Integer.toString(hour_middle);
	       }
			if(month.length()==1) month="0"+month;
			if(day.length()==1) day="0"+day;
			if(hour.length()==1) hour="0"+hour;
			if(minute.length()==1) minute="0"+minute;
			if(second.length()==1) second="0"+second;
			if(nanos.length()==0) nanos="000";
			if(nanos.length()==1) nanos="00"+nanos;
			if(nanos.length()==2) nanos="0"+nanos;
			String currentDateTime = year + "-" +month + "-" + day + " " + hour + ":" + minute + ":" +second;
			return currentDateTime;
	}

    public static String  getCurrentDateTimeKeyStr(){
		//�õ���ǰ������ʱ�������
	     Calendar c;
		 String year;
		 String month;
	     String day;
	     String hour;
		 int hour_middle;
	     String minute;
	     String second;
		 String nanos;
		 String date;
	       c = new GregorianCalendar();
	       year = Integer.toString(c.get(1));
	       int k_c = c.get(2);
	       k_c++;
	       month = Integer.toString(k_c);
	       day = Integer.toString(c.get(5));
	       int j_c = c.get(10);
			hour = Integer.toString(j_c);
	       minute = Integer.toString(c.get(12));
	       second = Integer.toString(c.get(13));
	       nanos = Integer.toString(c.get(14));
		    k_c = c.get(9);
			if(k_c == 1)
	       {
			hour_middle = Integer.parseInt(hour)+12;
			hour=Integer.toString(hour_middle);
	       }
			if(month.length()==1) month="0"+month;
			if(day.length()==1) day="0"+day;
			if(hour.length()==1) hour="0"+hour;
			if(minute.length()==1) minute="0"+minute;
			if(second.length()==1) second="0"+second;
			if(nanos.length()==0) nanos="000";
			if(nanos.length()==1) nanos="00"+nanos;
			if(nanos.length()==2) nanos="0"+nanos;
			String currentDateTime = year + month + day + hour + minute + second;
//			得到四位随机数
			int first1;
			int first2;
			int first3;
			int first4;
			String first;
			Random random = new Random();
			first1=random.nextInt(10);
			first2=random.nextInt(10);
			first3=random.nextInt(10);
			first4=random.nextInt(10);
			first=String.valueOf(first1)+String.valueOf(first2)+String.valueOf(first3)+String.valueOf(first4);
			String keyStr=currentDateTime+first;

			return keyStr;
	}

    public static Date getCurrentDate()
    {

        Calendar cal = Calendar.getInstance();
        Date currDate = cal.getTime();
        return currDate;
    }
    public static Date getCurrentDate2()
    {
    	SimpleDateFormat sdf_JP = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        sdf_JP.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        //Calendar cal = Calendar.getInstance();
        //Date currDate = cal.getTime();
        String strDate = sdf_JP.format(new Date());

        return parseDateTime(strDate);
    }


    public static String getCurrentDateStr()
    {
        Calendar cal = Calendar.getInstance();
        Date currDate = cal.getTime();
        return format(currDate);
    }

    /**
     * 获取本月的第一天
     * @param dateFormat
     * @return
     * @throws ParseException
     */
    public static String getFirstOfMonth(String dateFormat)  {
        //获取当前月第一天：
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天
        String first = new SimpleDateFormat(dateFormat).format(c.getTime());
        return first;
    }

    /**
     * 获取本月的最后一天
     * @param dateFormat
     * @return
     * @throws ParseException
     */
    public static String getLastOfMonth(String dateFormat) {
        //获取当前月最后一天
        Calendar ca = Calendar.getInstance();
        ca.setTime(new Date());
        ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
        String last = new SimpleDateFormat(dateFormat).format(ca.getTime());
        return last;
    }

    /**
     * 获取本周的第一天日期
     * @param dateFormat
     * @return
     * @throws ParseException
     */
    public static String getFirstOfWeek(String dateFormat)  {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int d = 0;
        if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
            d = -6;
        } else {
            d = 2 - cal.get(Calendar.DAY_OF_WEEK);
        }
        cal.add(Calendar.DAY_OF_WEEK, d);
        // 所在周开始日期
        String data1 = new SimpleDateFormat(dateFormat).format(cal.getTime());

        return data1;

    }

    /**
     * 获取本周的最后一天日期
     * @param dateFormat
     * @return
     * @throws ParseException
     */
    public static String getLastOfWeek(String dateFormat) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int d = 0;
        if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
            d = -6;
        } else {
            d = 2 - cal.get(Calendar.DAY_OF_WEEK);
        }
        cal.add(Calendar.DAY_OF_WEEK, d);
        // 所在周结束日期
        cal.add(Calendar.DAY_OF_WEEK, 6);
        String data2 = new SimpleDateFormat(dateFormat).format(cal.getTime());
        return  data2;

    }

    public static String getYesterdayStr(String strFormat){
    	Calendar   cal   =   Calendar.getInstance();
    	cal.add(Calendar.DATE,   -1);
    	String yesterday = new SimpleDateFormat( strFormat).format(cal.getTime());
    	return yesterday;
    }

    public static String getCurrentDateStr(String strFormat)
    {
        Calendar cal = Calendar.getInstance();
        Date currDate = cal.getTime();
        return format(currDate, strFormat);
    }

    public static Date parseDate(String dateValue)
    {
    	dateValue=dateValue.replaceAll("/", "-");
        return parseDate("yyyy-MM-dd", dateValue);
    }


    public static Date parseDate2(String dateValue)
    {
        return parseDate("yyyy-MM-dd HH:mm:ss", dateValue);
    }

    public static Date parseDateTime(String dateValue)
    {
        return parseDate("yyyy-MM-dd HH:mm:ss", dateValue);
    }

    public static Date parseDate(String strFormat, String dateValue)
    {
        if(dateValue == null)
        {
            return null;
        }
        if(strFormat == null)
        {
            strFormat = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(strFormat);
        Date newDate = null;
        try
        {
            newDate = dateFormat.parse(dateValue);
        }
        catch(ParseException pe)
        {
            newDate = null;
        }
        return newDate;
    }

    public static String format(String dateValue, String format)
    {
    	if(format == null || "".equals(format)) format = C_DATE_PATTON_DEFAULT;
        return format(parseDate(C_DATE_PATTON_DEFAULT, dateValue), format);
    }

    public static String format(String dateValue,String inFormat, String outFormat)
    {
    	if (outFormat == null || "".equals(outFormat)) outFormat = C_DATE_PATTON_DEFAULT;
        return format(parseDate(inFormat, dateValue), outFormat);
    }

    public static String format(Date aTs_Datetime)
    {
        return format(aTs_Datetime, "yyyy-MM-dd");
    }

    public static String formatTime(Date aTs_Datetime)
    {
        return format(aTs_Datetime, "yyyy-MM-dd HH:mm:ss");
    }

    public static String format(Date aTs_Datetime, String as_Pattern)
    {
        if(aTs_Datetime == null || as_Pattern == null)
        {
            return null;
        } else
        {
            SimpleDateFormat dateFromat = new SimpleDateFormat();
            dateFromat.applyPattern(as_Pattern);
            return dateFromat.format(aTs_Datetime);
        }
    }

    public static String formatTime(Date aTs_Datetime, String as_Format)
    {
        if(aTs_Datetime == null || as_Format == null)
        {
            return null;
        } else
        {
            SimpleDateFormat dateFromat = new SimpleDateFormat();
            dateFromat.applyPattern(as_Format);
            return dateFromat.format(aTs_Datetime);
        }
    }

    public static String getFormatTime(Date dateTime)
    {
        return formatTime(dateTime, "HH:mm:ss");
    }



    public static String format(Timestamp aTs_Datetime, String as_Pattern)
    {
        if(aTs_Datetime == null || as_Pattern == null)
        {
            return null;
        } else
        {
            SimpleDateFormat dateFromat = new SimpleDateFormat();
            dateFromat.applyPattern(as_Pattern);
            return dateFromat.format(aTs_Datetime);
        }
    }

    public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(5, days);
        return cal.getTime();
    }

    public static int daysBetween(Date date1, Date date2)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        long time1 = cal.getTimeInMillis();
        cal.setTime(date2);
        long time2 = cal.getTimeInMillis();
        long between_days = (time2 - time1) / 0x5265c00L;
        return Integer.parseInt(String.valueOf(between_days));
    }
    public static String timeBetween(Date date1, Date date2)
    {
    	if(date1==null||date2==null) return "";
       String resultStr="";
       long miaocount=0;
       long mincount=0;
       long hourCount=0;
       long dayCount=0;
       long DAY = 24L * 60L * 60L * 1000L;
       long HOUR=60L * 60L * 1000L;
       long MIN=60L * 1000L;
       long MIAO=1000L;
       long miaolen=date2.getTime()-date1.getTime();
       if(miaolen>60)miaocount=(miaolen / MIAO)%60;
           mincount=(miaolen / MIN)%60;
           hourCount=(miaolen / HOUR)%24;
           dayCount=miaolen / DAY;
       resultStr=dayCount+"天"+hourCount+"小时"+mincount+"分"+miaocount+"秒";
       if(dayCount==0)
    	   resultStr=hourCount+"小时"+mincount+"分"+miaocount+"秒";
       if(dayCount==0&&hourCount==0)
    	   resultStr=mincount+"分"+miaocount+"秒";
       if(dayCount==0&&hourCount==0&&mincount==0)
    	   resultStr=miaocount+"秒";
        return resultStr;
    }
    public static long getRelativeDays(Date date)
    {
        Date relativeDate = parseDate("yyyy-MM-dd", "1977-12-01");
        return (long)daysBetween(relativeDate, date);
    }

    public static Date getDateBeforTwelveMonth()
    {
        String date = "";
        Calendar cla = Calendar.getInstance();
        cla.setTime(getCurrentDate());
        int year = cla.get(1) - 1;
        int month = cla.get(2) + 1;
        if(month > 9)
        {
            date = (new StringBuilder(String.valueOf(String.valueOf(year)))).append("-").append(String.valueOf(month)).append("-").append("01").toString();
        } else
        {
            date = (new StringBuilder(String.valueOf(String.valueOf(year)))).append("-").append("0").append(String.valueOf(month)).append("-").append("01").toString();
        }
        Date dateBefore = parseDate(date);
        return dateBefore;
    }

    public static Date addDate(String date)
    {
        if(date == null)
        {
            return null;
        } else
        {
            Date tempDate = parseDate("yyyy-MM-dd", date);
            String year = format(tempDate, "yyyy");
            String month = format(tempDate, "MM");
            String day = format(tempDate, "dd");
            GregorianCalendar calendar = new GregorianCalendar(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(day));
            calendar.add(5, 1);
            return calendar.getTime();
        }
    }

    public static Date addYear (String date, String years) {
        if(date == null) {
            return null;
        } else {
            Date tempDate = parseDate("yyyy-MM-dd", date);
            String year = format(tempDate, "yyyy");
            String month = format(tempDate, "MM");
            String day = format(tempDate, "dd");
            GregorianCalendar calendar = new GregorianCalendar(Integer.parseInt(year) + Integer.parseInt(years), Integer.parseInt(month) - 1, Integer.parseInt(day));
            //calendar.add(5, 1);
            return calendar.getTime();
        }
    }

    /**
     * 判断date1是否在date2,date3之间，date1>=date2->true && date1<=date3->true，除此之外返回false
     * date1,date2,date3格式为：yyyy-MM-dd
     * @param date1
     * @param date2
     * @param date3
     * @return
     */
    public static boolean compareTo(String date1, String date2, String date3) {
    	if(date1 != null && date2 !=null&&date3 != null){
    		return (Long.parseLong(date1.replace("-", "")) >= Long.parseLong(date2.replace("-", "")) && Long.parseLong(date1.replace("-", "")) <= Long.parseLong(date3.replace("-", "")));
    	}else{
    		return false;
    	}

    }
    /**
     * 判断date1和date2的大小，大于返回1，等于返回0，小于返回-1
     * @param date1 格式为：yyyy-MM-dd
     * @param date2 格式为：yyyy-MM-dd
     * @return
     */
    public static int compareTo(String date1, String date2) {
    	return date1.compareTo(date2);
    }

  //判断当前时间是否在两个日期之间
    /**
     * 判断datetime1和datetime2的大小，大于返回2，等于返回0，小于返回-1
     * @param datetime1
     * @param datetime2
     * @return
     */
    public static int compareDatetime(String datetime1,String datetime2)
    {
    	int res = 0;
    	//date1 暂时没有用到
    	String currentDate = getCurrentDateStr().replace("-", ""); //当前日期
    	if(datetime1 != null && datetime2 !=null){
//    		String d1 = DateUtil.format(date1, "yyyymmdd");
//    		String d2 = DateUtil.format(date2, "yyyymmdd");
    		long dt1 = Long.parseLong(DateUtil.format(datetime1,C_TIME_PATTON_DEFAULT,"yyyyMMddHHmmss"));
    		long dt2 = Long.parseLong(DateUtil.format(datetime2, C_TIME_PATTON_DEFAULT,"yyyyMMddHHmmss"));
    		if( dt1 > dt2){
    			res = 1;
    		} else if( dt1 == dt2){
    			res = 0;
    		} else if( dt1 < dt2){
    			res = -1;
    		}
    	}
        return res;
    }


    //判断当前时间是否在两个日期之间
    public static boolean compareTo(Date date1,Date date2)
    {
    	//date1 暂时没有用到
    	String currentDate = getCurrentDateStr().replace("-", ""); //当前日期
    	if(date1 != null && date2 !=null){
//    		String d1 = DateUtil.format(date1, "yyyymmdd");
//    		String d2 = DateUtil.format(date2, "yyyymmdd");

    		return (Long.parseLong(DateUtil.format(date2, "yyyymmdd")) >= Long.parseLong(currentDate));
    	}else{
    		return true;
    	}

    }

  //判断当前时间是否在两个日期之间
    public static boolean compareTo(Date date)
    {
    	//date1 暂时没有用到
    	String currentDate = getCurrentDateStr().replace("-", ""); //当前日期
    	if(date !=null){
//    		String d1 = DateUtil.format(date1, "yyyymmdd");
//    		String d2 = DateUtil.format(date2, "yyyymmdd");

    		return (Long.parseLong(DateUtil.format(date, "yyyymmdd")) >= Long.parseLong(currentDate));
    	}else{
    		return true;
    	}

    }

    public static boolean compareToNow(String date)
    {
    	//date1 暂时没有用到
    	String currentDate = formatTime(new Date()).replace("-", "").replace(":", "").replace(" ", ""); //当前日期
    	if(date != null){
//    		String d1 = DateUtil.format(date1, "yyyymmdd");
//    		String d2 = DateUtil.format(date2, "yyyymmdd");

    		return (Long.parseLong(DateUtil.format(DateUtil.parseDate("yyyyMMddHHmmss", date), "yyyyMMddHHmmss")) >= Long.parseLong(currentDate));
    	}else{
    		return false;
    	}

    }



    /**
     * 某月总共多少天
     * @param date 格式为：yyyy-MM-dd
     * @return
     */
    public static int getDaysOfMonth(String date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(parseDate("yyyy-MM-dd", date));
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
    /**
     * 当前月总共多少天
     * @return
     */
    public static int getDaysOfCurMonth() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
    /**
     * @param date 格式为：yyyy-MM-dd
     * @return
     */
    public static String getWeekOfDate(String date, Map<String,Object> params) {
    	ReloadableResourceBundleMessageSource messageSource = (ReloadableResourceBundleMessageSource) params.get("messageSource");
		Locale curLocale = (Locale) params.get("curLocale");
        String[] weekDays = {
        		messageSource.getMessage("label.su", null, curLocale),
        		messageSource.getMessage("label.m", null, curLocale),
        		messageSource.getMessage("label.t", null, curLocale),
        		messageSource.getMessage("label.w", null, curLocale),
        		messageSource.getMessage("label.th", null, curLocale),
        		messageSource.getMessage("label.f", null, curLocale),
        		messageSource.getMessage("label.s", null, curLocale)
        }; //"日", "一", "二", "三", "四", "五", "六"
        Calendar cal = Calendar.getInstance();
        cal.setTime(parseDate("yyyy-MM-dd", date));
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    /**
     * @param date 格式为：yyyy-MM-dd
     * @return
     */
    public static String getAllWeekOfDate(String date, Map<String,Object> params) {
    	ReloadableResourceBundleMessageSource messageSource = (ReloadableResourceBundleMessageSource) params.get("messageSource");
		Locale curLocale = (Locale) params.get("curLocale");
        String[] weekDays = {
        		messageSource.getMessage("label.sunday", null, curLocale),
        		messageSource.getMessage("label.monday", null, curLocale),
        		messageSource.getMessage("label.tuesday", null, curLocale),
        		messageSource.getMessage("label.wednesday", null, curLocale),
        		messageSource.getMessage("label.thursday", null, curLocale),
        		messageSource.getMessage("label.friday", null, curLocale),
        		messageSource.getMessage("label.saturday", null, curLocale)
        }; //"日", "一", "二", "三", "四", "五", "六"
        Calendar cal = Calendar.getInstance();
        cal.setTime(parseDate("yyyy-MM-dd", date));
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    /**
     * 某一天是周几
     * @param date 格式为：yyyy-MM-dd
     * @return
     */
    public static int getWeekDayOfMonth(String date) {
        int[] weekDays = {7, 1, 2, 3, 4, 5, 6};
        Calendar cal = Calendar.getInstance();
        cal.setTime(parseDate("yyyy-MM-dd", date));
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    /**
     * 某天是周几
     * @param date 格式为：yyyy-MM-dd
     * @return
     */
    public static String getWeekOf1stDayOfMonth(String date) {
        String[] weekDays = {"7", "1", "2", "3", "4", "5", "6"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(parseDate("yyyy-MM-dd", date));
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    public static String getWeek(Date date){
        String[] weeks = {"星期日","星期一","星期二","星期三","星期四","星期五","星期六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int week_index = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if(week_index<0){
            week_index = 0;
        }
        return weeks[week_index];
    }

    /**
    *  格式为：yyyy-MM-dd
    * @return
    */
   public static String getLastDayOfMonth() {
       SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
       Calendar cale = Calendar.getInstance();
       cale.add(Calendar.MONTH, 1);
       cale.set(Calendar.DAY_OF_MONTH, 0);
       String lastday = format.format(cale.getTime());
       return lastday;
   }

   /****
    * 传入具体日期 ，返回具体日期增加n个月。
    * @param date 日期(2017-04-13)
    * @return 2017-05-13
    * @throws ParseException
    */
   public static  Date addMonth(Date date, int add)  {
       //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
       //Date dt = sdf.parse(date);
       Calendar rightNow = Calendar.getInstance();
       rightNow.setTime(date);
       rightNow.add(Calendar.MONTH, add);
       Date dt1 = rightNow.getTime();
       //String reStr = sdf.format(dt1);
       return dt1;
   }


   /**
    * 时间相减，time1-time2=
    * @param time1 格式为08:10
    * @param time2 格式为01:30
    * @return
    */
   public static String timeSubtract(String time1, String time2){
	   	//String a = "0810";
		//String b = "0130";
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy");
		long result;
		String res = null;
		try {
			result = sdf.parse(time1).getTime() - sdf.parse(time2).getTime()+sdf1.parse("1970").getTime();
			Date d1 = new Date(result);
			//System.out.println(sdf.format(d1).toString());
			res = sdf.format(d1).toString();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//加上1970年
		return res;
   }

   /**
    * 时间相减，time1-time2=
    * @param time1 格式为08:10
    * @param time2 格式为01:30
    * @return
    */
   public static int timeSubtract2(String time1, String time2){
	   	//String a = "0810";
		//String b = "0130";
	   	int res = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy");
		long result = 0;
		try {
			result = sdf.parse(time1).getTime() - sdf.parse(time2).getTime();
			//Date d1 = new Date(result);
			//System.out.println(sdf.format(d1).toString());
			//res = sdf.format(d1).toString();
			res = Math.abs(Integer.valueOf(String.valueOf(result/1000/60)));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//加上1970年
		return res;
   }

   /**
    * 计算两个日期时间的差值是xx天xx小时xx分钟
    * date1应大于date2
    * @param date1 格式为yyyy-MM-dd HH:mm:ss
    * @param date2 格式为yyyy-MM-dd HH:mm:ss
    * @return
    */
   public static String dateTimeSubtract(String date1, String date2, ReloadableResourceBundleMessageSource messageSource, Locale curLocale){
	   String res = "";
	   DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   try {
	     Date d1 = df.parse(date1);
	     Date d2 = df.parse(date2);
	     long diff = d1.getTime() - d2.getTime();//这样得到的差值是微秒级别
	     long days = diff / (1000 * 60 * 60 * 24);
	     long hours = (diff-days*(1000 * 60 * 60 * 24))/(1000* 60 * 60);
	     long minutes = (diff-days*(1000 * 60 * 60 * 24)-hours*(1000* 60 * 60))/(1000* 60);
	     res =  ""+days+messageSource.getMessage("label.days", null, curLocale)+hours+messageSource.getMessage("label.hours", null, curLocale)+minutes+messageSource.getMessage("label.minutes", null, curLocale);
	   }catch (Exception e) {
		   e.printStackTrace();
	   }
	   return res;
   }



   /**
    * 计算两个日期时间的差值是xx天
    * date1应大于date2
    * @param date1 格式为yyyy-MM-dd HH:mm:ss
    * @param date2 格式为yyyy-MM-dd HH:mm:ss
    * @return
    */
   public static String dateSubtract(String date1, String date2){
	   String res = "";
	   DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   try {
	     Date d1 = df.parse(date1);
	     Date d2 = df.parse(date2);
	     long diff = d1.getTime() - d2.getTime();//这样得到的差值是微秒级别
	     long days = diff / (1000 * 60 * 60 * 24);
	    res = String.valueOf(days);
	   }catch (Exception e) {
		   e.printStackTrace();
	   }
	   return res;
   }

    /**
     * 计算两个日期时间的差值是多少秒
     * date1应大于date2
     * @param date1 格式为yyyy-MM-dd HH:mm:ss
     * @param date2 格式为yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String secondSubtract(String date1, String date2){
        String res = "";
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d1 = df.parse(date1);
            Date d2 = df.parse(date2);
            long diff = d1.getTime() - d2.getTime();//这样得到的差值是微秒级别
            long seconds = diff / (1000);
            res = String.valueOf(seconds);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

   /**
    * 时间相加，time1+time2=
    * @param time1 格式为08:10
    * @param time2 格式为01:30
    * @return
    */
   public static String timeAddition(String time1, String time2){
	   	//String a = "0810";
		//String b = "0130";
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy");
		long result;
		String res = null;
		try {
			result = sdf.parse(time1).getTime() + sdf.parse(time2).getTime()+sdf1.parse("1970").getTime();
			Date d1 = new Date(result);
			//System.out.println(sdf.format(d1).toString());
			res = sdf.format(d1).toString();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//加上1970年
		return res;
   }
   /**
    * 时间加分钟
    * @param time 格式为：08:10
    * @param minute 50
    * @return 返回时间格式 09:00
    */
   public static String addMinute(String time, String minute){
	   String res = "";
	   SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
	   Calendar cal = Calendar.getInstance();
	   cal.setTime(parseDate("HH:mm", time));
	   cal.add(Calendar.MINUTE, Integer.valueOf(minute));
	   res = sdf.format(cal.getTime());
	   return res;
   }
   /**
    * 分钟转时间
    * @param minute
    * @return
    */
   public static String minuteToTime(String minute){
	   String time = "";
	   int hour = Integer.valueOf(minute)/60 ;
	   int minu = Integer.valueOf(minute) - (hour * 60);
	   time = (hour < 10 ?"0"+String.valueOf(hour):String.valueOf(hour))+":"+ (minu < 10 ? "0"+String.valueOf(minu):String.valueOf(minu));
	   return time;
   }
   /**
    * 时间转分钟
    * @param time 格式为：08:10 意思为8小时10分钟
    * @return
    */
   public static String timeToMinute(String time){
	   String minute = "0";
	   String[] timeArr = time.split(":");
	   minute = String.valueOf(Integer.valueOf(timeArr[0])*60 + Integer.valueOf(timeArr[1]));
	   return minute;
   }
   /**
    * 时间转小时
    * @param time 格式为：08:30 意思为8.5小时
    * @return
    */
   public static String timeToHour(String time){
	   if("".equals(time) || time == null){
		   time = "00:00";
	   }
	   String minute = "0";
	   String[] timeArr = time.split(":");
	   minute = String.valueOf((Float.valueOf(timeArr[0]) + Float.valueOf(timeArr[1])/60));
	   return minute;
   }
   /**
    * 比较时间大小，time1大于time2返回1，等于返回0，小于返回2
    * @param time1
    * @param time2
    * @return
    */
   public static int compareTime(String time1, String time2){
	   int res = 0;
	   DateFormat df = new SimpleDateFormat("HH:mm:ss");//创建日期转换对象HH:mm:ss为时分秒，年月日为yyyy-MM-dd
       try {
           Date dt1 = df.parse(time1+":00");//将字符串转换为date类型
           Date dt2 = df.parse(time2+":00");
           if(dt1.getTime()>dt2.getTime()){
        	   res = 1;
           } else if(dt1.getTime()==dt2.getTime()){
        	   res = 0;
           } else if(dt1.getTime() < dt2.getTime()){
        	   res = -1;
           }
       } catch (ParseException e) {
           e.printStackTrace();
       }
       return res;
   }
   /**
    * 比较时间，time1如果接近time2，返回1，如果接近time3返回2
    * @param time1
    * @param time2
    * @param time3
    * @return
    */
   public static int compareTime(String time1, String time2, String time3){
	   int res = 0;
	   int h1 = Integer.valueOf(time1.split(":")[0]);
	   int h2 = Integer.valueOf(time2.split(":")[0]);
	   int h3 = Integer.valueOf(time3.split(":")[0]);
	   if (h1 >= h2 && h1 <= h3) {
		   if (h1-h2 > h3-h1) {
			   res = 2;
		   } else if (h1-h2 <= h3-h1) {
			   res = 1;
		   } else {
			   res = 0;
		   }
	   } else if (h1 < h2) {
		   res = 1;
	   } else if (h1 > h3) {
		   res = 2;
	   }

       return res;
   }

   /**
    * 比较时间，time1如果接近time2，返回1，如果接近time3返回2
    * @param time1
    * @param time2
    * @param time3
    * @return
    */
   public static int compareTime2(String time1, String time2, String time3){
	   int res = 0;
	   int h1 = Integer.valueOf(time1.split(":")[0]);
	   int h2 = Integer.valueOf(time2.split(":")[0]);
	   int h3 = Integer.valueOf(time3.split(":")[0]);
	   if(h1 >= h3){
		  res = 2;
	   } else if(h1 >= h2){
		   res = 1;
	   } else {
		   res = 0;
	   }
       return res;
   }

   public static String getFirstDayByCurWeek(String curDate){
	   Calendar cal = Calendar.getInstance();
       cal.setTime(parseDate(C_DATE_PATTON_DEFAULT, curDate));
       int d = 0;
       if(cal.get(Calendar.DAY_OF_WEEK)==1){
           d = -6;
       }else{
           d = 2-cal.get(Calendar.DAY_OF_WEEK);
       }
       cal.add(Calendar.DAY_OF_WEEK, d);
       //所在周开始日期
       String monday = format(cal.getTime(), C_DATE_PATTON_DEFAULT);
       return monday;
   }

   public static String getLastDayByCurWeek(String curDate){
	   Calendar cal = Calendar.getInstance();
       cal.setTime(parseDate(C_DATE_PATTON_DEFAULT, curDate));
       int d = 0;
       if(cal.get(Calendar.DAY_OF_WEEK)==1){
           d = -6;
       }else{
           d = 2-cal.get(Calendar.DAY_OF_WEEK);
       }
       cal.add(Calendar.DAY_OF_WEEK, d);
       cal.add(Calendar.DAY_OF_WEEK, 6);
       //所在周结束日期
       String sunday = format(cal.getTime(), C_DATE_PATTON_DEFAULT);
       return sunday;
   }
   /**
    * 根据时间判断是上午(5-12)，下午(12-19)，晚上(19-23)
    * @param time
    * @return
    */
   public static int getDateSx(String time){
	   int res = 0;
	   SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
	   Calendar cal = Calendar.getInstance();
	   cal.setTime(parseDate("HH:mm", time));
	   int hour = cal.get(Calendar.HOUR_OF_DAY);
	   if (hour > 5 && hour < 12) {
		   res = 1;
	   } else if (hour > 12 && hour < 19) {
		   res = 2 ;
	   } else if (hour > 19) {
		   res = 3 ;
	   }
	   return res;
   	}

    public static String getDatetime(int year, int month, int day, int hour, int minute, int second){
    	StringBuffer dt = new StringBuffer("");
    	dt.append(year).append("-");
    	dt.append(month>9?month:"0"+month).append("-");
    	dt.append(day>9?day:"0"+day).append(" ");
    	dt.append(hour>9?hour:"0"+hour).append(":");
    	dt.append(minute>9?minute:"0"+minute).append(":");
    	dt.append(second>9?second:"0"+second);
    	return dt.toString();
    }

    /**
     * 获取昨天开始时间和结束时间
     * @return
     */
    public static Map getYesterdayRange() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map condition=new HashMap();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        condition.put("endDate",df.format(calendar.getTime()));
        calendar.set(Calendar.HOUR_OF_DAY,-24);
        condition.put("startDate",df.format(calendar.getTime()));
        return condition;
    }

    /**
     * 获得近一周的开始时间和结束时间
     * @return
     */
    public static Map getDaySevenRange(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map condition=new HashMap();
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        calendar.set(Calendar.HOUR_OF_DAY,24);
        condition.put("endDate",df.format(calendar.getTime()));
        calendar.set(Calendar.HOUR_OF_DAY,-168);
        condition.put("startDate",df.format(calendar.getTime()));
        return condition;
    }

    /**
     * 获得近一月的开始时间和结束时间
     * @return
     */
    public static Map getDayTRange(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map condition=new HashMap();
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        calendar.set(Calendar.HOUR_OF_DAY,24);
        condition.put("endDate",df.format(calendar.getTime()));
        calendar.set(Calendar.HOUR_OF_DAY,-720);
        condition.put("startDate",df.format(calendar.getTime()));
        return condition;
    }

    /**
     * 获得近一年的开始时间和结束时间
     * @return
     */
    public static Map getYearTRange(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map condition=new HashMap();
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        calendar.set(Calendar.HOUR_OF_DAY,24);
        condition.put("endDate",df.format(calendar.getTime()));
        calendar.set(Calendar.HOUR_OF_DAY,-8640);
        condition.put("startDate",df.format(calendar.getTime()));
        return condition;
    }

    /**
     * @Description:    获取两个时间字符串之间工作日天数
     * @Author: WBX
     * @Date: 2021/1/20 17:21
     * @param strStartDate:  开始日期
     * @param strEndDate:    结束日期
     * @return: int
     **/
    public static int workDays(String strStartDate, String strEndDate) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        Calendar cl1 = Calendar.getInstance();
        Calendar cl2 = Calendar.getInstance();

        try {
            cl1.setTime(df.parse(strStartDate));
            cl2.setTime(df.parse(strEndDate));

        } catch (ParseException e) {
            System.out.println("日期格式非法");
            e.printStackTrace();
        }
        AtomicInteger count = new AtomicInteger();
        while (cl1.compareTo(cl2) <= 0) {

            if (cl1.get(Calendar.DAY_OF_WEEK) != 7 && cl1.get(Calendar.DAY_OF_WEEK) != 1) {
                count.getAndIncrement();
            }
            cl1.add(Calendar.DAY_OF_MONTH, 1);
        }
        return count.get() - 1;

    }

//    /**
//     * @Description:  判断指定日期是否为法定节假日
//     * @Author: WBX
//     * @Date: 2021/1/21 8:21
//     * @param date:   日期格式字符串 yyyy-MM-dd
//     * @return: boolean  true是 / false否
//     **/
//    public static Boolean validatHoliday(String date) {
//        Boolean flag = false;
//        String currDate = "";
//        if(StringUtils.isBlank(date)) {
//            return false;
//        }
//        String[] split1 = date.split("-");
//        if(split1.length == 3) {
//            date = split1[0] + "-" + split1[1];
//            currDate = split1[1] + "-" + split1[2];
//         }
//        String s = HttpUtils.doGetJson("http://timor.tech/api/holiday/year/" + date);
//        Map<String, Object> map = GsonUtil.GsonToMaps(s);
//        if("0.0".equals(String.valueOf(map.get("code")))) {
//            Map<String, Object> holidayMap = (Map<String, Object>) map.get("holiday");
//            if(MapUtils.isNotEmpty(holidayMap)) {
//                // 说明当天是法定节假日
//                if(holidayMap.containsKey(currDate)) {
//                    flag = true;
//                }
//            }
//        }
//        return flag;
//    }
//
//
//    /**
//     * 获取两个日期之间的所有日期(字符串格式, 按天计算)
//     *
//     * @param startTime String 开始时间 yyyy-MM-dd
//     * @param endTime  String 结束时间 yyyy-MM-dd
//     * @return
//     */
//    public static List<String> getBetweenDays(String startTime, String endTime) {
//        if(StringUtils.isEmpty(startTime) || StringUtils.isEmpty(endTime)){
//            return null;
//        }
//        //1、定义转换格式
//        SimpleDateFormat df  = new SimpleDateFormat("yyyy-MM-dd");
//
//        Date start = null;
//        Date end = null;
//        try {
//            start = df.parse(startTime);
//            end = df.parse(endTime);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        if(StringTools.isEmpty(startTime) || StringTools.isEmpty(endTime)){
//            return null;
//        }
//        List<String> result = new ArrayList<String>();
//
//        Calendar tempStart = Calendar.getInstance();
//        tempStart.setTime(start);
//
//        tempStart.add(Calendar.DAY_OF_YEAR, 1);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        Calendar tempEnd = Calendar.getInstance();
//        tempEnd.setTime(end);
//        result.add(sdf.format(start));
//        while (tempStart.before(tempEnd)) {
//            result.add(sdf.format(tempStart.getTime()));
//            tempStart.add(Calendar.DAY_OF_YEAR, 1);
//        }
//        return result;
//    }

    public static void main(String args[])
    {
        System.out.println(getLocalDateStr(DateUtil.C_TIME_PATTON_DEFAULT));

        //System.out.println(DateUtil.format(DateUtil.addMonth(new Date(), -1)));
//    	System.out.println(DateUtil.timeSubtract("22:49", "08:30"));
//    	System.out.println(DateUtil.timeSubtract("22:49", "12:00"));
//    	System.out.println(DateUtil.timeSubtract("22:49", "14:00"));
//    	System.out.println(DateUtil.timeSubtract("22:49", "18:00"));
//    	System.out.println(DateUtil.timeSubtract("22:49", "20:00"));
//    	System.out.println(DateUtil.timeSubtract("22:49", "23:00"));
//    	System.out.println(DateUtil.timeSubtract2("19:49", "12:00"));
//    	System.out.println(DateUtil.timeSubtract2("19:49", "14:00"));
//    	System.out.println(DateUtil.timeSubtract2("19:49", "18:00"));
//    	System.out.println(DateUtil.timeSubtract2("19:49", "20:00"));
//    	System.out.println(DateUtil.timeSubtract2("19:49", "23:00"));
    	//System.out.println(DateUtil.dateSubtract("2017-11-30 12:00:00","2017-11-29 14:00:00"));
    	//System.out.println(DateUtil.getLastDayByCurWeek("2017-11-29"));
       // String date = "2006-07-31";
        //String date2 = "2006-07-31";
//        String d = getCurrentDateStr();
//        Date date2 =getCurrentDate();
//        date2 =new Date();
//        System.out.println(date2);

//        String d1 = format(getCurrentDate(),"yyyy-mm-dd");


//        java.util.Date date = new java.util.Date();
//        java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss",java.util.Locale.US);
//        String day = format.format(date);
//        System.out.println(day);


      //  String dateCN = "2011-03-16 20:22:34 GMT+08:00";
//        SimpleDateFormat sdf_JP = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
//        sdf_JP.setTimeZone(TimeZone.getTimeZone("GMT+8"));

       // System.out.println(sdf_JP.format(sdf_JP.parse(dateCN)));
       // System.out.println(getCurrentDate2());

        //java.util.Calendar cal = new java.util.Calendar("beijing",java.util.Locale.CHINA);

        //System.out.println(date);
        //Date date2 = addDate(date);
        //String data = timeBetween(parseDate(date),parseDate(date));
        //String dd = getCurrentDateStr().replace("-", "");
//        String d2 = date2.replace("-","");
//
//        System.out.println(dd);
//        System.out.println(d2);
//       System.out.println(Long.parseLong(dd)> Long.parseLong(d2));
//        System.out.println(getCurrentDateStr().replace("-", "").compareTo(date2.replace("-","")));

//    	Date date=new Date();
//    	   SimpleDateFormat dateFm = new SimpleDateFormat("EEEE");
//    	   dateFm.format(date);
//        System.out.println(getDaysOfMonth("2011-10-11"));
//        System.out.println( dateFm.format(date));
//        System.out.println(daysBetween(parseDate("2011-10-11"),parseDate("2012-9-11"))); //>0
//        System.out.println(daysBetween(parseDate("2012-10-11"),parseDate("2011-9-11")));
    }
}

