//package com.example.demo.util;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.rocketmq.spring.core.RocketMQTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
///**
// * rocketMq工具包
// *
// * @date： 2021/6/4
// * @author: wbx
// */
//@Slf4j
//@Component
//public class RocketMqUtil {
//
//    @Autowired
//    private RocketMQTemplate rocketMQTemplate;
//
//    public  void sendHelloWorld() {
//        log.info("<<<<<< test: 【{}】 >>>>>>", DateUtil.getCurrentDateStr(DateUtil.C_TIME_PATTON_DEFAULT));
//        String msgContent = "************************ Hello World ************************";
//        log.info("生产者发送消息 : " + msgContent);
//        // 生产者 - 发送消息
//        // ZQ_TOPIC 消息发送到的topic TAG1 消息发送的标签类型 需要与消费者对应
//        this.rocketMQTemplate.convertAndSend("ZQ_TOPIC:TAG1", msgContent);
//    }
//
//}
