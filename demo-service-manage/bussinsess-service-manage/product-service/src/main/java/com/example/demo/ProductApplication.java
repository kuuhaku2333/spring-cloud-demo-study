package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ProductApplication extends BaseApplication {

    /**
     * @Description 生产者服务
     **/
    public static void main(String[] args) {
        SpringApplication.run(ProductApplication.class, args);
        log.info("##############################");
        log.info("##############################");
        log.info("#######生产者服务启动成功#######");
        log.info("##############################");
        log.info("##############################");
    }
}
