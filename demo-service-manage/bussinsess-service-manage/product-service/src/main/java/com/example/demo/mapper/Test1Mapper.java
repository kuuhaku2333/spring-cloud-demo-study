package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.po.Test1Po;

/**
 * (Test1)表数据库访问层
 *
 * @author wbx
 * @since 2021-05-14 11:16:12
 */
public interface Test1Mapper extends BaseMapper<Test1Po> {

}
