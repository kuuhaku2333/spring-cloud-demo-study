package com.example.demo.controller;
import com.example.demo.common.response.ResponseData;
import com.example.demo.util.EsUtil;
//import com.example.demo.util.RocketMqUtil;
import com.example.demo.util.common.ResponseDataUtil;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 生产者controller
 * @date： 2021/5/8
 * @author: wbx
 */
@NoArgsConstructor
@Data
@RestController
public class ProdctController {
//    @Resource
//    RocketMqUtil rocketMqUtil;

    /**
     * index接口
     *
     * @return
     */
    @GetMapping("/index")
    public String index() {
        return "hello world";
    }

//    @PostMapping("/sendMessage")
//    @ApiOperation("发送消息")
//    public ResponseData<String> sendMessage() {
//        rocketMqUtil.sendHelloWorld();
//        return ResponseDataUtil.responseData("消息发送成功");
//    }

    /**
     * 判断es索引是否存在
     * @return
     */
    @PostMapping("/esindex")
    public ResponseData<String> esindex(String index) {
        Boolean aBoolean = EsUtil.existsIndex(index);
        return ResponseDataUtil.responseData("es索引是否存在" + aBoolean);
    }


}
