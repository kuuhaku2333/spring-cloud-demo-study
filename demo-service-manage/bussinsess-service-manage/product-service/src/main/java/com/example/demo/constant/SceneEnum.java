package com.example.demo.constant;

/**
 * RocketMq常量枚举类
 *
 * @date： 2021/5/19
 * @author: wbx
 */
public enum SceneEnum {

    USER_REGISTER("11000001", "USER_REGISTER"),
    USER_REMOVE("11000002", "USER_REMOVE"),

    ORDER_CREATE("12000001", "ORDER_CREATE"),
    ORDER_CLOSED("12000002", "ORDER_CLOSED"),
    ;

    private String sceneCode;

    private String desc;

    SceneEnum(String sceneCode, String desc) {
        this.sceneCode = sceneCode;
        this.desc = desc;
    }

    public String getSceneCode() {
        return sceneCode;
    }

    public String getDesc() {
        return desc;
    }

    public static String destination(SceneEnum scene) {

        String topic = "TP_S_" + scene.getSceneCode().substring(0, 4) + "%" + "EC_EVENT_" + scene.getSceneCode().substring(4, 8);

        String tag = scene.getDesc();

        return topic + ":" + tag;
    }

}
