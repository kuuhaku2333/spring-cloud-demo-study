package com.example.demo.service.impl;

import com.example.demo.util.DateUtil;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时任务调度
 * @date： 2021/6/10
 * @author: wbx
 */
@Component
public class SimpleScheduleImpl {
    /**
     * 3.添加定时任务
     * Cron表达式参数分别表示：
     *           秒（0~59） 例如0/5表示每5秒
     *           分（0~59）
     *           时（0~23）
     *           月的某天（0~31） 需计算
     *           月（0~11）
     *           周几（ 可填1-7 或 SUN/MON/TUE/WED/THU/FRI/SAT）
     */
    // 每隔五秒执行一次
    @Scheduled(cron = "0/5 * * * * ?")
    private void configureTasks() {
        System.out.println("执行定时任务: " + DateUtil.getCurrentDateTimeStr());
    }
}
