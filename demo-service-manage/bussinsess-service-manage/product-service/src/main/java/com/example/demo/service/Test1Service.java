package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.po.Test1Po;

/**
 * (Test1)表服务接口
 *
 * @author wbx
 */
public interface Test1Service extends IService<Test1Po> {

}
