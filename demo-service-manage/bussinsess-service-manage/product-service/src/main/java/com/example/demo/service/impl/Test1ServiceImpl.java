package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.mapper.Test1Mapper;
import com.example.demo.entity.po.Test1Po;
import com.example.demo.service.Test1Service;
import org.springframework.stereotype.Service;

/**
 * (Test1)表服务实现类
 *
 * @author wbx
 * @since 2021-05-14 11:16:14
 */
@Service("test1Service")
public class Test1ServiceImpl extends ServiceImpl<Test1Mapper, Test1Po> implements Test1Service {

}
