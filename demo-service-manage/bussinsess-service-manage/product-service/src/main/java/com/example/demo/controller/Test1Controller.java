package com.example.demo.controller;


import com.example.demo.entity.po.Test1Po;
import com.example.demo.service.Test1Service;
import io.swagger.annotations.Api;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * (Test1)控制层
 *
 * @author wbx
 * @since 2021-05-14 11:16:12
 */
@RestController
@Api(value = "", tags = "  by wbx")
@RequestMapping("/test1")
public class Test1Controller  {
    /**
     * 服务
     */
    @Resource
    private Test1Service test1Service;

    @PostMapping("/test1Add")
    @Transactional
    public String test1Add(@RequestBody Test1Po test1Po){
        boolean flag = true;
        flag = test1Service.save(test1Po);
//        int a = 10 / 0;
        return test1Po.getId();
    }
}
