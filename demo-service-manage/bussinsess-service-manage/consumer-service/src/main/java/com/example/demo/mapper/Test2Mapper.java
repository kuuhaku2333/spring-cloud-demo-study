package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.po.Test2Po;

/**
 * (Test2)表数据库访问层
 *
 * @author wbx
 * @since 2021-05-14 11:08:07
 */
public interface Test2Mapper extends BaseMapper<Test2Po> {

}
