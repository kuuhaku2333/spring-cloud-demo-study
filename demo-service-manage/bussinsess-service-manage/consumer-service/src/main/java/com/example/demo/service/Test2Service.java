package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.po.Test2Po;

/**
 * (Test2)表服务接口
 *
 * @author wbx
 * @since 2021-05-14 11:08:08
 */
public interface Test2Service extends IService<Test2Po> {

}
