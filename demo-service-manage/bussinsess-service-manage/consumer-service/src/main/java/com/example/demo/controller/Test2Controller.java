package com.example.demo.controller;


import com.alibaba.nacos.common.utils.UuidUtils;
import com.demo.example.feign.Test1Feign;
import com.example.demo.entity.po.Test1Po;
import com.example.demo.entity.po.Test2Po;
import com.example.demo.service.Test2Service;
import io.seata.spring.annotation.GlobalTransactional;
import io.swagger.annotations.Api;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * (Test2)控制层
 *
 * @author wbx
 */
@RestController
@Api(value = "", tags = "  by wbx")
@RequestMapping("/test2")
@Validated
public class Test2Controller {
    @Resource
    Test2Service test2Service;
    @Resource
    Test1Feign test1Feign;

    @PostMapping("/addTest2")
    @GlobalTransactional
    public boolean addTest2(@RequestBody @Validated Test2Po test2Po) {

        boolean test1Flag = true;
        boolean test2Flag = true;

        // 调用test2新增方法
        if(test2Flag) test2Po.setTest1Id(UuidUtils.generateUuid().replaceAll("-",""));
        test1Flag = test2Service.save(test2Po);
        // 出现异常进行数据回滚
        int i = 10 / 0;
        Test1Po test1Po = new Test1Po();
        test1Po.setTest1(test2Po.getTest2());
        // 调用test1插入(fegin)
        String test1ID = test1Feign.test1Add(test1Po);

        return test1Flag && test2Flag;
    }

}
