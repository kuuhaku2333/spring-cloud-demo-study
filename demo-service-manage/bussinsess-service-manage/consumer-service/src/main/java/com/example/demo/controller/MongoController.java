package com.example.demo.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.MongoUtil;
import com.example.demo.common.response.ResponseData;
import com.example.demo.entity.form.MongoTestForm;
import com.example.demo.entity.mongoDB.MongoTestPo;
import com.example.demo.entity.param.MongoParam;
import com.example.demo.util.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

import static com.example.demo.util.common.ResponseDataUtil.responseData;

/**
 *  mongoController(测试mongoDB)
 * @author wbx
 */
@RestController
@Api(value = "", tags = "  by wbx")
@RequestMapping("/mongo")
public class MongoController {

    /**
     *  保存文档数据
     */
    @ApiOperation("保存文档数据")
    @PostMapping("/saveMoDoc")
    public ResponseData<Boolean> saveMoDoc( @Validated @RequestBody MongoTestForm mongoTestForm) {
        MongoTestPo mongoTestPo = mongoTestForm.toPo(MongoTestPo.class);
        mongoTestPo.setMydate(DateUtil.getCurrentDateStr());
        boolean b = MongoUtil.save(mongoTestPo);
        return responseData(b);
    }

    /**
     *  根据ID查询ES数据
     */
    @ApiOperation("查询mongoDB集合所有数据")
    @PostMapping("/queryMongoAll")
    public ResponseData<List<MongoTestPo>> queryMongoAll() {
        List<MongoTestPo> mongoTestPos = MongoUtil.findAll(MongoTestPo.class);
        return responseData(mongoTestPos);
    }

    /**
     *  查询mongoDB分页
     */
    @ApiOperation("查询mongoDB分页")
    @PostMapping("/queryPage")
    public ResponseData<Page<MongoTestPo>> queryPage( @Validated @RequestBody MongoParam mongoParam) {
        IPage<MongoTestPo> iPage = (IPage<MongoTestPo>) MongoUtil.queyPage(mongoParam.getValue(), mongoParam.getCollectName(), mongoParam.getCurrent(), mongoParam.getPageSize(), MongoTestPo.class);
        return responseData(iPage);
    }


    /**
     *  根据ID删除文档数据
     */
    @ApiOperation("根据ID删除文档数据")
    @PostMapping("/removeMoByID/{id}")
    public ResponseData<Boolean> removeMoByID(@PathVariable Serializable id) {
        boolean b = MongoUtil.removeByID(id, MongoTestPo.class);
        return responseData(b);
    }






}
