package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.po.Test2Po;
import com.example.demo.mapper.Test2Mapper;
import com.example.demo.service.Test2Service;
import org.springframework.stereotype.Service;

/**
 * (Test2)表服务实现类
 *
 * @author wbx
 * @since 2021-05-14 11:08:08
 */
@Service("test2Service")
public class Test2ServiceImpl extends ServiceImpl<Test2Mapper, Test2Po> implements Test2Service {

}
