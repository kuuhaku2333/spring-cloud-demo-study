//package com.example.demo.service.impl;
//
//import com.example.demo.util.DateUtil;
//import org.apache.rocketmq.spring.annotation.ConsumeMode;
//import org.apache.rocketmq.spring.annotation.MessageModel;
//import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
//import org.apache.rocketmq.spring.core.RocketMQListener;
//import org.springframework.stereotype.Component;
//
///**
// * 消费者Service
// *
// * @date： 2021/5/19
// * @author: wbx
// */
//@Component
//@RocketMQMessageListener(
//        // 消费的topic 与生产者一致
//        topic = "ZQ_TOPIC",
//        // 消费者 消费消息组
//        consumerGroup = "USER_CONSUMER",
//        // 标签类型
//        selectorExpression = "TAG1",
//        //
//        consumeMode = ConsumeMode.ORDERLY,
//        // 消费者模式
//        messageModel = MessageModel.CLUSTERING,
//        consumeThreadMax = 1)
//public class ConsumerService implements RocketMQListener<String> {
//    @Override
//    public void onMessage(String msg) {
//        System.out.println("消费消息：" + msg + "==时间：" + DateUtil.getCurrentDateStr(DateUtil.C_TIME_PATTON_DEFAULT));
//    }
//}
