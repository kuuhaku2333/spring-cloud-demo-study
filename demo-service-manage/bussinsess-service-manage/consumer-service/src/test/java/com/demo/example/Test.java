package com.demo.example;

import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * 测试类
 *
 * @date： 2021/7/13
 * @author: wbx
 */
@RunWith(SpringRunner.class)
public class Test {


    @org.junit.Test
    public void test() {
        /*Integer i1 = 33;

        Integer i2 = 33;

        System.out.println(i1 == i2);// 输出 true

        Float i11 = 333f;

        Float i22 = 333f;

        System.out.println(i11 == i22);// 输出 false

        Double i3 = 1.2;

        Double i4 = 1.2;

        System.out.println(i3 == i4);// 输出 false*/

       /* Integer i1 = 40;

        Integer i2 = new Integer(40);

        System.out.println(i1==i2);*/

        /*float a = 1.0f - 0.9f;
        float b = 0.9f - 0.8f;
        System.out.println(a);// 0.100000024
        System.out.println(b);// 0.099999964
        System.out.println(a == b);// false*/

        BigDecimal a = new BigDecimal("1.0");
        BigDecimal b = new BigDecimal("0.9");
        BigDecimal c = new BigDecimal("0.8");

        BigDecimal x = a.subtract(b);
        BigDecimal y = b.subtract(c);

        System.out.println(x); /* 0.1 */
        System.out.println(y); /* 0.1 */
        System.out.println(Objects.equals(x, y)); /* true */



    }

}
