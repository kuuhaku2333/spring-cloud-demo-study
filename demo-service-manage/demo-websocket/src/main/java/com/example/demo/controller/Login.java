package com.example.demo.controller;

import com.example.demo.po.User;
import com.example.demo.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;


@Controller
public class Login {
	@Autowired
	LoginService loginservice;

	@RequestMapping(value = "/loginvalidate",method = {RequestMethod.GET,RequestMethod.POST})
	public String loginvalidate(@RequestParam("username") String username,@RequestParam("password") String pwd,HttpSession httpSession){
		if(username==null)
			return "login.html";
		String realpwd=loginservice.getpwdbyname(username);
		if(realpwd!=null&&pwd.equals(realpwd))
		{
			long uid=loginservice.getUidbyname(username);
			httpSession.setAttribute("uid", uid);
			return "redirect:/chatroom.html";
		}else
			return "fail.html";
	}

	@RequestMapping("/login")
	public String login(){
		return "login.html";
	}

	@RequestMapping("/logout")
	public String logout(HttpSession httpSession){
		return "login.html";
	}

	@RequestMapping(value="/currentuser",method = RequestMethod.GET)
	@ResponseBody
	public User currentuser(HttpSession httpSession){
		Long uid = (Long)httpSession.getAttribute("uid");
		String name = loginservice.getnamebyid(uid);
		return new User(uid, name);
	}
}
