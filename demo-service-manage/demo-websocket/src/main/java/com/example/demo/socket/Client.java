package com.example.demo.socket;

import java.io.*;
import java.net.Socket;

/**
 * 客户端
 */
public class Client {
    public static void main(String[] args) {
        //创建客户端socket建立连接，指定服务器地址和端口
        try{
            Socket s=new Socket("127.0.0.1",18888);
            PrintWriter pw=new PrintWriter(s.getOutputStream(),true);//IO流发送
            InputStreamReader isr=new InputStreamReader(System.in);//从控制台输入数据
            BufferedReader br=new BufferedReader(isr);//存入缓存区
            InputStream isr2=s.getInputStream();//读取数据
            BufferedReader br2=new BufferedReader(new InputStreamReader(isr2));//存到缓存区
            while(true){
                System.out.println("我发言: ");
                String fasong=br.readLine();//获取数据
                pw.println(fasong);
                String jieshou=br2.readLine();
                System.out.println("服务器说："+jieshou);//输出数据
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}