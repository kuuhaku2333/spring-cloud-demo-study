//package com.example.demo.controller;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//
///**
// * 页面跳转控制
// *
// * @date： 2021/10/19
// * @author: wbx
// */
//@Controller
//public class PageController {
//    @RequestMapping("/login")
//    public String showLogin() {
//        return "index.html";
//    }
//}
