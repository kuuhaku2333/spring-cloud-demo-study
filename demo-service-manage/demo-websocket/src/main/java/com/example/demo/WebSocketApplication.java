package com.example.demo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class WebSocketApplication {

    /**
     * @Description 生产者服务
     **/
    // 访问 http://127.0.0.1:18889/login 密码1234
    public static void main(String[] args) {
        SpringApplication.run(WebSocketApplication.class, args);
    }
}
