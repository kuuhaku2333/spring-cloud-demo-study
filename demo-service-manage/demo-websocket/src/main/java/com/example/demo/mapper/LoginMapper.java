package com.example.demo.mapper;


import com.example.demo.po.Staff;

public interface LoginMapper {
	Staff getpwdbyname(String name);
	Staff getnamebyid(long id);
}
