package com.example.demo.conf;

import com.alibaba.druid.filter.config.ConfigTools;
import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * 配置数据源，指定mapper包的扫描路径和SessionFactory对象
 */
@Configuration
@MapperScan(value = "com.example.demo.mapper",sqlSessionFactoryRef = "sqlSessionFactory")
public class MyDataSource {

    @Value("${spring.datasource.ssm.url}")
    private String url;

    @Value("${spring.datasource.ssm.username}")
    private String username;

    @Value("${spring.datasource.ssm.password}")
    private String password;

    @Value("${spring.datasource.ssm.driver-class-name}")
    private String driverClassName;

    @Bean
    public DataSource dataSource() {
    	DruidDataSource datasource = new DruidDataSource();
        datasource.setUrl(url);
        datasource.setUsername(username);
        datasource.setPassword(password);
        datasource.setDriverClassName(driverClassName);
        return datasource;
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory( ) throws Exception {
    	// 设置mapper的xml文件路径
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource());
        Resource[] resources = new PathMatchingResourcePatternResolver()
                .getResources("classpath:mapper/*.xml");
        factoryBean.setMapperLocations(resources);
        // 设置mybatis-config.xml的路径
        Resource config = new PathMatchingResourcePatternResolver()
                .getResource("classpath:mybatis-config.xml");
        factoryBean.setConfigLocation(config);
        return factoryBean.getObject();
    }

    @Bean
    public DataSourceTransactionManager primaryTransactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }


    @Bean
    public SqlSessionTemplate sqlSessionTemplate() throws Exception {
    	// 使用上面配置的Factory
        SqlSessionTemplate template = new SqlSessionTemplate(sqlSessionFactory());
        return template;
    }

    public static void main(String[] args) throws Exception {
        String path = "abc\\\\/cdf";
        String privateKey = "MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAp+rPOSN7LMqTHDaD3xJZMK3hkCF6s/6ebSlEgirb5lNhN7MxhV4WOAUjX3jI6MqJBsiegHN5L/vUCJIiQcLGjwIDAQABAkBSGOGcBBby/VATmSu8YtIslLcVqui9HB4FLQoLwjylTQ8JmqTQD8tNtT5u63yj0N3Q9/nwoVtaLfZ7rsAHpt3BAiEA5cXMAhKDV0oI6aXOMgK+gkJoNv4Kf3aixyTq95WH7VECIQC7FYfmKccMetYEvMyENW9KhP1LjqfWTV33csBWATL93wIhANIfG/0ZKPr+PUBN3E5nrAg66kq/7qG5wAoVfLqFCBTBAiEAgtMynmQWmoaHhPaVudo4Ag9vpVeTzIt7q19mWn4qL1cCIGg89iYgH4H5Ls8yimU75b/zvhAArXsZX6gK6f2qKvJ4";
        String publcKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKfqzzkjeyzKkxw2g98SWTCt4ZAherP+nm0pRIIq2+ZTYTezMYVeFjgFI194yOjKiQbInoBzeS/71AiSIkHCxo8CAwEAAQ==";
        // 加密后结果
        String encrypt = ConfigTools.encrypt(privateKey, "123456");
        System.out.println("encrypt = " + encrypt);
        // 解密后结果
        String decrypt = ConfigTools.decrypt(publcKey, encrypt);
        System.out.println("decrypt = " + decrypt);
    }



}
