package com.example.demo.jdk;

import com.example.demo.DynamicProxy.jdk.JdkProxyFactory;
import com.example.demo.DynamicProxy.service.SmsService;
import com.example.demo.DynamicProxy.service.impl.SmsServiceImpl;
import com.example.demo.JAVATESTApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @date： 2021/6/15
 * @author: wbx
 */
@SpringBootTest(classes = {JAVATESTApplication.class})
@RunWith(SpringRunner.class)
public class Test {

    @org.junit.Test
    public void test() {
        SmsService smsService = (SmsService) JdkProxyFactory.getProxy(new SmsServiceImpl());
        smsService.send("JDK 动态代理类 java");
    }
}
