package com.example.demo.asynctest;

import lombok.SneakyThrows;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @date： 2021/6/15
 * @author: wbx
 */
@RunWith(SpringRunner.class)
public class Test {

    @org.junit.Test
    public void test1() {
        List<String> list = new ArrayList<>();
        list.add("aaaa");
        list.add("a");
        list.add("b");
        list.add("b");
        list.add("c");
        // 统计一个集合中 各个对象的数量
        Map<String, Long> collect = list.stream().collect(Collectors.groupingBy(item -> item, Collectors.counting()));
        System.out.println("123");
    }

    @org.junit.Test
    /**
     *  同步执行 会阻塞
     */
    public void testAsync() throws ExecutionException, InterruptedException {
        AtomicInteger a = new AtomicInteger(1);
        // 执行一个任务 返回结果
        Future<Integer> completableFuture = CompletableFuture.completedFuture(sum(a.get(), 3));
        System.out.println("任务开始执行1！");
        // 获取任务的执行结果
        a.set(completableFuture.get());
        System.out.println(a.get());
    }

    /**
     *  多线程异步执行 主线程会等待子线程执行完毕
     */
    @org.junit.Test
    @SneakyThrows
    public void testThread() {
        AtomicInteger a = new AtomicInteger(1);
        Thread thread1 = new Thread(() -> {
            a.set(sum(a.get(), 1));
            System.out.println("thread1");
        });

        Thread thread2 = new Thread(() -> {
            a.set(sum(a.get(), 1));
            System.out.println("thread2");
        });

        thread1.start();
        // join主线程需要等到子线程结束后再进行操作(用于获取子线程执行的结果)
        thread1.join();
        thread2.start();
        thread2.join();
        a.set(a.get() + 1);
        System.out.println(a.get());
    }

    /**
     *  多线程异步执行 主线程会等待子线程执行完毕
     */
    @org.junit.Test
    @SneakyThrows
    public void testCompletableFuture() {
        AtomicInteger a = new AtomicInteger(1);
        CompletableFuture<Integer> future1 = new CompletableFuture<>();
        new Thread(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            future1.complete(sum(a.get(), 1));
        }).start();

        CompletableFuture<Integer> future2 = new CompletableFuture<>();
        new Thread(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            future2.complete(sum(a.get(), 1));
        }).start();
        future1.whenComplete((result, throwable) -> {
            System.out.println(String.format("future1执行完毕：%s",result));
        });
        future2.whenComplete((result, throwable) -> {
            System.out.println(String.format("future2执行完毕：%s",result));
        });
    }


    @SneakyThrows
    private int sum(int a, int b) {
        System.out.println("任务开始执行！");
        return a + b;
    }



    public static String stringParseASCII(String str) {
        StringBuilder result = new StringBuilder();
        char tmp;
        for (int i = 0; i < str.length(); i++) {
            tmp = (char) (str.charAt(i) + 5);
            if (tmp > 'z') {
                result.append('a');
            } else {
                result.append(tmp);
            }
        }
        return result.toString();
    }
}


