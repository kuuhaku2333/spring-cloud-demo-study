package com.example.demo.cglib;

import com.example.demo.DynamicProxy.cglib.CglibProxyFactory;
import com.example.demo.DynamicProxy.service.SmsService;
import com.example.demo.DynamicProxy.service.impl.SmsServiceImpl;
import com.example.demo.JAVATESTApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @date： 2021/6/15
 * @author: wbx
 */
@SpringBootTest(classes = {JAVATESTApplication.class})
@RunWith(SpringRunner.class)
public class Test {

    @org.junit.Test
    public void test() {
        SmsService aliSmsService = (SmsService) CglibProxyFactory.getProxy(SmsServiceImpl.class);
        aliSmsService.send("cglib 动态代理类 JAVA");
    }

    @org.junit.Test
    public void ListTest() {

        List<Integer> arrayList = new ArrayList<>();
        List<Integer> linkList = new LinkedList<>();
        for(int i = 0; i < 10000; i++) {
            arrayList.add(i);
            linkList.add(i);
        }

        long start = System.currentTimeMillis();
        Integer integer = arrayList.get(arrayList.size() - 1);
        long end = System.currentTimeMillis();
        System.out.println("ArrayList获取指定位置值时间：" + (end - start) + integer);
        long start1 = System.currentTimeMillis();
        Integer integer1 = linkList.get(arrayList.size() - 1);
        long end1 = System.currentTimeMillis();
        System.out.println("LinkList获取指定位置值时间：" + (end1 - start1) + integer1);
    }


    @org.junit.Test
    public void mapTest() {
        Map<String, String> map = new HashMap<>(17);
        map.put(null,null);
        map.put("test","test");
        map.putIfAbsent("test", "test1");
        // 如果不存在就放入 存在就不改变
        String put = map.putIfAbsent("test", "test2");
//        map.put("test3","test3");
//        map.put("test4","test3");
//        map.put("test5","test3");
//        map.put("test6","test3");
//        map.put("test7","test3");
//        map.put("test8","test3");
//        map.put("test9","test3");
//        map.put("test10","test3");
//        map.put("test11","test3");
//        map.put("test12","test3");
//        map.put("test13","test3");
//        map.put("test14","test3");
//        map.put("test15","test3");
//        map.put("test16","test3");
        System.out.println("test原始值是 = " + put);
        System.out.println("test实际值是 = " + map.get("test"));


    }


    @org.junit.Test
    public void concurrentHashMapTest() {

        ConcurrentHashMap<String, String> map = new ConcurrentHashMap<>();
        map.put("test","test");
        map.putIfAbsent("test", "test1");
        // 如果不存在就放入 存在就不改变
        String put = map.putIfAbsent("test", "test2");
        map.put("test0", "test2");
        map.put("test3","test3");
        map.put("test4","test3");
        map.put("test5","test3");
        map.put("test6","test3");
        map.put("test7","test3");
        map.put("test8","test3");
        map.put("test9","test3");
        map.put("test10","test3");
        map.put("test11","test3");
        map.put("test12","test3");
        map.put("test13","test3");
        map.put("test14","test3");
        map.put("test15","test3");
        map.put("test16","test3");
        map.put("test16","test4");
        System.out.println("test原始值是 = " + put);
        System.out.println("test实际值是 = " + map.get("test"));

    }


    /**
     *  JMX测试类
     */
    @org.junit.Test
    public void test1() {

        // 2的30次方 (左移运算)
        int a = 1 << 30;
        System.out.println("a = " + a);
        int maxValue = Integer.MAX_VALUE;
        System.out.println("maxValue = " + maxValue);
        System.out.println(" a > maxValue" + (a > maxValue));

    }

    /**
     *  JMX测试类
     */
    @org.junit.Test
    public void JMXTest() {
        // 获取 Java 线程管理 MXBean
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        // 不需要获取同步的 monitor 和 synchronizer 信息，仅获取线程和线程堆栈信息
        ThreadInfo[] threadInfos = threadMXBean.dumpAllThreads(false, false);
        // 遍历线程信息，仅打印线程 ID 和线程名称信息
        for (ThreadInfo threadInfo : threadInfos) {
            System.out.println("[" + threadInfo.getThreadId() + "] " + threadInfo.getThreadName());
        }
    }

}

