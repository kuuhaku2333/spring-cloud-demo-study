package com.example.demo.asynctest;

import lombok.SneakyThrows;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * CompletableFuture 异步多线程测试类
 * @date： 2021/6/15
 * @author: wbx
 */
@RunWith(SpringRunner.class)
public class CompletableFutureTest {

    /**
     *  模拟异步rpc调用
     */
    @org.junit.Test
    public void test1() {
        TaskService service = new TaskService();
        // 执行异步调用
        CompletableFuture<String> future = service.findUser();
        future.whenCompleteAsync((t, u) -> {
            if (u != null) {
                System.out.println("异步调用发生异常:" + u);
            } else {
                System.out.println("异步调用执行正常: " + t);
            }
        });
        System.out.println("主线程任务执行完毕");
    }

    @org.junit.Test
    @SneakyThrows
    public void testAsyncSum() {
        AtomicInteger a = new AtomicInteger(1);
        CompletableFuture<Integer> future1 = sum1(a.get(), 1);
        if (future1.isDone())
            a.set(future1.get());
        CompletableFuture<Integer> future2 = sum2(a.get(), 1);
        if (future2.isDone())
            a.set(future2.get());
        a.set(a.getAndIncrement());
        System.out.println("main：" + Thread.currentThread().getId());
        System.out.println(a.get());
    }

    @SneakyThrows
    private CompletableFuture<Integer> sum1(int a, int b) {
        CompletableFuture<Integer> future = new CompletableFuture<>();
        CompletableFuture.runAsync(() -> {
            System.out.println("线程ID" + Thread.currentThread().getId());
            future.complete(a + b);
        });
        return future;
    }

    @SneakyThrows
    private CompletableFuture<Integer> sum2(int a, int b) {
        CompletableFuture<Integer> future = new CompletableFuture<>();
        CompletableFuture.runAsync(() -> {
            System.out.println("线程ID" + Thread.currentThread().getId());
            future.complete(a + b);
        });
        return future;
    }

    static class TaskService {
        @SneakyThrows
        public CompletableFuture<String> findUser() {
            CompletableFuture<String> future = new CompletableFuture<>();
            //模仿远程调用线程
            new Thread(() -> {
                String result = null;
                System.out.println("任务开始执行....");
                try {
                    Thread.sleep(3000);
                    //模仿RPC远程调用
                    result = rpcRequest(true);
                    System.out.println("任务执行结束....");
                } catch (Exception ex) {
                    future.completeExceptionally(ex);
                }
                future.complete(result);
            }).start();
            future.get();
            //直接返回future.
            return future;
        }

        /**
         * 功能描述
         *
         * @author lgj
         * @Description 模仿RPC远程调用
         * @date 4/29/19
         * @param: flag　　　true：返回正常结果  false:抛出异常
         */
        public String rpcRequest(boolean flag) {
            String result;
            if (flag) {
                result = "libai";
            } else {
                throw new NullPointerException();
            }
            return result;
        }
    }
}


