package com.example.demo.jython;

import org.python.core.Py;
import org.python.core.PySystemState;
import org.python.util.PythonInterpreter;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @Description: java调用python方法
 */

public class Test {
    public static void main(String[] args) {
        try {
            PythonInterpreter interpreter = new PythonInterpreter();
            PySystemState sys = Py.getSystemState();
            sys.path.add("D:\\Java\\python\\jython\\Lib");
            interpreter.exec("from sympy import *");
            interpreter.exec("print sys.path");
            interpreter.exec("path = \"D:\\Java\\python\\jython\\Lib\"");
            interpreter.exec("sys.path.append(path)");
            interpreter.exec("print sys.path");
            interpreter.exec("a=3; b=5;");
            InputStream filePy = new FileInputStream("D:\\Java\\Lagrange_interpolation.py");
            interpreter.execfile(filePy);
            filePy.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

