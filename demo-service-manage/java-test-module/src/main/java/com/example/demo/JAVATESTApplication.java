package com.example.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = {"com.example.demo.excel"})
public class JAVATESTApplication {

    /**
     * @Description 消费者服务
     **/
    public static void main(String[] args) {
        SpringApplication.run(JAVATESTApplication.class, args);
    }


}
