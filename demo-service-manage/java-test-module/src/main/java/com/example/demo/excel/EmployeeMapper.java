package com.example.demo.excel;

import java.util.List;

public interface EmployeeMapper extends BaseDaoMapper<EmployeeExporter> {

    /**
     * 批量插入
     * @param employees 员工表
     */
    void batchInsert(List<EmployeeExporter> employees);
}
