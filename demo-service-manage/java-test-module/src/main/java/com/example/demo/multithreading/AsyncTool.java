package com.example.demo.multithreading;

import cn.hutool.core.date.DateUtil;
import com.jd.platform.async.executor.Async;
import com.jd.platform.async.wrapper.WorkerWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * 三个线程并行
 * @date： 2021/11/4
 * @author: wbx
 */
public class AsyncTool {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ParWorker1 w1 = new ParWorker1();
        ParWorker2 w2 = new ParWorker2();
        ParWorker3 w3 = new ParWorker3();

        List<WorkerWrapper> workerWrappers = new ArrayList<>();
        WorkerWrapper<String, String> workerWrapper2 =  new WorkerWrapper.Builder<String, String>()
                .worker(w2)
                .callback(w2)
                .param("2")
                .build();

        WorkerWrapper<String, String> workerWrapper1 =  new WorkerWrapper.Builder<String, String>()
                .worker(w1)
                .callback(w1)
                .param("1")
                .build();

        WorkerWrapper<String, String> workerWrapper =  new WorkerWrapper.Builder<String, String>()
                .worker(w3)
                .callback(w3)
                .param("3")
                .build();
        workerWrappers.add(workerWrapper);
        workerWrappers.add(workerWrapper1);
        workerWrappers.add(workerWrapper2);

        long start = System.currentTimeMillis();
        System.out.println("begin-" + DateUtil.now());

        WorkerWrapper [] strArr =  workerWrappers.toArray( new WorkerWrapper[]{});
        Async.beginWork(1500, strArr);

        System.out.println("end-" + DateUtil.now());
        long end = System.currentTimeMillis();
        System.err.println("cost-" + (end - start));
        System.out.println(Async.getThreadCount());
        System.out.println(workerWrapper.getWorkResult());
        Async.shutDown();

    }
}
