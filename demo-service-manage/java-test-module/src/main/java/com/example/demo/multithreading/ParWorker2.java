package com.example.demo.multithreading;

import cn.hutool.core.date.DateUtil;
import com.jd.platform.async.callback.ICallback;
import com.jd.platform.async.callback.IWorker;
import com.jd.platform.async.worker.WorkResult;

public class ParWorker2 implements IWorker<String, String>, ICallback<String, String> {

    @Override
    public String action(String object) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "result = " + DateUtil.now() + "---param = " + object + " from 1";
    }

    @Override
    public String defaultValue() {
        return "worker2--default";
    }

    @Override
    public void begin() {
        //System.out.println(Thread.currentThread().getName() + "- start --" + System.currentTimeMillis());
    }

    @Override
    public void result(boolean success, String param, WorkResult<String> workResult) {
        if (success) {
            System.out.println("callback worker2 success--" + DateUtil.now() + "----" + workResult.getResult()
                    + "-线程名称:" +Thread.currentThread().getName());
        } else {
            System.err.println("callback worker2 failure--" + DateUtil.now() + "----"  + workResult.getResult()
                    + "-线程名称:" +Thread.currentThread().getName());
        }
    }

}