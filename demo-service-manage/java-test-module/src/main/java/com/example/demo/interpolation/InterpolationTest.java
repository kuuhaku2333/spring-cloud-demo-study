package com.example.demo.interpolation;

import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.MathException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.commons.math.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math.analysis.interpolation.UnivariateRealInterpolator;
import org.apache.commons.math.analysis.polynomials.PolynomialFunction;
import org.apache.commons.math.analysis.polynomials.PolynomialFunctionLagrangeForm;
import org.apache.commons.math.analysis.polynomials.PolynomialSplineFunction;

/**
 * @author Jia Yu
 * @date 2010-11-21
 */
public class InterpolationTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        polynomialsInterpolation();
        System.out.println("-------------------------------------------");
        interpolatioin();
    }

    private static void interpolatioin() {
        // TODO Auto-generated method stub
        // double x[] = { 0.0, 0.5, 1.0 };
        // double y[] = { 0.0, 0.5, 1.0 };
//        double[] x = {0.0, Math.PI / 6d, Math.PI / 2d, 5d * Math.PI / 6d,
//                Math.PI, 7d * Math.PI / 6d, 3d * Math.PI / 2d,
//                11d * Math.PI / 6d, 2.d * Math.PI};
//        double[] y = {0d, 0.5d, 1d, 0.5d, 0d, -0.5d, -1d, -0.5d, 0d};
        double[] x = {0,1,2,3};
        double[] y = {2,1,0,-1};
        UnivariateRealInterpolator i = new SplineInterpolator();
        UnivariateRealFunction f = null;
        // interpolate y when x = 0.5
        try {
            f = i.interpolate(x, y);
            System.out.println("when x = 0.5, y = " + f.value(0.5));
        } catch (MathException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //check polynomials functions
        PolynomialFunction[] polynomials = ((PolynomialSplineFunction) f).getPolynomials();
        for (int j = 0; j < polynomials.length; j++) {
            System.out.println("cubic spline:f" + j + "(x) = " + polynomials[j]);
        }
    }

    private static void polynomialsInterpolation() {
        // TODO Auto-generated method stub
        double[] x = {0.0, -1.0, 0.5};
        double[] y = {-3.0, -6.0, 0.0};
        PolynomialFunctionLagrangeForm p = new PolynomialFunctionLagrangeForm(x, y);
        double[] interpolatingPoints = p.getInterpolatingPoints();
        double[] interpolatingValues = p.getInterpolatingValues();

        // output directly
        System.out.println("ugly output is " + p);
        // interpolate y when x = 1.0
        try {
            System.out.println("when x = 1.0, y = " + p.value(1.0));
        } catch (FunctionEvaluationException e) {
            e.printStackTrace();
        }
        // degree
        System.out.println("polynomial degree is " + p.degree());
        // coefficients
        for (int i = 0; i < p.getCoefficients().length; i++) {
            System.out.println("coeff[" + i + "] is " + p.getCoefficients()[i]);
        }
        //
    }

}