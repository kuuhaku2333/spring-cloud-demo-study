package com.example.demo.DynamicProxy.service;

/**
 * 定义发送短信的接口
 *
 * @date： 2021/7/15
 * @author: wbx
 */
public interface SmsService {
    String send(String message);
}
