package com.example.demo.DynamicProxy.service.impl;

import com.example.demo.DynamicProxy.service.SmsService;
import org.springframework.stereotype.Service;

/**
 * 实现发送短信的接口
 *
 * @date： 2021/7/15
 * @author: wbx
 */
@Service
public class SmsServiceImpl implements SmsService {
    public String send(String message) {
        System.out.println("send message:" + message);
        return message;
    }
}
