package com.example.demo.excel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Api(tags = "excel操作")
@RestController
@RequestMapping(value = "/employee/excel")
public class EmployeeExcelController {

    @Resource
    private IEmployeeService employeeService;

    @ApiOperation(value = "初始化数据")
    @GetMapping("/initData")
    public ResultVo initData(){
        employeeService.initData();
        return ResultVo.success();
    }

    @ApiOperation(value = "导入")
    @PostMapping(value="/import")
    public ResultVo importExcel(@RequestParam(name = "file") MultipartFile file) throws IOException {
        employeeService.importExcel(file);
        return ResultVo.successMsg("导入成功");
    }

    @ApiOperation(value = "导出")
    @PostMapping(value = "/export")
    public void exportExcel(HttpServletResponse response) throws IOException {
        employeeService.exportExcel(response);
    }
}
