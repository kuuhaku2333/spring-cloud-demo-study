package com.example.demo.jython;

import org.python.core.Py;
import org.python.core.PyFunction;
import org.python.core.PyInteger;
import org.python.core.PyObject;
import org.python.core.PySystemState;
import org.python.util.PythonInterpreter;

public class FirstJavaScript {
    public static void main(String[] args) {
        PythonInterpreter interpreter = new PythonInterpreter(); //‘odbchelper’是自定义的python库，在 Python 方法中我们需要用到这个库中的方法，所以需要提前导入进来
        PySystemState sys = Py.getSystemState();
        sys.path.add("E:\\_Python\\diveintopythonzh-cn-5.4b\\py");
        interpreter.exec("import odbchelper");
        interpreter.execfile("E:\\_Python\\diveintopythonzh-cn-5.4b\\py_test\\pythonTest.py");
        PyFunction func = (PyFunction) interpreter.get("add", PyFunction.class);
        int a = 2010, b = 2;
        PyObject pyobj = func.__call__(new PyInteger(a), new PyInteger(b));
        System.out.println("anwser = " + pyobj.toString());
    }

}
