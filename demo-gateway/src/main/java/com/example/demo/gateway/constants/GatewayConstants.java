package com.example.demo.gateway.constants;

/**
 * Created on 2020/11/1.
 */
public interface GatewayConstants {

    String IP_ADDRESS = "ipAddress";

    String DYNAMIC_ROUTE_DATA_ID = "dynamic";

    String DYNAMIC_ROUTE_GROUP_ID = "gateway-group";

}
