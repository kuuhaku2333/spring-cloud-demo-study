package com.example.demo.gateway.service.impl;

import com.example.demo.gateway.service.PathVerifyService;
import org.springframework.stereotype.Service;

/**
 * Created on 2020/11/1.
 */
@Service
public class PathVerifyServiceImpl implements PathVerifyService {

    @Override
    public Boolean shouldFilter(String path) {
        return true;
    }
}
