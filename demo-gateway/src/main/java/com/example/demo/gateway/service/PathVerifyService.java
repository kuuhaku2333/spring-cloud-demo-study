package com.example.demo.gateway.service;

/**
 * Created on 2020/11/1.
 */
public interface PathVerifyService {

    /**
     * 判断路径是否需要鉴权
     * @param path
     * @return
     */
    Boolean shouldFilter(String path);

}
