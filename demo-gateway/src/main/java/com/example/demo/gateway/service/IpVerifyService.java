package com.example.demo.gateway.service;

/**
 * Created on 2020/11/1.
 */
public interface IpVerifyService {

    /**
     * 校验ip是否为有效访问
     * @param ip ip
     * @return Boolean
     */
    Boolean isValid(String ip);
}
