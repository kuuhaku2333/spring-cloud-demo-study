package com.example.demo.gateway.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Created on 2020/11/4.
 */
@RestController
@RequestMapping("/v1/test")
@Slf4j
public class FallbackController {

    @GetMapping("/fallback")
    public Mono<String> fallback(){
        log.info("fallback");
        return Mono.just("fallback");
    }
}
