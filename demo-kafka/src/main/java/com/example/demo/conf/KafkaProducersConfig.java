//package com.example.demo.conf;
//
//import org.apache.kafka.clients.producer.ProducerConfig;
//import org.apache.kafka.common.serialization.StringDeserializer;
//import org.apache.kafka.common.serialization.StringSerializer;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.kafka.annotation.EnableKafka;
//import org.springframework.kafka.core.DefaultKafkaProducerFactory;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.kafka.core.ProducerFactory;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * kafaka配置文件
// *
// * @date： 2021/7/2
// * @author: wbx
// */
//@Configuration
//@EnableKafka
//public class KafkaProducersConfig {
//
//    private String brokers = "10.161.126.248:9092";
//
//    @Bean("kafkaTemplate")
//    public KafkaTemplate<String, String> kafkaTemplate() {
//        KafkaTemplate<String, String> kafkaTemplate = new KafkaTemplate<String, String>(producerFactory());
//        return kafkaTemplate;
//    }
//
//    public ProducerFactory<String, String> producerFactory() {
//        Map<String, Object> properties = new HashMap<String, Object>();
//        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
//        properties.put(ProducerConfig.BATCH_SIZE_CONFIG, 4096);
//        properties.put(ProducerConfig.LINGER_MS_CONFIG, 1);
//        properties.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 40960);
//        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
//        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
//        return new DefaultKafkaProducerFactory<String, String>(properties);
//    }
//
//
//}
